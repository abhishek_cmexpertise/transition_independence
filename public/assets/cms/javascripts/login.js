var Login = function () {

    var handleLogin = function () {

        $('#login-form').validate({

            rules: {
                admin_username: {required: true},
                admin_password: {required: true},
                remember: {required: false}
            },

            messages: {
                admin_username: {required: "Please enter email address"},
                admin_password: {required: "Please enter password"},
                remember: {required: false}
            },

            submitHandler: function () {

                var url = $('#login-form').attr('action');
                var data = $('#login-form').serialize();

                ajaxCall(url, data, function (output) {
                    if (output.status == 'success') {
                        $('.alert-danger', $('#login-form')).hide();
                        $('.alert-success').children('span').html(output.message);
                        $('.alert-success', $('#login-form')).show();

                        setTimeout(function () {
                            location.href = output.redirect;
                        }, 1500);

                    } else if (output.status == 'error') {
                        $('.alert-danger').children('span').html(output.message);
                        $('.alert-danger', $('#login-form')).show();
                        $('.alert-success', $('#login-form')).hide();
                    }
                });
            }
        });

        $('.login-form input').keypress(function (e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    $('.login-form').submit();
                }
                return false;
            }
        });

    }

    var general = function () {
        $('#back-btn').click(function () {
            window.location.href = base_url + 'admin/login';
        });
    }


    return {
        init: function () {
            handleLogin();
        },
    }

}();


