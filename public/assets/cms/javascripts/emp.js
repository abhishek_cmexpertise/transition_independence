var Emp = function () {

    var handleBannerEdit = function () {

        $('#event_add').validate({
            rules: {
                banner_title: {required: true},
                bigimg: {
                    required: {depends: function (e) {
                            return ($('#hidden_logo').val() == '');
                        }},
                    extension: "png|jpeg|jpg|PNG|JPEG|JPG",
                },
            },
            messages: {
                banner_title: {required: "Please enter banner image alternative tag"},
                bigimg: {required: "Please select image", extension: "Only png,jpeg,jpg file allowed"},
            },
        });

        $(document).on('change', '#imgInp', function () {
            setTimeout(function () {
                $('#banner_title').focus();
                $('#imgInp').focus();
            }, 100)
        });

        function readURL(input) {

            if (input.files && input.files[0]) {

                var fileName = $("#imgInp").val();
                var idxDot = fileName.lastIndexOf(".") + 1;
                var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

                if (extFile == "jpg" || extFile == "jpeg" || extFile == "png") {
                    $("#blah").show();
                } else {
                    $("#blah").hide();
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    }

    var handleEmpEdit = function () {

        $('#event_add').validate({
            rules: {
                banner_desc: {required: true},
                banner_title: {required: true},
            },
            messages: {
                banner_desc: {required: "Please enter topic description"},
                banner_title: {required: "Please enter topic title"},
            },
        });

    }

    var handleInstructorJob = function () {

        $('#event_add').validate({
            rules: {
                banner_desc: {required: true},
                banner_title: {required: true},
            },
            messages: {
                banner_desc: {required: "Please enter topic description"},
                banner_title: {required: "Please enter topic title"},
            },
        });

    }

    var handleSupervisorData = function () {

        $('#event_add').validate({
            rules: {
                banner_desc: {required: true},
                banner_title: {required: true},
            },
            messages: {
                banner_desc: {required: "Please enter topic description"},
                banner_title: {required: "Please enter topic title"},
            },
        });

    }

    return {
        init: function () {
            handleBannerEdit();
        },
        empEdit: function () {
            handleEmpEdit();
        },
        instruct: function () {
            handleInstructorJob();
        },
        supervisor: function () {
            handleSupervisorData();
        }

    }

}();


