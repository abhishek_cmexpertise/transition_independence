var Tools = function () {

    var handleSettings = function () {

        $('#frmsettings').validate({
            rules: {
                facebook_link: {required: true},
                twitter_link: {required: true},
                googleplus_link: {required: true},
                pinterest_link: {required: true},
                googlemap_link: {required: true},
            },
            messages: {
                facebook_link: {required: "Please enter facebook link"},
                twitter_link: {required: "Please enter twitter link"},
                googleplus_link: {required: "Please enter google plus link"},
                pinterest_link: {required: "Please enter pinterest link"},
                googlemap_link: {required: "Please enter google map link"},
            },
        });
    }

    var handleChangePassword = function () {

        $('#change_password').validate({
            rules: {
                oldpass: {required: true},
                newpass: {required: true, minlength: 6},
                conpass: {required: true, equalTo: '#newpass'}
            },
            messages: {
                oldpass: {required: "Please enter old password"},
                newpass: {required: "Please enter new password", minlength: "Please enter minimum 6 characters or digits"},
                conpass: {required: "Please enter confirm password", equalTo: 'Confirm password and new does not match'}
            },
        });

    }


    return {
        init: function () {
            handleSettings();
        },
        changePassword: function () {
            handleChangePassword();
        },

    }

}();


