var Client = function () {

    var handleBannerEdit = function () {

        $('#event_add').validate({
            rules: {
                banner_desc: {required: true},
                banner_title: {required: true},
                bigimg: {
                    required: {depends: function (e) {
                            return ($('#hidden_logo').val() == '');
                        }},
                    extension: "png|jpeg|jpg|PNG|JPEG|JPG",
                },
            },
            messages: {
                banner_desc: {required: "Please enter banner text"},
                banner_title: {required: "Please enter banner alternative tag"},
                bigimg: {required: "Please select image", extension: "Only png,jpeg,jpg file allowed"},
            },
        });

        $(document).on('change', '#imgInp', function () {
            setTimeout(function () {
                $('#banner_title').focus();
                $('#imgInp').focus();
            }, 100)
        });

        function readURL(input) {

            if (input.files && input.files[0]) {

                var fileName = $("#imgInp").val();
                var idxDot = fileName.lastIndexOf(".") + 1;
                var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

                if (extFile == "jpg" || extFile == "jpeg" || extFile == "png") {
                    $("#blah").show();
                } else {
                    $("#blah").hide();
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });

    }

    var handleList = function () {
        $('body').on('click', '.deleteClient', function () {
            var href = $(this).attr('data-href');
            var footerContent = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\
                                <a href="' + href + '" class="btn btn-danger"><i class="fa fa-check">Delete</i></a>';
            $("#modelTitle").text('Delete Client');
            $("#modelContent").html("<h3>Are you sure you want to delete ?</h3>");
            $("#modelFooter").html(footerContent);
            $('#myModal').modal('show');
        });


        $('body').on('click', '.status', function () {
            var id = $(this).data('val');
            var val = $(this).val();
            ajaxCall(base_url + '/superadmin/Client/updateStatus/', {id: id, val: val}, function (output) {});
        });

        $('body').on('click', '.multipleAction', function () {

            var id = $(this).data('id');
            var data = {ids: id};
            var url = base_url + 'superadmin/Client/handleMultiple/';

            $('#myModal').modal('hide');

            ajaxCall(url, data, function (output) {
                $("#msg").html(getFlashMessage(output.class, output.message));
                $('#user_management_table').DataTable().ajax.reload(null, false).on('draw.dt', function () {
                    $('body').find('.loading').remove();
                });
                $('.allCheck').prop('checked', false);
            });
        });

        $('body').on('click', '.allCheck', function () {

            if ($(this).is(':checked')) {
                $('.selectCheck').prop('checked', true);
            } else {
                $('.selectCheck').prop('checked', false);
            }
        });


        /*
         * Handle commonly multiple active deactive and delete
         */
        var checkedId = [];

        $('body').on('click', '.multipleHandle', function () {

            var status = $(this).data('status');

            $('.checkboxes').each(function () {
                if ($(this).is(':checked')) {
                    if ($.inArray($(this).val(), checkedId) === -1) {
                        checkedId.push($(this).val());
                    }
                } else {
                    if ($.inArray($(this).val(), checkedId) !== -1) {
                        checkedId.splice($.inArray($(this).val(), checkedId), 1);
                    }
                }
            });

            if (checkedId.length == 0) {
                alert('Please select checkbox');
            } else {

                $.ajax({
                    type: 'POST',
                    url: base_url + '/superadmin/Client/handleMultiple/',
                    data: {id: checkedId},
                    success: function (msg) {
                        window.location.href = base_url + '/superadmin/Client/mng_client';
                    }
                });
            }
        });
    }

    var handleAdd = function () {

        $('#event_add').validate({
            rules: {
                client_name: {required: true},
                banner_desc: {required: true},
                bigimg: {
                    required: {depends: function (e) {
                            return ($('#hidden_logo').val() == '');
                        }},
                    extension: "png|jpeg|jpg|PNG|JPEG|JPG",
                },
            },
            messages: {
                client_name: {required: "Please enter client name"},
                banner_desc: {required: "Please enter client description"},
                bigimg: {required: "Please select image", extension: "Only png,jpeg,jpg file allowed"},
            },
        });

        $(document).on('change', '#imgInp', function () {
            setTimeout(function () {
                $('#banner_title').focus();
                $('#imgInp').focus();
            }, 100)
        });

        function readURL(input) {

            if (input.files && input.files[0]) {

                var fileName = $("#imgInp").val();
                var idxDot = fileName.lastIndexOf(".") + 1;
                var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();

                if (extFile == "jpg" || extFile == "jpeg" || extFile == "png") {
                    $("#blah").show();
                } else {
                    $("#blah").hide();
                }
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function () {
            readURL(this);
        });
    }


    return {
        init: function () {
            handleBannerEdit();
        },
        list: function () {
            handleList();
        },
        add: function () {
            handleAdd();
        }
    }

}();


