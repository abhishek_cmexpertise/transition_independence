var timesheetDatatable = null;
var Mytimesheet = function () {

    var handletimesheet = function () {

        $('.genarate_report').click(function () {
            var month = $('.month').val();

            var search_duration = $('.search_duration').val();

            var year = $('.year').val();
            if (month != '' && search_duration != '' && year != '') {
                var url = employee_url + 'Employeetimesheet/manageTimesheet/' + month + '/' + search_duration + '/' + year;
                var url1 = employee_url + 'Employeetimesheet/managelog';
                $.ajax({
                    type: 'POST',
                    url: url1,
                    data: {month: month, search_duration: search_duration, year: year},
                    success: function (output)
                    {
                    }
                });

                timesheetDatatable = getDataTable('#my_timesheet', url, {dataTable: {}});
            } else {
                Toastr.init('warning', 'Please fill all the value');
            }
        });
    }

    return {
        init: function () {
            handletimesheet();
        },
    }
}();

