var ClientsDatatable = null;

var Clients = function () {

    var handleClients = function () {
        var url = employee_url + 'clients/manageClients/';
        ClientsDatatable = getDataTable('#client_table', url, {dataTable: {}});
    }

    var handleClientslog = function () {
        var url = employee_url + 'clients/manageClientslog/' + shiftlog_id;
        ClientsDatatables = getDataTable('#client_table', url, {dataTable: {}});
    }

    function getTimeDiff(startTime, endTime, that) {

        var sStartTime = startTime.split(':');
        var eEndTime = endTime.split(':');
        var errorCount = 0;

        /*Check Condition for start time always less then end time*/
        var timefrom = new Date();
        var temp = sStartTime;
        timefrom.setHours((parseInt(temp[0]) - 1 + 24) % 24);
        timefrom.setMinutes(parseInt(temp[1]));

        var timeto = new Date();
        var endTemp = eEndTime;

        timeto.setHours((parseInt(endTemp[0]) - 1 + 24) % 24);
        timeto.setMinutes(parseInt(endTemp[1]));
        if (timeto < timefrom) {
            errorCount++;
            that.closest('td').find('.error').html('start time should be smaller than end time!')
        } else {
            errorCount = 0;
            that.closest('td').find('.error').html('');
        }

        /*calculate Time Diffrent*/
        var startDate = new Date("January 1, 1970 " + startTime);
        var endDate = new Date("January 1, 1970 " + endTime);
        var timeDiff = Math.abs(startDate - endDate);

        if (errorCount == 0) {
            var hh = Math.floor(timeDiff / 1000 / 60 / 60);

            if (hh < 10) {
                hh = '0' + hh;
            }
            timeDiff -= hh * 1000 * 60 * 60;
            var mm = Math.floor(timeDiff / 1000 / 60);
            if (mm < 10) {
                mm = '0' + mm;
            }
            timeDiff -= mm * 1000 * 60;
            var ss = Math.floor(timeDiff / 1000);
            if (ss < 10) {
                ss = '0' + ss;
            }
            var tmDif = hh + ":" + mm + ":" + ss;
        } else {
            var tmDif = '00:00:00';
        }
        that.closest('td').parent('tr').next('tr').children('td:first').find('.total-time').val(tmDif);
        var tot_hour = 0;
        var tot_min = 0;
        for (var i = 1; i <= 5; i++)
        {
            if ($("#tot_time" + i).val())
            {
                if (isNaN($("#tot_time" + i).val()))
                {
                    tot_hour += parseInt(getTimeTotalHour($("#tot_time" + i).val()));
                }
            }

            if ($("#tot_time" + i).val())
            {
                tot_min += parseInt(getTimeTotalMin($("#tot_time" + i).val()));
                if (tot_min >= 60)
                {
                    tot_min = tot_min - 60;
                    tot_hour++;
                }
            }
        }

        if (tot_min < 10) {
            tot_min = "0" + tot_min;
        }
        $('.total_hour').val(tot_hour + ":" + tot_min + ":00");
    }
    function getTraveltime(travelTime)
    {
        var tot_hour = 0;
        var tot_min = 0;
        var tot_sec = 0;
        for (var i = 1; i <= 5; i++)
        {
            if ($("#travel_time" + i).val())
            {
                tot_hour += parseInt(getTimeTotalHour($("#travel_time" + i).val()));
            }
            if ($("#travel_time" + i).val())
            {
                tot_min += parseInt(getTimeTotalMin($("#travel_time" + i).val()));
                if (tot_min >= 60)
                {
                    tot_min = tot_min - 60;
                    tot_hour++;
                }
            }
            if ($("#travel_time" + i).val())
            {
                tot_sec += parseInt(getTimeTotalSec($("#travel_time" + i).val()));
                if (tot_sec >= 60)
                {
                    tot_sec = tot_sec - 60;
                    tot_min++;
                }
            }
        }
        if (tot_hour < 10) {
            tot_hour = "0" + tot_hour;
        }
        if (tot_min < 10) {
            tot_min = "0" + tot_min;
        }
        if (tot_sec < 10) {
            tot_sec = "0" + tot_sec;
        }
        $('.total_travel_time').val(tot_hour + ":" + tot_min + ":" + tot_sec);
    }
    function checkPreviousEndTime(startTime, that) {
        var previousEndTime = that.closest('td').parent('tr').prev('tr').prev('tr').children('td:first').find('.endTimepickerMultiple').val();
        if (typeof previousEndTime !== 'undefined') {
            var timefrom = new Date();
            var temp = previousEndTime.split(':');
            timefrom.setHours((parseInt(temp[0]) - 1 + 24) % 24);
            timefrom.setMinutes(parseInt(temp[1]));
            var timeto = new Date();
            var endTemp = startTime.split(':');
            timeto.setHours((parseInt(endTemp[0]) - 1 + 24) % 24);
            timeto.setMinutes(parseInt(endTemp[1]));
            if (timeto < timefrom) {
                that.closest('td').find('.error').html('start time should be smaller than previous end time!');
            } else {
                that.closest('td').find('.error').html('');
            }
        }
    }


    function getTimeTotalHour(time)
    {
        var tm = time;
        var time_hr = "";
        var time_min = "";
        var time_sec = "";
        var total_hrtime = "";
        var total_mintime = "";
        var total_sectime = "";

        time_hr = tm.split(":")[0];
        time_min = tm.split(":")[1];
        time_sec = tm.split(":")[2];

        total_hrtime = 1 * time_hr;
        total_mintime = 1 * time_min;
        total_sectime = 1 * time_sec;

        if (total_mintime >= 60)
        {
            total_hrtime = total_hrtime + 1;
        }

        if (total_hrtime >= 24)
        {
            total_hrtime = total_hrtime - 24;
        }

        if (total_sectime >= 60)
        {
            total_mintime = total_mintime + 1;
        }
        return  total_hrtime;
    }
    function getTimeTotalMin(time)
    {
        var tm = time;
        var time_hr = "";
        var time_min = "";
        var time_sec = "";
        var total_hrtime = "";
        var total_mintime = "";
        var total_sectime = "";

        time_hr = tm.split(":")[0];
        time_min = tm.split(":")[1];
        time_sec = tm.split(":")[2];

        total_hrtime = 1 * time_hr;
        total_mintime = 1 * time_min;
        total_sectime = 1 * time_sec;

        if (total_mintime >= 60)
        {
            total_mintime = total_mintime - 60;
        }
        if (total_sectime >= 60)
        {
            total_mintime = total_mintime + 1;
        }
        return  total_mintime;
    }

    function getTimeTotalSec(time)
    {
        var tm = time;
        var time_hr = "";
        var time_min = "";
        var time_sec = "";
        var total_hrtime = "";
        var total_mintime = "";
        var total_sectime = "";

        time_hr = tm.split(":")[0];
        time_min = tm.split(":")[1];
        time_sec = tm.split(":")[2];

        total_hrtime = 1 * time_hr;
        total_mintime = 1 * time_min;
        total_sectime = 1 * time_sec;

        if (total_sectime >= 60)
        {
            total_sectime = total_sectime - 60;
        }
        return  total_sectime;
    }

    return {
        init: function () {
            handleClients();
        },
        shift: function ()
        {
            handleClientslog();
        },
        handleaddClientlog: function ()
        {
            jQuery('#date').datepicker({
                autoclose: true,
                format: "yyyy-mm-dd",
                endDate: "today",
            });

            $('.startTimepickerMultiple').timepicker({
                showSeconds: true,
                showMeridian: false,
            }).on('changeTime.timepicker', function (e) {
                checkPreviousEndTime(e.time.value, $(this));
            });

            $('.travelTimepickerMultiple').timepicker({
                showSeconds: true,
                showMeridian: false,
            }).on('changeTime.timepicker', function (e) {
                var travelTime = $(this).closest('td').parent('tr').find('.travelTimepickerMultiple').val();
                getTraveltime(travelTime);
            });

            $('.endTimepickerMultiple').timepicker({
                showSeconds: true,
                showMeridian: false,
            }).on('changeTime.timepicker', function (e) {
                var startTime = $(this).closest('td').parent('tr').prev('tr').children('td:first').find('.startTimepickerMultiple').val();
                getTimeDiff(startTime, e.time.value, $(this));
            });
        },
    }
}();