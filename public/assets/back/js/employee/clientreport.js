var ClientreportDatatable = null;
var Clientreport = function () {

    var handleClientreport = function () {
        var url = employee_url + 'clientreport/manageClientreport/';
        ClientsDatatable = getDataTable('#client_table', url, {dataTable: {}});
    }
    $('body').on('click', '.allCheck', function () {
        if ($(this).is(':checked')) {
            $('.selectCheck').prop('checked', true);
        } else {
            $('.selectCheck').prop('checked', false);
        }
    });
    var checkedId = [];

    $('body').on('click', '.addbutt', function () {

        var status = $(this).data('status');
        $('.selectCheck').each(function () {

            if ($(this).is(':checked')) {
                if ($.inArray($(this).val(), checkedId) === -1) {
                    checkedId.push($(this).val());
                }
            } else {
                if ($.inArray($(this).val(), checkedId) !== -1) {
                    checkedId.splice($.inArray($(this).val(), checkedId), 1);
                }
            }
        });

        if (checkedId.length == 0) {
            Toastr.init('warning', 'Please check at least one check box');
        } else {
            var url = employee_url + "clientreport/clientreportexcels";
            var fullDate = new Date();
            var twoDigitMonth = ((fullDate.getMonth().length + 1) === 1) ? (fullDate.getMonth() + 1) : +(fullDate.getMonth() + 1);

            var currentDate = twoDigitMonth + "-" + fullDate.getDate() + "-" + fullDate.getFullYear();
            $.ajax({
                type: 'POST',
                url: url,
                data: {checkedId: checkedId},
                success: function (output)
                {
                    window.location.href = base_url + "public/Clients_Monthly_Report(" + currentDate + ").csv";
                }
            });
        }
    });

    var handleaddClientreport = function ()
    {
        $('body').on('change', '#report_client', function () {

            var client_id = $(this).val();
            var url = employee_url + 'clientreport/getclientdetail/';

            $.ajax({
                type: 'POST',
                url: url,
                data: {client_id: client_id},
                success: function (output)
                {
                    var data = JSON.parse(output);
                    console.log(data);
                    $('#client_uci').val(data[0].client_uci);
                    $('#client_dob').val(data[0].client_dob);
                    $('.client_name').val(data[0].consumer_name);
                }
            });
        });
    }
    return {

        init: function ()
        {
            handleClientreport();
        },

        handleaddClientreport: function ()
        {
            handleaddClientreport();
        },
    }
}();