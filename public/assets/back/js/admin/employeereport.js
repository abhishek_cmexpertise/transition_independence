var EmployeereportDatatable = null;
var EmployeeLogDatatable = null;

var Employeereport = function () {

    var handleEmployeereport = function () {
        var url = admin_url + 'Employeetimesheet/manageEmployeetimesheet/';
        EmployeereportDatatable = getDataTable('#employeereport_table', url, {dataTable: {}});
    }

    var handleLog = function () {
        var url = admin_url + 'Employeetimesheet/manageLog/' + shiftlog_id;
        EmployeeLogDatatable = getDataTable('#employeereport_table', url, {dataTable: {}});
    }

    return {
        init: function () {
            handleEmployeereport();
        },
        add: function () {
            handleLog();
        }
    }
}();

