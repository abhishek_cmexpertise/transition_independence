var ClientsDatatable = null;
var Clients = function () {

    var handleClients = function () {
        var url = admin_url + 'clients/manageClients/';
        ClientsDatatable = getDataTable('#client_table', url, {dataTable: {}});

        // Delete records
        $('body').on('click', '.deleteClient', function () {
            var href = $(this).attr('data-href');
            var footerContent = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\
                                <a href="' + href + '" class="btn btn-danger"><i class="fa fa-check">Delete</i></a>';
            $("#modelTitle").text('Delete Client');
            $("#modelContent").html("<h3>Are you sure you want to delete ?</h3>");
            $("#modelFooter").html(footerContent);
            $('#myModal').modal('show');
        });
    }

    var handleaddClients = function ()
    {
        $('#addclient').validate({

            rules: {
                client_uci: {required: true, number: true, minlength: 6, maxlength: 8},
                consumer_name: {required: true},
                address1: {required: true},
                address2: {required: true},
                client_dob: {required: true},
                emergency_contact: {required: true, minlength: 10, number: true},
                disability: {required: true},
                manager_name: {required: true},
                manager_phone: {required: true, minlength: 10, number: true},
                allocated_hours: {required: true},
            },
            messages: {
                client_uci: {required: "Please enter UCI#", number: "Please enter only number", minlength: "UCI# number should be minimum 6 charecter ", maxlength: "UCI# number should be maximum 8 charecter"},
                consumer_name: {required: "Please enter client name"},
                address1: {required: "Please enter address line 1"},
                address2: {required: "Please enter city, state, zip"},
                client_dob: {required: "Please select date of birth"},
                emergency_contact: {required: "Please enter emergency contact no", number: "Please enter valid emergency contact no", minlength: "Please emergency contact no should be 10 digit"},
                disability: {required: "Please enter disability"},
                manager_name: {required: "Please enter case manager name"},
                manager_phone: {required: "Please enter case manager phone number", number: "Please enter valid case manager phone number", minlength: "Please case manager phone no should be 10 digit"},
                allocated_hours: {required: "Please enter client's allocated hours"},
            },
        });
    }

    return {
        init: function () {
            handleClients();
        },
        handleaddClients: function ()
        {
            handleaddClients();
            jQuery('#date').datepicker({
                autoclose: true,
                format: "yyyy-mm-dd",
                endDate: "today",
            });
        },
    }
}();