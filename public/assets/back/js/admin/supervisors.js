var SupervisorsDatatable = null;
var Supervisors = function () {

    var handleSupervisors = function () {
        var url = admin_url + 'supervisors/manageSupervisors/';
        SupervisorsDatatable = getDataTable('#supervisor_table', url, {dataTable: {}});

        // Delete records
        $('body').on('click', '.deleteClient', function () {
            var href = $(this).attr('data-href');
            var footerContent = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\
                                <a href="' + href + '" class="btn btn-danger"><i class="fa fa-check">Delete</i></a>';
            $("#modelTitle").text('Delete Supervisor');
            $("#modelContent").html("<h3>Are you sure you want to delete ?</h3>");
            $("#modelFooter").html(footerContent);
            $('#myModal').modal('show');
        });
    }

    var handleaddSupervisors = function ()
    {
        $('#addsupervisor').validate({
            rules: {
                first_name: {
                    required: true,
                },
                middle_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                user_email: {
                    required: true,
                    email: true,
                    remote: {
                        url: 'verifyemail',
                        type: "post",
                        async: false,
                        data: {
                            check_email: function () {
                                return $("#user_email").val();
                            },
                            flag: '1'
                        }
                    }
                },
                dob: {
                    required: true,
                },
                social_security_no: {
                    required: true,
                    minlength: 9,
                    number: true
                }
            },
            messages: {
                first_name: {
                    required: "Please enter name",
                },
                middle_name: {
                    required: "Please enter middle name",
                },
                last_name: {
                    required: "Please enter last name",
                },
                user_email: {
                    required: "Please enter email",
                    email: "Please enter valid email",
                    remote: "your entered email already registered"
                },
                dob: {
                    required: "Please select date of birth",
                },
                social_security_no: {
                    required: " Please enter social security number",
                    minlength: "social security no should be 9 charecter",
                    maxlength: "social security no should be 9 charecter",
                    number: "Please enter valid social security number ",
                },
            },
        });
    }

    var handleeditSupervisors = function ()
    {
        $('#editsupervisor').validate({
            rules: {
                fresh_password: {
                    required: true,
                    minlength: 8,
                    maxlength: 12,
                },
                first_name: {
                    required: true,
                },
                middle_name: {
                    required: true,
                },
                last_name: {
                    required: true,
                },
                user_email: {
                    required: true,
                    email: true,

                },
                dob: {
                    required: true,
                },
                social_security_no: {
                    required: true,
                    minlength: 9,
                    number: true
                }
            },
            messages: {
                fresh_password: {
                    required: "Please enter password",
                    minlength: "Password should be minimum 8 charecter",
                    maxlength: "Password should be maximum 12 charecter",
                },
                first_name: {
                    required: "Please enter name",
                },
                middle_name: {
                    required: "Please enter middle name",
                },
                last_name: {
                    required: "Please enter last name",
                },
                user_email: {
                    required: "Please enter email",
                    email: "Please enter valid email",
                },
                dob: {
                    required: "select date of birth",
                },
                social_security_no: {
                    required: " Please enter social security number",
                    minlength: "social security no should be 9 charecter",
                    maxlength: "social security no should be 9 charecter",
                    number: "Please enter valid social security number ",
                },
            },
        });
    }

    return {
        init: function () {
            handleSupervisors();
        },
        handleaddSupervisors: function ()
        {
            handleaddSupervisors();
            jQuery('#date').datepicker({
                autoclose: true,
                format: "yyyy-mm-dd",
                endDate: "today",
            });
        },

        handleeditSupervisors: function ()
        {
            handleeditSupervisors();
            jQuery('#date').datepicker({
                autoclose: true,
                format: "yyyy-mm-dd",
                endDate: "today",
            });
        },
    }
}();