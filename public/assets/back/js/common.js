
function ajaxCall(url, data, callback) {
var html = '<div id="loading"><div class="spinner-wrapper load_screen"><div class="spinner"></div></div></div>';
$('body').append(html);
    $.ajax({
        type: 'POST',
        url: url,
        async: true,
        data: data,
        success: function (result) {
            $('#loading').remove();
            callback(JSON.parse(result));
            autoHideMgs();
        }
    });
}

function getFlashMessage(type, message) {
    var Alert = '<div class="alert alert-'
            + type
            + ' alert-dismissible text-center showhide" role="alert">'
            + '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
            + '<span aria-hidden="true">&times;</span>' + '</button>' + message
            + '</div>';

    return Alert;

}

function CKupdate() {
    for (instance in CKEDITOR.instances)
        CKEDITOR.instances[instance].updateElement();
}
if (typeof CKEDITOR !== 'undefined') {
    CKEDITOR.on('instanceCreated', function (ev) {
        CKEDITOR.dtd.$removeEmpty['a'] = 0;
    })
}


$(document).ready(function () {
    setTimeout(function () {
        $(".showhide").fadeOut(2000, function () {
            $(this).remove();
        });
    }, 3000)

});

function autoHideMgs(){
    setTimeout(function () {
        $(".showhide").fadeOut(2000, function () {
            $(this).remove();
        });
    }, 3000)
}


/* Init common ajax datatable method */

function getDataTable(tableID, ajaxPath, extraOption) {

    if (typeof extraOption === 'undefined') {
        extraOption = {};
    }
    var grid = new Datatable();
    var options = {
        src: $(tableID),
        onSuccess: function (grid, response) {
            // grid: grid object
            // response: json object of server side ajax response
            // execute some code after table records loaded
        },
        onError: function (grid) {
            // execute some code on network or other general error
        },
        onDataLoad: function (grid) {
            // execute some code on ajax data load
        },
        loadingMessage: 'Loading...',
        dataTable: {// here you can define a typical datatable settings from
					// http://datatables.net/usage/options
            responsive: false,
            // Uncomment below line("dom" parameter) to fix the dropdown
			// overflow issue in the datatable cells. The default datatable
			// layout
            // setup uses scrollable div(table-scrollable) with overflow:auto to
			// enable vertical scroll(see: assets/global/scripts/datatable.js).
            // So when dropdowns used the scrollable div should be removed.
            // "dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4
			// col-sm-12'f<'table-group-actions pull-right'>>r>t<'row'<'col-md-8
			// col-sm-12'pli><'col-md-4 col-sm-12'>>",
            "bStateSave": false, // save datatable state(pagination, sort,
									// etc) in cookie.

            "lengthMenu": [
                [10, 20, 50, 100, 150, -1],
                [10, 20, 50, 100, 150, "All"] // change per page values here
            ],
            "pageLength": 10, // default record count per page
            // "ajax": {
            // "url": ajaxPath, // ajax source
            // },
            "order": [
                [0, "desc"]
            ],// set first column as a default sort by asc

            "language": {
                "processing": '<div id="loading"><div class="spinner-wrapper load_screen"><div class="spinner"></div></div></div>',
                "infoFiltered": "",
            },
            "processing": true,
            "serverSide": true,
            "searching": true,
            
        }
    };
    options = $.extend(true, options, extraOption);
    if ('ajax' in options.dataTable == false) {
        options.dataTable.ajax = {"url": ajaxPath};
    }
    grid.init(options);

    grid.setAjaxParam("customActionType", "group_action");
    // grid.getDataTable().ajax.reload();
    grid.clearAjaxParams();
    return grid;
}


/*
 * Handel multiple active deactive and delete
 *
 */

function handleMultiple(moduleName) {

    $('body').on('click', '.allCheck', function () {

        if ($(this).is(':checked')) {
            $('.selectCheck').prop('checked', true);
        } else {
            $('.selectCheck').prop('checked', false);
        }
    });


    /*
	 * Handle commonly multiple active deactive and delete
	 */
    var checkedId = [];
    $('body').on('click', '.multipleHandle', function () {
        var status = $(this).data('status');
        $('.selectCheck').each(function () {
            if ($(this).is(':checked')) {
                if ($.inArray($(this).val(), checkedId) === -1) {
                    checkedId.push($(this).val());
                }
            } else {
                if ($.inArray($(this).val(), checkedId) !== -1) {
                    checkedId.splice($.inArray($(this).val(), checkedId), 1);
                }
            }
        });

        if (checkedId.length == 0) {
            $("#msg").html(getFlashMessage('danger', 'Please select at least one '+ moduleName.toLowerCase()));
            autoHideMgs();
            return false;
        } else {
            /*
			 * handle popup
			 */
            var text = '';
            var content = '';
            var footerContent = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\
                                <a href="javascript:;" class="btn btn-danger multipleAction"><i class="fa fa-check">Save</i></a>';
            if (status == '2') {
                text = 'Delete '+moduleName;
                content = '<h3>Are you sure you want to delete ?</h3>';
            } else if (status == '0') {
                text = 'Deactive '+moduleName;
                content = '<h3>Are you sure you want to Deactive ?</h3>';
            } else if (status == '1') {
                text = 'Active '+moduleName;
                content = '<h3>Are you sure you want to Active ?</h3>';
            }

            $("#modelTitle").text(text);
            $("#modelContent").html(content);
            $("#modelFooter").html(footerContent);
            $('.multipleAction').attr('data-id', checkedId);
            $('.multipleAction').attr('data-status', status);
            $('#myModal').modal('show');
        }
    });
}

function getDeleteConfirmation(){

	var footerContent = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\
        <a href="javascript:;" class="btn btn-danger deleteSingle"><i class="fa fa-check">Save</i></a>';
    	$("#modelTitle").text('Delete confirmation');
    	$("#modelFooter").html(footerContent);
    	var content = '<h3>Are you sure you want to delete ?</h3>';
    	$("#modelContent").html(content);
    	$('#myModal').modal('show');
}


function getpopup(title = "this is popuop",footerContent = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\
    <a href="javascript:;" class="btn btn-danger multipleAction"><i class="fa fa-check">Save</i></a>',content = 'content message here'){

	$("#modelTitle").text(title);
	$("#modelFooter").html(footerContent);
	$("#modelContent").html(content);
	$('#myModal').modal('show');
}


$(document).on('submit','form',function(){
	$(this).find(':input[type=submit]').prop('disabled', true);
});

$(document).on('click','.cancleButton',function(){
    var url = $(this).attr('data-href');
    window.location.href = url;
});

// $(document).ready(function(){
// var value = $('#chnage_color').val();
// var color = 'background : '+value;
// $('.btn-primary').attr('style',color);
// });


$('#chnage_color').on('change',function(){
    var value = $(this).val();
    var color = 'background : '+value;
    var color1 = 'color : '+value;
    $('.btn-primary').attr('style',color);
    $('.waves-effect.active').attr('style',color);
    $('.navbar-header').attr('style',color);
    $('.top-left-part').attr('style',color);
    $('.breadcrumb a').attr('style',color1);
});

var Toastr = function () {

    return {
        //main function to initiate the module
        init: function (type, title, message) {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "positionClass": "toast-top-center",
                "onclick": null,
                "showDuration": "1000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"

            }
            toastr[type](message, title);
        }

    };

}();

function setCookie(name,value,days) {

    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
function eraseCookie(name) {
    document.cookie = name+'=; Max-Age=-99999999;';
}
