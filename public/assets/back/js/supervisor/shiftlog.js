var ShiftlogDatatable = null;
var Shiftlog = function () {

    var handleShiftlog = function (url) {
        if(url == null){
            var url = supervisor_url + 'shiftlog/manageShiftlog/';
        }
        ShiftlogDatatable = getDataTable('#shiftlog_table', url, {dataTable: {}});
     
        $('.filtershiftlog').click(function(){
                var startDate = $('#startDate').val();
                var endDate = $('#endDate').val();
                var employeeName = $('.emp_name').val();
                var clientName = $('.client_name').val();
                if(startDate != '' && endDate !='' && employeeName != '' && clientName !=''){
                    $('#shiftlog_table').DataTable().destroy();
                    var url = supervisor_url + 'shiftlog/manageShiftlog/'+employeeName+'/'+clientName+'/'+startDate+'/'+endDate; 
                    handleShiftlog(url);
                }else{
                    Toastr.init('warning','Please fill all the value');
                }
        });
    }
    
    return {
        init: function () {
            handleShiftlog();
            var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $('#startDate').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
            iconsLibrary: 'fontawesome',
            minDate: today,
            maxDate: function () {
                return $('#endDate').val();
            }
        });
        $('#endDate').datepicker({
            uiLibrary: 'bootstrap4',
            format: 'yyyy-mm-dd',
            iconsLibrary: 'fontawesome',
            minDate: function () {
                return $('#startDate').val();
            }
        });
        },
    }
}();

