var MiscellaneousfileDatatable = null;
var Miscellaneousfile = function () {

    var handleMiscellaneousfile = function () {
        var url = supervisor_url + 'Miscellaneousfile/manageMiscellaneousfile/';
        MiscellaneousfileDatatable = getDataTable('#miscellaneousfile_table', url, {dataTable: {}});
        
        // Delete records
        $('body').on('click', '.deleteClient', function () {
            var href = $(this).attr('data-href');
            var footerContent = '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>\n\
                                <a href="' + href + '" class="btn btn-danger"><i class="fa fa-check">Delete</i></a>';
            $("#modelTitle").text('Delete Miscellaneous file');
            $("#modelContent").html("<h3>Are you sure you want to delete ?</h3>");
            $("#modelFooter").html(footerContent);
            $('#myModal').modal('show');
        });
        
    }
      var handleaddMiscellaneousfile = function()
    {
        $('#addMiscellaneousfile').validate({
            rules:{
               files:{
                required:true,
                extension: "docx|doc|pdf",
                filesize : 2000,
                }
            },
            messages:{
              files:{
                required:"input type is required",                  
                extension:"select input file format is docx , pdf",
                filesize :" please selected file size must be less then 2 MB"
                }
               },
        });
    }
    
   
    
    return {
        init: function () {
            handleMiscellaneousfile();
        },
        
         handleaddMiscellaneousfile: function(){
            handleaddMiscellaneousfile();
        },
       
    }
}();

