var Changepass = function () {

    var changepass = function () {

        $('#Changepass').validate({
            ignore: "hidden",
            rules: {
                currpass: {required: true, maxlength: 15, minlength: 8, remote: {url: 'verifyOldPass', type: 'POST', async: false, data: {check_pass: function () {
                                return $('#currpass').val();
                            }, flag: '1'}}},
                newpass: {required: true, maxlength: 15, minlength: 8, remote: {url: 'verifyOldPass', type: "post", async: false, data: {check_pass: function () {
                                return $("#newpass").val();
                            }, flag: '2'}}},
                confirmpass: {required: true, maxlength: 15, minlength: 8, equalTo: "#newpass"},
            },

            messages: {
                currpass: {required: "current password Required", remote: "Current password is incorrect"},
                newpass: {required: "new password Required", remote: "New Password should not same like Current Password "},
                confirmpass: {required: "confirm Password Required"},
            },
            submitHandler: function (form) {
                var url = $(form).attr('action');
                var data = $(form).serialize();

                ajaxcall(url, data, function (result) {
                    handleAjaxResponse(result);
                })
            }
        });
    }

    return {
        changepass: function () {
            changepass();
        },
    }
}();