var Employment_application = function () {

    var handleEmploymentApplication = function () {

        $('#employment_application').validate({
            rules: {
                l_name: {required: true},
                f_name: {required: true},
                mid_name: {required: true},
                street_name: {required: true},
                city_name: {required: true},
                state_name: {required: true},
                uemail: {required: true, email: true, },
                zip_name: {required: true},
                hphone: {required: true},
                cellphone: {required: true},
                ophone: {required: true},
                l_name_em: {required: true},
                f_name_em: {required: true},
                f_rel_em: {required: true},
                street_name_em: {required: true},
                city_name_em: {required: true},
                state_name_em: {required: true},
                uemail_em: {required: true, email: true, },
                zip_name_em: {required: true},
                ophone_em: {required: true},

                terms_con: {required: true},
                'upload[]': {required: true, extension: "docx|doc|pdf", filesize: 2000},
                
            },
            messages: {

                /*Personal Detail*/
                l_name: {required: "enter  your last name "},
                f_name: {required: " enter your first name"},
                mid_name: {required: " enter your middle name "},
                street_name: {required: "enter your street address"},
                city_name: {required: " enter your city"},
                state_name: {required: "enter your state"},
                uemail: {required: " enter your email", email: 'Please enter valid email address', },
                zip_name: {required: "enter area zip code "},
                celltxt1: {required: " your cellphone "},
                ophone: {required: " enter other phone number"},

                /*In case Emergency Please Notify*/
                l_name_em: {required: "enter your last name "},
                f_name_em: {required: "enter your first name"},
                f_rel_em: {required: "enter your relationship"},
                street_name_em: {required: "enter street address"},
                city_name_em: {required: "enter your city "},
                state_name_em: {required: " enter your state"},
                uemail_em: {required: " enter your email id", email: 'Please enter valid email address', },
                zip_name_em: {required: "enter area zip code "},
                hphone_em: {required: " enter your home phone number"},
                ophone_em: {required: " enter uout othor phone number"},

                /*term and condition*/
                terms_con: {required: "Please agree to our Terms of Service"},
                'upload[]': {required: "upload file", extension: "select input file format is docx , pdf", filesize: "please selected file size must be less then 2 MB"},
               
            },
            
        });
         var cap_str = $('#cap_str').val();
           
            var cap_char = $('#cap_char').val();
        
        $('.submit').click(function(){
            if(cap_str != cap_char)
            {
               
               document.getElementById("err_cap_char").innerHTML = '<span class="required caperror" style="color:red;">Your  Captcha code is required.</span>';
        p=1;
            }
        });
        
        $('#cap_char').keyup(function(){
         
            if(cap_str != cap_char)
            {
               
               document.getElementById("err_cap_char").innerHTML = '<span class="required caperror" style="color:red;">Your entered code is incorrect.</span>';
        p=1;
            }
           
          
        });
    }

    return {
        init: function () {
            handleEmploymentApplication();
        }
    }
}();