var Contact = function () {

    var handlecontact = function () {

        $("#contact-form").validate({
            rules: {
                user_firstname: {required: true},
                user_lastname: {required: true},
                user_email: {required: true, email: true, },
                user_message: {required: true},
            },
            messages: {
                user_firstname: {required: "Please enter first name"},
                user_lastname: {required: "Please enter last name"},
                user_email: {required: "Please enter email address", email: 'Please enter valid email address'},
                user_message: {required: "Please enter your message"},
            },
            submitHandler: function (form) {
                var url = $(form).attr('action');
                var data = $(form).serialize();
                ajaxcall(url, data, function (result) {
                    handleAjaxResponse(result);
                })
            }
        });

    }

    return {
        init: function () {
            handlecontact();
        }
    }

}();