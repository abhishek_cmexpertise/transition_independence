$(document).ready(function(){
var words = $(".heading").text().split(" ");
$(".heading").empty();
$.each(words, function(i, v) {
$(".heading").append($("<span>").text(v));
});
//
var words = $(".heading-inn").text().split(" ");
$(".heading-inn").empty();
$.each(words, function(i, v) {
$(".heading-inn").append($("<span>").text(v));
});

//
$.mCustomScrollbar.defaults.scrollButtons.enable=true; //enable scrolling buttons by default
$(".contentscroll").mCustomScrollbar({theme:"dark"});
//
$(".skippopup").click(function(){
	//$(".overlaymain").fadeOut();
	location.reload(); 	
});	


$(".forgetpass").click(function(){
	$(".form-box-pop-up").fadeOut();
	$(".lost-password-box").fadeIn();		
});

$(".login-button").click(function(){
	$(".overlaymain").fadeIn();
	$(".lost-password-box").fadeOut();
	$(".form-box-pop-up").fadeIn();
	
});
$('.healp-row').last().addClass('healp-row-last');
$('.table-box-5 .form-row-border').last().addClass('form-row-border-last');
$('.table-box-6 .form-row-border').last().addClass('form-row-border-last');
$('.table-box-7 .form-row-border').last().addClass('form-row-border-last');
$('.testimonial-row').last().addClass('testimonial-row-last');
$('.testimonial-row').first().addClass('testimonial-row-first');


//
$('.banner-images-wrapper').bxSlider({
  auto: true,
  mode: 'fade',
  pause: 4000,
  speed: 600
});
//

$(".lightbox").colorbox({rel:'lightbox'});
$(".video-popup").colorbox({iframe:true, innerWidth:640, innerHeight:390});
//
var ht=0;
 $('.mobMenu').click(function(){
	 if(ht==1){
	 $('header nav').slideUp();
	 ht=0;
	 }else{
		 $('header nav').slideDown();
		 ht=1; 
	 }
 });
 
//
jQuery('.numbersOnly').keyup(function () { 
    this.value = this.value.replace(/[^0-9\.]/g,'');
});

});
