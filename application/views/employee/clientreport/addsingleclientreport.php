<style>

    
    .main{padding: 25px; width: 70%;margin:0px auto;background: white;border: 1px solid #0277bd;}
 .heading1{font-size: 20px;
    border: 1px solid #0277bd;
    padding: 20px;} 
 .table-section {
    padding-left: 10px;
    padding-top: 10px;
}   
 .form-rowsrt-wrap {
    float: left;
    width: 100%;
    margin-bottom: 10px;
    margin-top: 10px}
 .form-rowsrt-wrap .form-row {
    float: left;
    width: 19%;
    margin-right:65px;
}
 form.monthly-report input[type=text] {
    background: none;
    border: 0;
    border-bottom: 1px solid #000;
    text-align: center;
 }
    .rowfoemm {
    float: left;
    padding: 5px 0;
    width: 100%;
    margin-top: 22px;
}
.rowfoemm h2 {
    font-size: 18px;
    text-align: center;
    color: #111;
    font-weight: bold;
    font-size: 22px;
}
.object-1{  float: left;
    padding: 5px 0;
    width: 100%;
    margin-top: 22px;}
.object-1 h2{text-align: left;
    background: #edf2f5;
    padding: 5px 15px;
    font-size: 22px;
    text-transform: uppercase}

.form-box-innrer {
    float: left;
    font-size: 14px;
    width: 100%;
    margin-top: 10px;
    margin-bottom: 10px;
    line-height: 20px;
}
 .form-box-innrer h2{   font-size: 16px;
    color: #111;
    font-weight: bold;
    padding-bottom: 8px;
    }
    form.monthly-report textarea {
    background: none;
    border: 1px solid #ccc;
    border-radius: 5px;
    height: 100px;
    width: 100%;
    padding: 10px;
    margin-top: 10px;
    float: left;
    resize: vertical}
    .rowfoemm.text-left.underline{
       
    }
    .rowfoemm.text-left.underline h2 {
    font-size: 16px;
    text-decoration: underline;
    background: none;
    text-align: left;
    text-transform: uppercase;}
    .input.butt {
    padding: 5px 20px;
    margin: 0 10px;
    border: 0;
    cursor: pointer;
    border:none;
}   
.butt {
    background: #0277bd;
    color: #fff;
    font-size: 16px;}
</style>

<div class="main">
            <div class="row">
                <div class="col-lg-12">  
                <h3 class="heading1">Client's Monthly Report</h3>
                </div>
                </div>
                            <div class="row bg-row">
                    
                </div>
                <div class="table-section">
                    <form class="monthly-report" style="margin-bottom:20px;" action="<?php echo employee_url()."Clientreport/addsingleclientreport"?>" method="post" enctype="multipart/form-data" onsubmit="return form_validation();">
                        <input type="hidden" name="action" value="add">
                    
                        <div class="form-rowsrt-wrap">
                               <input type="hidden" name="client_id" id="" value="<?php echo $clientreport[0]->client_id ?>">
                                <div class="form-row"><label>Client:</label><input type="text" name="client_name" id="client_name" value="<?php echo $clientreport[0]->consumer_name?>" readonly=""></div>
                                                            
                            <div class="form-row"><label>U.C.I.:</label><input type="text" name="client_uci" id="client_uci" readonly="" value="<?php echo $clientreport[0]->client_uci?>"></div>
                            <div class="form-row"><label>Date of Birth:</label><input type="text" name="client_dob" id="client_dob" readonly="" value="<?php echo date('m/d/Y',strtotime($clientreport[0]->client_dob));?>"></div>
                            <div class="form-row"><label>Date of Report:</label><input type="text" name="report_date" id="report_date" value="<?php echo date('m/d/Y');?>" readonly=""></div>
                        </div>

                        <div class="rowfoemm">
                            <h2>Monthly Report</h2>
                            <h2>ILS Objectives</h2>
                        </div>
                        
                        <div class="object-1"><h2>Objective # 1:</h2></div>
						
						<div class="form-box-innrer">
                        <h2>Comment for Objective # 1:</h2>
                         <textarea name="comment1" id="comment1" required=""></textarea>
                        </div>
						
												
                        <div class="form-box-innrer">
                        <h2>Current Status:</h2>
                        (how is <input class="input-line" readonly="" type="text" value="<?php echo $clientreport[0]->consumer_name?>"> (Client’s Name) doing on this goal? -  If there is success, explain what happened.  For services we did not get to or are a challenge explain the barriers)
                        </div>
						
                        <div class="form-box-innrer">
                        <h2>Services for Objective # 1:</h2>
                        insert services from Objective # on the ISP)  (- instructor gives progress on services or skills - Met, Partially Met, Not Met???)
                        <textarea name="objective1" id="objective1" required=""></textarea>
                        </div>
                        
                        
                        
                        <div class="object-1"><h2>Objective # 2:</h2></div>
						
						<div class="form-box-innrer">
                        <h2>Comment for Objective # 2:</h2>
                         <textarea name="comment2" id="comment2" required=""></textarea>
                        </div>
						
						
						
                        <div class="form-box-innrer">
                        <h2>Current Status:</h2>
                        (how is <input class="input-line" readonly="" type="text" value="<?php echo $clientreport[0]->consumer_name?>"> (Client’s Name)  doing on this goal? -  If there is success, explain what happened.  For services we did not get to or are a challenge explain the barriers)
                        </div>
						
												
                        <div class="form-box-innrer">
                        <h2>Services for Objective # 2:</h2>
                        (insert services from Objective # on the ISP)  (- instructor gives progress on services or skills - Met, Partially Met, Not Met???)
                        <textarea name="objective2" id="objective2"></textarea>
                        </div>
                        
                        
                        
                        <div class="object-1"><h2>Objective # 3:</h2></div>
						
						
						<div class="form-box-innrer">
                        <h2>Comment for Objective # 3:</h2>
                         <textarea name="comment3" id="comment3" required=""></textarea>
                        </div>
						
                        <div class="form-box-innrer">
                        <h2>Current Status:</h2>
                        (how is <input class="input-line" readonly="" type="text" value="<?php echo $clientreport[0]->consumer_name?>"> (Client’s Name)  doing on this goal? -  If there is success, explain what happened.  For services we did not get to or are a challenge explain the barriers)
                        </div>
                        
						
												
                        <div class="form-box-innrer">
                        <h2>Services for Objective # 3:</h2>
                        (insert services from Objective # on the ISP)  (- instructor gives progress on services or skills - Met, Partially Met, Not Met???)
                        <textarea name="objective3" id="objective3"></textarea>
                        </div>
                        
                        
                        
                        <div class="object-1"><h2>Objective # 4:</h2></div>
						
						<div class="form-box-innrer">
                        <h2>Comment for Objective # 4:</h2>
                         <textarea name="comment4" id="comment4" required=""></textarea>
                        </div>
						
						
                        <div class="form-box-innrer">
                        <h2>Current Status:</h2>
                        (how is <input class="input-line" readonly="" type="text" value="<?php echo $clientreport[0]->consumer_name?>"> (Client’s Name)  doing on this goal? -  If there is success, explain what happened.  For services we did not get to or are a challenge explain the barriers)
                        </div>
						
						
						
						
						
						
                        <div class="form-box-innrer">
                        <h2>Services for Objective # 4:</h2>
                        (insert services from Objective # on the ISP)  (- instructor gives progress on services or skills - Met, Partially Met, Not Met???)
                        <textarea name="objective4" id="objective4"></textarea>
                        </div>
                        
                        
                        
                        <div class="object-1"><h2>Objective # 5:</h2></div>
						
						<div class="form-box-innrer">
                        <h2>Comment for Objective # 5:</h2>
                         <textarea name="comment5" id="comment5" required=""></textarea>
                        </div>
						
						
                        <div class="form-box-innrer">
                        (how is <input class="input-line" readonly="" type="text" value="<?php echo $clientreport[0]->consumer_name?>"> (Client’s Name)  doing on this goal? -  If there is success, explain what happened.  For services we did not get to or are a challenge explain the barriers)
                        </div>
						
						
						
						
						
                        <div class="form-box-innrer">
                        <h2>Current Status including progress and barriers to progress: </h2> 
                        (how is Carolyn doing on this goal? -  If there is success, explain what happened.  For services we did not get to or are a challenge explain the barriers)
                        <textarea name="objective5" id="objective5"></textarea>
                        </div>
                        
                        <div class="rowfoemm text-left underline"><h2>Additional Notes:  (this is where you can put in risk factors, potential new goals, and please feel free to propose new steps to achieve the above goals if you see an opportunity. )</h2></div>
						<div class="form-box-innrer">
                        <h2>Additional Comment</h2>
                         <textarea name="comment" id="comment" required=""></textarea>
                        </div>
                        <input type="submit" value="Save" class="butt">
                    </form>
                    <div class="clr"></div>
                </div>
                                
        </div>
