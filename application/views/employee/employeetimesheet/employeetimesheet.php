
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">My Timesheet</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                            <a href=<?php echo employee_url().'dashboard'?>> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href=<?php echo employee_url().'dashboard'?>>Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">My Timesheet</a>
                        </li>
                    </ul>
                </div>
            </div>
          
           <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
               
                <div>
                    <form method="post"  >
                          <div class="page-header card">
                        <div class="row" style="padding: 25px;">   
                                    <div class="form-group col-xl-3 col-lg-4 col-md-4"  style="display:flex;margin-top:15px;justify-content: center">
                                    <label style="padding-right:10px">Select Month </label>
                                    <select class="form-control month" name="month">
                                        <option value="January" <?php if(date('F',strtotime('first day of last month'))=="January"){echo "selected";}?>>January</option>
                                        <option value="February" <?php if(date('F',strtotime('first day of last month'))=="February"){echo "selected";}?>>February</option>
                                        <option value="March" <?php if(date('F',strtotime('first day of last month'))=="March"){echo "selected";}?>>March</option>
                                        <option value="April" <?php if(date('F',strtotime('first day of last month'))=="April"){echo "selected";}?>>April</option>
                                        <option value="May" <?php if(date('F',strtotime('first day of last month'))=="May"){echo "selected";}?>>May</option>
                                        <option value="June" <?php if(date('F',strtotime('first day of last month'))=="June"){echo "selected";}?>>June</option>
                                        <option value="July" <?php if(date('F',strtotime('first day of last month'))=="July"){echo "selected";}?>>July</option>
                                        <option value="August" <?php if(date('F',strtotime('first day of last month'))=="August"){echo "selected";}?>>August</option>
                                        <option value="September" <?php if(date('F',strtotime('first day of last month'))=="September"){echo "selected";}?>>September</option>
                                        <option value="October" <?php if(date('F',strtotime('first day of last month'))=="October"){echo "selected";}?>>October</option>
                                        <option value="November" <?php if(date('F',strtotime('first day of last month'))=="November"){echo "selected";}?>>November</option>
                                        <option value="December" <?php if(date('F',strtotime('first day of last month'))=="December"){echo "selected";}?>>December</option>
                                    </select>    
                                    </div >
                                    
                                       <div class="form-group col-xl-3 col-lg-4 col-md-4"  style="display:flex;margin-top:15px;justify-content: center">
                                    <label style=" padding-right:10px">Select Half  </label>
                                    <select class="form-control search_duration" name="search_duration">
                                            <option value="1">1st to 15th</option>
                                            <option value="2">16th to end of month</option>
                                        </select>
                                    </div >
                                    <div class="form-group col-xl-3 col-lg-4 col-md-4"  style="display:flex;margin-top:15px;justify-content: center">
                                    <label style="padding-right:10px">Select Year </label>
                                    <select class="form-control year" name="year">
                                      <option value="<?php echo date('Y');?>"><?php echo date('Y');?></option>
                                      <option value="<?php echo date("Y",strtotime("-1 year"));?>"><?php echo date("Y",strtotime("-1 year"));?></option>
                                    </select>
                                    </div>
                                      <div class="row bg-row col-xl-3 col-lg-12 col-md-12" style="margin-top:15px;display:flex ;justify-content: center">
                                        <div class="downloadbutton">
                                            <input type="button" class="btn btn-primary genarate_report"  name="genarateReport"  value="Genrate Report"> 
                                        </div>
                                   </div>
                            </div>  
                       </div>
                    </form>
                  
                </div>
                 <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="my_timesheet">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th>ID</th>
                                                <th>Submission Date</th>
                                                <th>Report Month</th>
                                                <th>Report Year</th>     
                                                <th>Sub-Total Hours</th>
                                                <th>Sub-Total Travel Time</th>
                                                <th>Report Download</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
  </div>
 </div>
</div>

