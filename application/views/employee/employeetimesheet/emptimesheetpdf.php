<?php
$logo = base_url() . "public/assets/front/images/logo.png";
$user_fullname = $shiftlogreport[1][0]->first_name . ' ' . $shiftlogreport[1][0]->last_name;
$pay_period = $shiftlogreport[0];
?>

<html ><body style="width:700px;font-family: Roboto, 'Segoe UI', Tahoma, sans-serif;
            margin-left:0px" >
        <table style="border-collapse: collapse; margin-bottom:20px; width:700px; border-spacing: 0;">
            <tr><td align="center"><img src= "public/assets/front/images/logo.png"></td></tr>
        </table>
        <table style="border-collapse: collapse; margin-bottom:20px; width:700px; border-spacing: 0;">
            <tr><td style="border:1px solid #000; padding:5px; width:700px;">ILS Instructor Name<?php echo " :- ".$user_fullname ?></td></tr>
            <tr><td style="border:1px solid #000; padding:5px; width:700px;">Pay Period<?php echo " :- ".$pay_period ?></td></tr>
            <tr><td style="border:1px solid #000; padding:5px; width:700px;">I acknowledge that the above hours are a true and accurate reflection of hours worked:</td></tr>
        </table>
        <?php
        for ($i = 0; $i < count($shiftlogreport[2]); $i++) {
            ?>
            <table style="border-collapse: collapse; width:300px; margin-bottom:20px; border-spacing: 0; font-family: arial;">
                <tr>
                    <td style="border:1px solid #000; padding:2px;" colspan=17>Client Name:<?php echo "  ".$shiftlogreport[2][$i][0]->consumer_name ?></td>
                </tr>
                <tr>
                    <td style="border-spacing: 0;" cellpadding="0" cellspacing="0">
                        <table style="border-collapse:collapse; border-spacing: 0;">
                            <tr><td style="border:1px solid #000; padding:4px 2px; width:30px; background:#bfbfbf;">Date</td></tr>
                            <tr><td style="border:1px solid #000; padding:4px 2px; width:30px;  background:#bfbfbf;">Hours</td></tr>
                            <tr><td style="border:1px solid #000; padding:4px 2px; width:30px;  background:#bfbfbf;">Travel:</td></tr>
                        </table>
                    </td>
                    <?php
                    $start_day = $shiftlogreport['start_day'];
                    $end_day = $shiftlogreport['end_day'];
                    for ($j = $start_day; $j <= $end_day; $j++) {
                        $lastArray = end($shiftlogreport[3][$i][$j]);
                        ?>
                        <td>
                            <table style="border-spacing: 0; " cellpadding="0" cellspacing="0">
                                <tr><td align="center" style="border:1px solid #000; padding:4px 2px; width:30px; border-spacing: 0; border-left:0px;"><?php echo $j; ?></td></tr>	
                                <tr><td align="center" style="border:1px solid #000; padding:4px 2px; width:30px; border-spacing: 0; border-left:0px;"><?php
                                        if (@$lastArray->total_hours) {
                                            echo @$lastArray->total_hours;
                                        } else {
                                            echo ' - ';
                                        }
                                        ?></td></tr>
                                <tr><td align="center" style="border:1px solid #000; padding:4px 2px; width:30px;  border-spacing: 0; border-left:0px;"><?php
                                        if (@$lastArray->total_travel_time && @$lastArray->total_travel_time != "00:00:00") {
                                            echo @$lastArray->total_travel_time;
                                        } else {
                                            echo ' - ';
                                        }
                                        ?></td></tr>
                            </table>
                        </td>
                <?php
            }
            ?>
                </tr>			
            </table>
    <?php
}
?>
        <table style="width:700px; margin-top:20px; border-spacing: 0;" cellpadding="0" cellspacing="0">
            <tr>
                <td style="border:1px solid #000; padding:5px;  width:166px;">Signature : </td>
                <td style="border:1px solid #000; padding:5px; width:166px;">Date : </td>
                <td style="border:1px solid #000; padding:5px; width:167px;">ILS Instructor Total Hours : <?php echo $shiftlogreport[1][0]->subtotal_hours ?></td>
                <td style="border:1px solid #000; padding:5px; width:167px;">Total Travel Time :<?php echo $shiftlogreport[1][0]->subtotal_travel_time_google; ?></td>
            </tr>
        </table>
    </body></html>