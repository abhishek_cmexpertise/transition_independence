<script>
    var shiftlog_id = "<?php echo $id?>";
</script>

<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Client shiftlog</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                            <a href="index.html"> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Client shiftlog</li>
                    </ul>
                </div>
            </div>
            
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">
                                      <div class="row bg-row" style="float: right;padding-bottom: 15px;padding-right: 15px;">
                                        <a class="btn btn-primary addbutt" href=<?php echo employee_url().'clients/shiftlog_form/'.$id?>>Add New</a>  
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" id="client_table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Date</th>
                                                <th>Client Name</th>
                                                <th>Staff Name</th>
                                                <th>Hours Worked</th>
                                                <th>Total Travel Time</th>
                                                <th>Show</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>