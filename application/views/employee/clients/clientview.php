<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
           
            <br>
            <div class="box box-primary box-primary-custom">
               
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="portlet light portlet-fit ">
                            <div class="portlet-body">
                                  <div  >
                                    <table class="table table-bordered " id="dashboard_table">
                                        <thead class="thead-dark"> <th colspan="3" ><h4><center>Client</h4></center></th></thead>
                                      
                                            <tr>
                                                <th>UCI#</th>
                                                <td><?php echo $singleClient[0]->client_uci?></td>
                                                
                                            </tr>
                                            <tr>
                                                <th>Client Name</th>
                                                 <td><?php echo $singleClient[0]->consumer_name?></td>
                                            </tr>
                                            <tr>
                                                <th>Address Line 1</th>
                                                 <td><?php echo $singleClient[0]->address1?></td>
                                            </tr>
                                            <tr>
                                                <th>Address Line 2 </th>
                                                 <td><?php echo $singleClient[0]->address2?></td>
                                            </tr>
                                            <tr>
                                                <th>Date Of Birth</th>
                                                <td><?php echo date('m/d/Y', strtotime($singleClient[0]->client_dob))?></td>
                                            </tr>
                                            <tr>
                                                <th>Emergency Contact</th>
                                                 <td><?php echo $singleClient[0]->emergency_contact?></td>
                                            </tr>
                                            <tr>
                                                 <th>Disability</th>
                                                  <td><?php echo $singleClient[0]->disability?></td>
                                            </tr>
                                             <tr>
                                                 <th>Case Manager Name</th>
                                                  <td><?php echo $singleClient[0]->manager_name?></td>
                                            </tr>
                                             <tr>
                                                 <th>Case Manager Phone Number</th>
                                                  <td><?php echo $singleClient[0]->manager_phone?></td>
                                            </tr>
                                      
                                    </table>
                                </div>
                                   <div class="row bg-row" style="margin-bottom: 50px">
             
                                       <a class="btn btn-primary btn-md addbutt" href=<?php echo employee_url().'clients'?>>close</a>  
                                       <a class="btn btn-primary btn-md addbutt" style="margin-left: 300px" href=<?php echo employee_url().'clients/clientshiftlog/'.$id?>>ILS Shift Log</a>  
                                  </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

:	
