<style>
        
    label.error{
        color:red;
        font-size: 12px;
        padding-top: 12px;
    }
    
    .shiftlog {
        padding-bottom: 25px;
    }
    
    .shiftlog table tr td label {
        padding-bottom: 5px;
        display: block;
    }
    
    .shiftlog {
        max-width: 1215px;
        margin: 0 auto;
        width: 100%;
    }
    
    .shiftlog h4 {
        margin: 30px 0px;
        display: inline-block;
        text-align: center;
        font-size: 28px;
        width: 100%;
    }
    
    .shiftlog .table > thead > tr > th,
    .shiftlog .table > tfoot > tr > td {
        border: 0;
        border-bottom: 1px solid #0071f3;
        width: 33.33%;
    }
    
    .shiftlog .table > thead > tr > th label {
        display: block;
    }
    
    .shiftlog .table tfoot tr td.full-td {
        text-align: center;
    }
    
    .shiftlog .table tfoot tr td.full-td a {
        padding: 5px 20px;
        margin: 0 10px;
        border: 0;
        cursor: pointer;
        display: inline-block;
        background: linear-gradient(to right, #004ef9, #79a3ff) !important;
        color: #fff;
        font-size: 16px;
        text-transform: capitalize;
    }
    
    .shiftlog .table-bordered td,
    .shiftlog .table-bordered th,
    .shiftlog .table-bordered {
        border: 1px solid #0071f3;
        background: #fff;
    }
    
    .shiftlog table tr td textarea {
        scroll-behavior: 0;
        resize: none;
        height: 100px;
        width: 100%;
        border: 0;
    }
    
    .shiftlog table tr td textarea:focus {
        outline: 0;
        box-shadow: none;
    }
    
    input {
        border: none;
    }
    span.error{
        color: red;
    }
</style>

<div class="shiftlog">
    <h4>Shift log</h4>
    <form method="POST" id="addShiftlog" action=<?php echo employee_url().'clients/addShiftlog/'?>>
              <table class="table table-bordered">
        <thead>
                  <input type="hidden" name="client_id" value="<?php echo $clientdata[0]->client_id;?>">
            <tr>
                <?php
                $current_month = date('m');
                $shiftlog_month = date('m', strtotime($clientdata[0]->shiftlog_date));

                if ($current_month == $shiftlog_month) {
                    $time = $clientdata[0]->remaining_hours;
                    
                    ?>
                    <th colspan="3">Remaining Allocated Time :
                        <?php echo $time; ?>
                    </th>
                    <?php
                } else {
                    $time = $clientdata[0]->allocated_hours;
                    ?>
                        <th colspan="3">Remaining Allocated Time :
                            <?php echo $time; ?>
                        </th>
                        <?php
                }
                foreach ($clientdata as $clientdatas) {

                }
                ?>
            </tr>
           <tr>
                <th>
                    <label>Date</label>
                    <input class="form-control" style="border:none;padding: 0px;" placeholder="select date" type="text" name="shiftlog_date" id="date">

                </th>
                <th>
                    <label>Client Name:</label>
                    <?php echo $clientdata[0]->consumer_name; ?>
                </th>
                <th>
                    <label>Staff Name:</label>
                    <?php echo $this->session->userdata['valid_login']['firstname'] . ' ' . $this->session->userdata['valid_login']['lastname']; ?>
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td></td>
                <td>Be sure to address Client Goals, Location, and Activities in the narrative.</td>
                <td>Travel Time</td>
            </tr>
            <?php
            for ($i = 1; $i <= 5; $i++) {
                ?>
            <tr class="new_time">
                    <td>
                        <label>Time Start:</label>
                         
                         <div class="input-group bootstrap-timepicker timepicker">
                            <input type="text" class="form-control input-small startTimepickerMultiple" name="start_time[]">
                            <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                        </div>
                        <span class="error"></span>
                    </td>
                    <td rowspan="3">
                        <textarea placeholder="Enter Client Description" name="consumer_detail[]" ></textarea>
                    </td>
                    
                    <td rowspan="3">
                        <div class="input-group bootstrap-timepicker timepicker">
                            <input type="text" class="form-control input-small travelTimepickerMultiple"  id="travel_time<?php echo $i?>" name="travel_time[]">
                            <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Time End:</label>

                        <div class="input-group bootstrap-timepicker timepicker">
                            <input type="text" class="form-control input-small endTimepickerMultiple" name="end_time[]">
                            <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
                        </div>
                        <span class="error"></span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Total Time</label>
                            <input type="text" name="total_time[]" readonly="" class="total-time" id="tot_time<?php echo $i?>">
                    </td>
                </tr>
                <?php
            }
            ?>
        </tbody>
        <tfoot>
            <tr>
                <td>
                    <label>Hours Worked:</label>
                    <input type="text" name="tot_hour" readonly="" class="total_hour">
                </td>
                <td>
                    <label>Total Travel Time:</label>
                      <input type="text" name="tot_travel_time" readonly="" class="total_travel_time">
                </td>
                <td>
                    <label>Staff Signature:</label>
                    <?php echo $this->session->userdata['valid_login']['firstname'] . ' ' . $this->session->userdata['valid_login']['lastname']; ?>
                </td>
            </tr>
        </tfoot>
    </table>
   
  <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="shitlog-btn" style="text-align:center">
                    <input class="btn btn-primary addbutt" type="submit" name="" value="Save">
                    <a href="<?php echo employee_url() . 'clients/clientshiftlog/'.$id ?>" class="btn btn-primary addbutt">Close</a>
                </div>
            </div>
        </div>
    </div>
         </form>
</div>