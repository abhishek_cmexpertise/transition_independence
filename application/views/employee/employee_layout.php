<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('employee/includes/header'); ?>

  <body>
 
	  <div class="fixed-button">
		
	  </div>
       <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">
<!-- Start Top Bar -->
         
          <?php $this->load->view('employee/includes/body_header'); ?>
          
<!--End  Top Bar -->
            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <!-- Start Sidebar -->
                     <?php $this->load->view('employee/includes/sidebar'); ?>
                    <!-- End Sidebar -->
                    <div class="pcoded-content">
                      <!-- Start Content -->
                <?php $this->load->view($page); ?>
                      <!-- End Content -->
                </div>
            </div>
        </div>

 
<!-- Required Jquery -->
 <?php $this->load->view('employee/includes/footer'); ?>

</body>

</html>
