
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Training Time Sheet</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                            <a href=<?php echo admin_url().'dashboard'?>> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href=<?php echo admin_url().'dashboard'?>>Dashboard</a>
                        </li>
                        <li class="breadcrumb-item"><a href="#!">Training Time Sheet</a>
                        </li>
                    </ul>
                </div>
            </div>
        
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">                          
<h1 align="center">Training Time Sheet</h1> 
<br>
              <p style="text-align:center;"><a href=<?php echo base_url().'public/assets/front/pdf/traning_timesheet.pdf'?> target="_blank">Click here to download the document.</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

