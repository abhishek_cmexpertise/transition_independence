<!DOCTYPE html>
<html lang="en"> 
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title><?= PROJECT_NAME ?> Admin</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="<?= base_url(); ?>public/assets/cms/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>public/assets/cms/css/metro.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>public/assets/cms/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>public/assets/cms/css/style.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>public/assets/cms/css/style_responsive.css" rel="stylesheet" />
        <link href="<?= base_url(); ?>public/assets/cms/css/style_brown.css" rel="stylesheet" id="style_color" />
        <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/uniform/css/uniform.default.css" />
        <link rel="shortcut icon" href="" />
        <style>
            .login {background-color: #27A9E3 !important;}
        </style>
    </head>
    <!-- END HEAD -->
    
    <!-- BEGIN BODY -->
    <body class="login">
        <!-- BEGIN LOGO -->
        <div class="logo">
            <h3 style="color:#FFFFFF"><?= PROJECT_NAME ?> Admin</h3>
        </div>
        <!-- END LOGO -->
        
        <!-- BEGIN LOGIN -->
        <?php $this->load->view($page); ?>
        <!-- END LOGIN -->

        <!-- BEGIN COPYRIGHT -->
        <div class="copyright"><?= date('Y') ?> &copy; <?= PROJECT_NAME ?></div>
        <!-- END COPYRIGHT -->
        
        <!-- BEGIN JAVASCRIPTS -->
        <script src="<?= base_url(); ?>public/assets/cms/js/jquery-1.8.3.min.js"></script>
        <script src="<?= base_url(); ?>public/assets/cms/bootstrap/js/bootstrap.min.js"></script>  
        <script src="<?= base_url(); ?>public/assets/cms/uniform/jquery.uniform.min.js"></script> 
        <script src="<?= base_url(); ?>public/assets/cms/js/jquery.blockui.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?= base_url(); ?>public/assets/cms/js/app.js"></script>
        
        <?php
        if (!empty($js)) {
            foreach ($js as $value) {
                ?>
                <script src="<?php echo base_url(); ?>public/assets/cms/javascripts/<?php echo $value ?>"
                type="text/javascript"></script>
                <?php
            }
        }
        ?>
        
        <script>
            jQuery(document).ready(function () {
            <?php
            if (!empty($init)) {
                foreach ($init as $value) {
                    echo $value . ';';
                }
            }
            ?>
            });
            
        </script>
        
         <script type="text/javascript">
            var base_url = '<?php echo base_url(); ?>';
        </script>       
        
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>