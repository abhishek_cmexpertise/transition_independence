<div class="header navbar navbar-inverse navbar-fixed-top">
    <!-- BEGIN TOP NAVIGATION BAR -->
    <div class="navbar-inner">
        <div class="container-fluid">
            <!-- BEGIN LOGO -->
            <a class="brand" href="<?php echo base_url().'superadmin/dashboard'; ?>">
                <h3 style="color:#FFFFFF; margin:-10px; font-size:20px;"><?= PROJECT_NAME ?></h3>
            </a>
            <!-- END LOGO -->
            <!-- BEGIN RESPONSIVE MENU TOGGLER -->
            <a href="javascript:;" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                <img src="<?= base_url(); ?>public/assets/cms/img/menu-toggler.png" alt="" />
            </a>          
            <!-- END RESPONSIVE MENU TOGGLER -->				
            <!-- BEGIN TOP NAVIGATION MENU -->					
            <ul class="nav pull-right">
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img alt="" src="<?= base_url(); ?>public/assets/cms/images/user_nopic.png" width="28"/>
                        <span class="username"><?php echo $this->session->userdata['valid_login']['email']; ?></span>
                        <i class="icon-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo base_url().'superadmin/dashboard'; ?>"><i class="icon-user"></i> Dashboard</a></li>
                        <li><a href="<?php echo base_url().'superadmin/tools/changePassword'; ?>"><i class="icon-calendar"></i> Change Password</a></li>
                        <li><a href="<?php echo base_url().'superadmin/tools'; ?>"><i class="icon-calendar"></i> Settings</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo base_url().'superadmin/dashboard/logout'; ?>"><i class="icon-key"></i> Log Out</a></li>
                    </ul>
                </li>
                <!-- END USER LOGIN DROPDOWN -->
            </ul>
            <!-- END TOP NAVIGATION MENU -->	
        </div>
    </div>
    <!-- END TOP NAVIGATION BAR -->
</div>

