<div class="page-sidebar nav-collapse collapse">
    <ul>
<!--        <li><div class="sidebar-toggler hidden-phone"></div></li>
        <li>&nbsp;</li>-->
        <li class="start <?php if ($this->uri->segment(2, 0) == 'dashboard') { echo 'active';} ?>">
            <a href="<?php echo base_url() . "superadmin/dashboard"; ?>">
                <i class="icon-home"></i> 
                <span class="title">Dashboard</span>
                <span class="<?php if ($action == 'dashboard') { echo 'selected';} ?>"></span>
            </a>
            
        </li>

        <!--Home -->

        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'home') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">HOME PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'home') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'home') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                  <li class="<?php if ($action == 'home_banner') {echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/home"; ?>">Manage Banner</a>
                </li>
                <li class="<?php if ($action == 'home_text') {echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/home/mngHomeText"; ?>">Manage Home Text</a>
                </li>
            </ul>
        </li>

        <!--About-->

        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'about') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">ABOUT PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'about') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'about') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'about_banner') {echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/about"; ?>">Manage Banner</a>
                </li>
                <li class="<?php if ($action == 'about_text') {echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/about/aboutText"; ?>">Manage About Text</a>
                </li>
                
                <li class="<?php if ($action == 'about_writer') {echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/about/aboutWriter"; ?>">Manage About Writers</a>
                </li>
            </ul>
        </li>
        
        <!--Contacts-->
        
        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'contact') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">CONTACTS PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'contact') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'contact') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'contact_banner'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/contact"; ?>">Manage Banner</a>
                </li>
                <li class="<?php if ($action == 'contact_info'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/contact/contactInfo"; ?>">Manage Contact Info</a>
                </li>
            </ul>
        </li>
        
        <!-- Helpfulllink -->
        
        
        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'Helpfulllink') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">HELPFUL LINKS PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'Helpfulllink') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'Helpfulllink') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'link_banner'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Helpfulllink"; ?>">Manage Banner</a>
                </li>
                <li class="<?php if ($action == 'helpful_link'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Helpfulllink/helpful_link_list"; ?>">Manage Helpful Links</a>
                </li>
            </ul>
        </li>        
        
        
        <!--EMPLOYMENT OPPORTUNITIES PAGE-->
        
        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'Empopportunities') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">EMPLOYMENT OPPORTUNITIES PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'Empopportunities') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'Empopportunities') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'emp_banner'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Empopportunities"; ?>">Manage Banner</a>
                </li>
                <li class="<?php if ($action == 'emp_oportunity'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Empopportunities/empOportunityList"; ?>">Manage Employment Opportunities</a>
                </li>
                <li class="<?php if ($action == 'emp_instructor_job'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Empopportunities/empInstructorJobList"; ?>">Manage Instructor Job Description</a>
                </li>
                <li class="<?php if ($action == 'emp_supervisor_job'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Empopportunities/empSupervisorJobList"; ?>">Manage Supervisor Job Description</a>
                </li>
            </ul>
        </li> 
        
        
        
        <!--TESTIMONIALS PAGE-->
        
        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'Testimonials') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">TESTIMONIALS PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'Testimonials') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'Testimonials') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'testimonials'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Testimonials"; ?>">Manage Banner</a>
                </li>
                 <li class="<?php if ($action == 'testimonials_mgmt'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Testimonials/mng_testimonials"; ?>">Manage Testimonials</a>
                </li>
            </ul>
        </li> 

        
        <!--ARTISTS PAGE-->
        
        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'Artists') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">ARTISTS PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'Artists') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'Artists') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'artists_list'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Artists"; ?>">Manage Banner</a>
                </li>
                 <li class="<?php if ($action == 'art_mgmt'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Artists/mng_artist"; ?>">Manage Artists</a>
                </li>
            </ul>
        </li>
        
        
        <!--CLIENTS PAGE-->
        
        
        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'Client') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">CLIENTS PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'Client') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'Client') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'client_list'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Client"; ?>">Manage Banner</a>
                </li>
                 <li class="<?php if ($action == 'client_mgmt'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Client/mng_client"; ?>">Manage Clients</a>
                </li>
            </ul>
        </li>
        
        
        <!--VIDEO PAGE-->
        
         <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'Video') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">VIDEO PAGE</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'Video') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'Video') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'video_list'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Video"; ?>">Manage Banner</a>
                </li>
                 <li class="<?php if ($action == 'mng_video'){echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/Video/mng_video"; ?>">Manage Videos</a>
                </li>
            </ul>
        </li>
        
        
        
        <!--Tools-->

        <li class="has-sub <?php if ($this->uri->segment(2, 0) == 'tools') {echo 'active';} ?>">
            <a href="javascript:;">
                <i class="icon-bookmark-empty"></i> 
                <span class="title">Tools</span>
                <span class="<?php if ($this->uri->segment(2, 0) == 'tools') {echo 'selected';} ?>"></span>
                <span class="arrow <?php if ($this->uri->segment(2, 0) == 'tools') {echo 'open';} ?>"></span>
            </a>
            <ul class="sub">
                <li class="<?php if ($action == 'tools_setting') {echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/tools"; ?>">Settings</a>
                </li>
                
                <li class="<?php if ($action == 'tools_change_password')  {echo 'active';} ?>">
                    <a href="<?php echo base_url() . "superadmin/tools/changePassword"; ?>">Change Password</a>
                </li>
                <li>
                    <a href="<?php echo base_url() . "superadmin/tools/logout"; ?>">Logout</a>
                </li>
            </ul>
        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>