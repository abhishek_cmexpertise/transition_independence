<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/js/jquery-1.8.3.min.js"></script>	
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/tinymce/tinymce.min.js"></script>  
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.8.1/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/breakpoints/breakpoints.js"></script>		
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>	
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/js/jquery.blockui.js"></script>	
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/js/jquery.cookie.js"></script>

<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/data-tables/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/data-tables/DT_bootstrap.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/chosen-bootstrap/chosen/chosen.jquery.min.js"></script>

<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script>	
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/gritter/js/jquery.gritter.js"></script>

<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/js/jquery.pulsate.min.js"></script>


<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>


<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/bootstrap-daterangepicker/date.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/bootstrap-daterangepicker/daterangepicker.js"></script>	
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/js/app.js"></script>				
<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/bootstrap-toggle-buttons/static/js/jquery.toggle.buttons.js"></script>

<script type="text/javascript" src="<?= base_url(); ?>public/assets/cms/bootstrap-fileupload/bootstrap-fileupload.js"></script>
 <script type="text/javascript" src="http://localhost/real_estate/public/assets/theme/js/additional-methods.js?v=266"></script>


<?php
    if (!empty($js)) {
        foreach ($js as $value) {
            ?>
            <script src="<?php echo base_url(); ?>public/assets/cms/javascripts/<?php echo $value ?>"
            type="text/javascript"></script>
            <?php
        }
    }
?>

<script>
    jQuery(document).ready(function () {
        <?php
        if (!empty($init)) {
            foreach ($init as $value) {
                echo $value . ';';
            }
        }
        ?>
    });
</script>


<!--Delete Record Alert -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id = "modelTitle">Modal Header</h4>
            </div>
            <div class="modal-body" id = "modelContent"></div>
            <div class="modal-footer" id = "modelFooter"></div>
        </div>
    </div>
</div>