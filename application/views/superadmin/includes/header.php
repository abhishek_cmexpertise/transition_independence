<!-- START HEAD -->
<head>
    <meta charset="utf-8" />
    <title><?= PROJECT_NAME ?> Admin :: <?php echo $page_title;  ?></title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/css/metro.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap-fileupload/bootstrap-fileupload.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/css/style.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/css/style_responsive.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/css/style_brown.css" rel="stylesheet" id="style_color" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/chosen-bootstrap/chosen/chosen.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/jquery-tags-input/jquery.tagsinput.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/clockface/css/clockface.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap-timepicker/compiled/timepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap-colorpicker/css/colorpicker.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap-toggle-buttons/static/stylesheets/bootstrap-toggle-buttons.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/data-tables/DT_bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/bootstrap-daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/assets/cms/uniform/css/uniform.default.css" />
    
      <script type="text/javascript">
        var base_url = '<?php echo SITE_URL; ?>';
    </script>
</head>
<!-- END HEAD -->
