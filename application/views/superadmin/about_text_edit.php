<style>label.error{color: red;}</style>

<div class="page-content">
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title"><?php echo $page_title; ?></h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo $url; ?>"><?php echo 'Home Page'; ?></a> 
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="<?php echo $url1; ?>"><?php echo $module_name; ?></a>
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="<?php echo $formAction; ?>"><?php echo $page_title; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->   
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <h4><i class="icon-globe"></i><?php echo $page_title; ?></h4>
                    </div>
                    <div class="portlet-body form">
                        <form  class="form-horizontal" id="event_add" method="post" action="<?php echo $formAction; ?>"  style="margin:0; padding:0" enctype="multipart/form-data" >
                            <div class="control-group">
                                <label class="control-label">Topic Title</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="banner_title" id="banner_title" value="<?php echo @$AboutTextData[0]->page_name ?>" />
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Topic Description</label>
                                <div class="controls" style="margin-right:15px;">
                                    <textarea class="span6 m-wrap" name="banner_desc" rows="6" id="banner_desc"><?php echo @$AboutTextData[0]->page_desc; ?></textarea>
                                </div>
                            </div>
                            <div class="form-actions" style="padding-left:10px">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn" id="cancel" onClick="location.href = '<?php echo $url1;  ?>'">Cancel</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->  
    </div>
    <!-- END PAGE CONTAINER-->
</div>

