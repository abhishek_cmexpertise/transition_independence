<style>label.error{color: red;}</style>

<div class="page-content">
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title"><?php echo $page_title; ?></h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo $url; ?>"><?php echo 'Home Page'; ?></a> 
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="<?php echo $url1; ?>"><?php echo $module_name; ?></a>
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="<?php echo $formAction; ?>"><?php echo $page_title; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->   
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <h4><i class="icon-globe"></i><?php echo $page_title; ?></h4>
                    </div>
                    <div class="portlet-body form">
                        <form  class="form-horizontal" id="event_add" method="post" action="<?php echo $formAction; ?>"  style="margin:0; padding:0" enctype="multipart/form-data" >

                            <div class="control-group">
                                <label class="control-label">Contact Image &nbsp;</label>				  
                                <div class="controls">
                                    <input type = "hidden" name = "hidden_logo"  id="hidden_logo" value="<?php echo @$ContactInfoData[0]->contact_pic ? $ContactInfoData[0]->contact_pic : ''; ?>" size = "20" />
                                    <input type="file" name="bigimg" size = "20" id="imgInp" placeholder="Please Upload Image"/><br>
                                    <img id="blah" src="<?php echo @$ContactInfoData[0]->contact_pic ? base_url() . 'public/assets/cms/' . $ContactInfoData[0]->contact_pic : ''; ?>" alt="your image" width="180px" height="180px" type = "hidden" style="<?php echo @$ContactInfoData[0]->contact_pic ? 'display: block' : 'display: none'; ?>"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Contact Name</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="contact_name" id="contact_name" value="<?php echo @$ContactInfoData[0]->contact_name ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Contact Designation</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="contact_desigmation" id="contact_desigmation" value="<?php echo @$ContactInfoData[0]->contact_designation ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Contact Phone</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="contact_phone" id="contact_phone" value="<?php echo @$ContactInfoData[0]->contact_phone ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Contact Fax</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="contact_fax" id="contact_fax" value="<?php echo @$ContactInfoData[0]->contact_fax ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Contact Email</label>				  
                                <div class="controls">
                                    <input type="email" class="span6 m-wrap" name="contact_email" value="<?php echo @$ContactInfoData[0]->contact_email ?>" />
                                </div>
                            </div>

                            <div class="form-actions" style="padding-left:10px">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn" id="cancel" onClick="location.href = '<?php echo $url1; ?>'">Cancel</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->  
    </div>
    <!-- END PAGE CONTAINER-->
</div>

