<style>
    label.error {color: #ff0000 !important;}
    .display-hide{display:none;}
</style>

<div class="content">
    <!-- BEGIN LOGIN FORM -->
    <form class="form-vertical login-form" id="login-form" action="<?= $formAction; ?>" method="POST">

        <input type="hidden" name="dologin" value="yes" />

        <h3 class="form-title">Login to your account</h3>

        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span></span>
        </div>

        <div class="alert alert-success display-hide">
            <button class="close" data-close="alert"></button>
            <span></span>
        </div>

        <div class="control-group">
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-user"></i>
                    <input class="m-wrap placeholder-no-fix" type="text" placeholder="Username" name="admin_username"/>
                </div>
            </div>
        </div>

        <div class="control-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <div class="controls">
                <div class="input-icon left">
                    <i class="icon-lock"></i>
                    <input class="m-wrap placeholder-no-fix" type="password" placeholder="Password" name="admin_password"/>
                </div>
            </div>
        </div>

        <div class="form-actions" style="border-bottom:none">
            <label class="checkbox">
                <input type="checkbox" name="rem" value="yes" <?php
                if (get_cookie('admin_username') != '') {
                    echo 'checked="checked"';
                }
                ?> /> Remember me
            </label>
            <button type="submit" class="btn green pull-right">
                Login <i class="m-icon-swapright m-icon-white"></i>
            </button>            
        </div>

    </form>
    <!-- END LOGIN FORM -->        
</div>