<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('superadmin/includes/header'); ?>
    <!-- END HEADER -->
    <body class="fixed-top">
        <!-- BEGIN BODY HEADER -->
        <div class="page-wrapper">
            <?php $this->load->view('superadmin/includes/body_header'); ?>
            <!-- END BODY HEADER -->
            <!-- BEGIN CONTAINER -->
            <div class="page-container row-fluid">
                <?php $this->load->view('superadmin/includes/body_left_pannel'); ?>
                <?php $this->load->view($page); ?>
            </div>
            <!-- END CONTAINER -->
            <!-- BEGIN BODY FOOTER -->
            <?php $this->load->view('superadmin/includes/body_footer'); ?>
            <!-- END BODY FOOTER -->
            <!-- BEGIN FOOTER -->
            <?php $this->load->view('superadmin/includes/footer'); ?>
            <!-- END FOOTER -->
        </div>
    </body>
</html>