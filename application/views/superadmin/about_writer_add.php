<style>label.error{color: red;}</style>

<div class="page-content">
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title"><?php echo $page_title; ?></h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo $url; ?>"><?php echo 'Home Page'; ?></a> 
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="<?php echo $url1; ?>"><?php echo $module_name; ?></a>
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="<?php echo $formAction; ?>"><?php echo $page_title; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->   
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <h4><i class="icon-globe"></i><?php echo $page_title; ?></h4>
                    </div>
                    <div class="portlet-body form">
                        <form  class="form-horizontal" id="event_add" method="post" action="<?php echo $formAction; ?>"  style="margin:0; padding:0" enctype="multipart/form-data" >

                            <div class="control-group">
                                <label class="control-label">Writer Image &nbsp;</label>				  
                                <div class="controls">
                                    <input type = "hidden" name = "hidden_logo"  id="hidden_logo" value="<?php echo @$WriterData[0]->writer_pic ? $WriterData[0]->writer_pic : ''; ?>" size = "20" />
                                    <input type="file" name="bigimg" size = "20" id="imgInp" placeholder="Please Upload Image"/><br>
                                    <img id="blah" src="<?php echo @$WriterData[0]->writer_pic ? base_url() . 'public/assets/cms/' . $WriterData[0]->writer_pic : ''; ?>" alt="your image" width="180px" height="180px" type = "hidden" style="<?php echo @$WriterData[0]->writer_pic ? 'display: block' : 'display: none'; ?>"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Writer Name</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="writer_name" id="writer_name" value="<?php echo @$WriterData[0]->writer_name ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Writer Designation</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="writer_desigmation" id="writer_desigmation" value="<?php echo @$WriterData[0]->writer_designation ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Writer Phone</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="writer_phone" id="writer_phone" value="<?php echo @$WriterData[0]->writer_phone ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Writer Fax</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="writer_fax" id="writer_fax" value="<?php echo @$WriterData[0]->writer_fax ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Writer Email</label>				  
                                <div class="controls">
                                    <input type="email" class="span6 m-wrap" name="writer_email" id="writer_email" value="<?php echo @$WriterData[0]->writer_email ?>" />
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Description</label>
                                <div class="controls" style="margin-right:15px;">
                                    <textarea class="span6 m-wrap" name="banner_desc" rows="6" id="banner_desc"><?php echo @$WriterData[0]->writer_desc; ?></textarea>
                                </div>
                            </div>


                            <div class="form-actions" style="padding-left:10px">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn" id="cancel" onClick="location.href = '<?php echo $url1;  ?>'">Cancel</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->  
    </div>
    <!-- END PAGE CONTAINER-->
</div>

