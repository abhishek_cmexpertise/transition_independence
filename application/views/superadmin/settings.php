<style>
    .form-horizontal .control-label {float: left;padding-top: 5px;text-align: left;width: 236px;}
    .form .form-actions{padding-left:20px;}
    label.error{color:red}
</style>

<!-- END SIDEBAR -->
<!-- BEGIN PAGE -->  
<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN STYLE CUSTOMIZER -->
                <div class="color-panel hidden-phone">
                    <div class="color-mode-icons icon-color"></div>
                    <div class="color-mode-icons icon-color-close"></div>
                    <div class="color-mode">
                        <p>THEME COLOR</p>
                        <ul class="inline">
                            <li class="color-black current color-default" data-style="default"></li>
                            <li class="color-blue" data-style="blue"></li>
                            <li class="color-brown" data-style="brown"></li>
                            <li class="color-purple" data-style="purple"></li>
                            <li class="color-white color-light" data-style="light"></li>
                        </ul>
                        <label class="hidden-phone">
                            <input type="checkbox" class="header" checked value="" />
                            <span class="color-mode-label">Fixed Header</span>
                        </label>                    
                    </div>
                </div>
                <!-- END BEGIN STYLE CUSTOMIZER -->   
                <h3 class="page-title">
                    <?php echo $page_title ?>
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo base_url() . 'superadmin/dashboard'; ?>">Home</a> 
                        <span class="icon-angle-right"></span>
                    </li>
                    <li><a href="<?php echo $formAction; ?>"><?php echo $page_title ?></a></li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->   
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <h4><i class="icon-reorder"></i>&nbsp;<?php echo $page_title ?></h4>
                    </div>
                    <div class="portlet-body form">
                        <?php echo $this->session->flashdata('myMessage'); ?>
                        <!-- BEGIN FORM-->
                        <form action="<?php echo $formAction; ?>" class="form-horizontal" id="frmsettings" name="frmsettings" method="post">
                            <div class="control-group">
                                <label class="control-label">Facebook Link</label>
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap"  name="facebook_link" id="facebook_link" value="<?php echo $settings_data[0]->config_val; ?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Twitter Link</label>
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap"  name="twitter_link" id="twitter_link" value="<?php echo $settings_data[1]->config_val; ?>"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Google Plus Link</label>
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap"  name="googleplus_link" id="googleplus_link" value="<?php echo $settings_data[2]->config_val; ?>"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Pinterest Link</label>
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap"  name="pinterest_link" id="pinterest_link" value="<?php echo $settings_data[3]->config_val; ?>"/>
                                </div>
                            </div>					   
                            <div class="control-group">
                                <label class="control-label">Google Map Link</label>
                                <div class="controls">
                                    <textarea class="span6 m-wrap"  name="googlemap_link" id="googlemap_link" rows="6"><?php echo $settings_data[4]->config_val; ?></textarea>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn" id="cancel">Cancel</button>
                            </div>
                        </form>
                        <!-- END FORM-->           
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->         
    </div>
    <!-- END PAGE CONTAINER-->
</div>
<!-- END PAGE -->  
