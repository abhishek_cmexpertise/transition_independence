<style>label.error{color: red;}</style>

<div class="page-content">
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>Portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title"><?php echo $page_title; ?></h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo $url; ?>"><?php echo 'Home Page'; ?></a> 
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="<?php echo $url1; ?>"><?php echo $module_name; ?></a>
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="<?php echo $formAction; ?>"><?php echo $page_title; ?></a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->   
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <h4><i class="icon-globe"></i><?php echo $page_title; ?></h4>
                    </div>
                    <div class="portlet-body form">
                        <form  class="form-horizontal" id="event_add" method="post" action="<?php echo $formAction; ?>"  style="margin:0; padding:0" enctype="multipart/form-data" >

                            <div class="control-group">
                                <label class="control-label">Artist Image &nbsp;</label>				  
                                <div class="controls">
                                    <input type = "hidden" name = "hidden_logo"  id="hidden_logo" value="<?php echo @$ArtistData[0]->artist_image ? $ArtistData[0]->artist_image : ''; ?>" size = "20" />
                                    <input type="file" name="bigimg" size = "20" id="imgInp" placeholder="Please Upload Image"/><br>
                                    <img id="blah" src="<?php echo @$ArtistData[0]->artist_image ? base_url() . 'public/assets/cms/' . $ArtistData[0]->artist_image : ''; ?>" alt="your image" width="180px" height="180px" type = "hidden" style="<?php echo @$ArtistData[0]->artist_image ? 'display: block' : 'display: none'; ?>"/>
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Artist Name</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="artist_name" id="artist_name" value="<?php echo @$ArtistData[0]->artist_name ?>" />
                                </div>
                            </div>

                            <div class="control-group">
                                <label class="control-label">Artist Age</label>				  
                                <div class="controls">
                                    <input type="text" class="span6 m-wrap" name="artist_age" id="artist_age" value="<?php echo @$ArtistData[0]->artist_age ?>" />
                                </div>
                            </div>


                            <div class="control-group">
                                <label class="control-label">Artist Description</label>				  
                                <div class="controls">
                                    <textarea type="text" class="span12 m-wrap" rows="8" name="artist_desc" id="artist_desc" spellcheck="false"><?php echo @$ArtistData[0]->artist_desc ?></textarea>
                                </div>
                            </div>
                            


                            <div class="form-actions" style="padding-left:10px">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn" id="cancel" onClick="location.href = '<?php echo $url1; ?>'">Cancel</button>
                            </div>

                        </form>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->  
    </div>
    <!-- END PAGE CONTAINER-->
</div>


 