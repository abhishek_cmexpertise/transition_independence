<style>
    .modal {border-radius: 6px;left: 43%;margin-left: -375px;outline: medium none;position: fixed;width: 920px;}
    .modal-header {background-color: #F3F3F3;background-image: linear-gradient(#F3F3F3, #E7E7E7 50%);border-bottom: 1px solid #C4C4C4;border-radius: 6px 6px 0 0;clear: both;color: #494949;}
    .modal-footer:after {clear: both;}
    .modal-footer:before, .modal-footer:after {content: "";display: table;line-height: 0;}
    .modal-footer:after {clear: both;}
    .modal-footer {border-radius: 0 0 6px 6px;}
    .toggle-button span.info{background:-moz-linear-gradient(center bottom , #D84A38, #BB2413) repeat scroll 0 0 #D84A38;}
    .tile.double {height: 64px;width: 150px !important;}
</style>

<div class="page-content">
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title"> <?php echo $page_title; ?></h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="<?php echo $url; ?>"><?php echo 'Home Page'; ?></a> 
                        <span class="icon-angle-right"></span>
                    </li>
                    <li><a href="<?php echo $formAction; ?>"><?php echo 'Artist Details'; ?></a></li>
                </ul>
            </div>
        </div>
        <!-- END PAGE HEADER-->

        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->   
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <h4><i class="icon-globe"></i><?php echo $page_title; ?></h4>
                    </div>
                    <div class="portlet-body">
                        <?php echo $this->session->flashdata('myMessage'); ?>
                        <div class="clearfix">
                            <div class="btn-group">
                                <a class="btn mini green" href="<?php echo $tst; ?>">Add New <i class="icon-plus"></i></a>
                            </div>
                        </div>
                        
                        
                        
                        <div id="tablesec">
                            <div id="sample_1_wrapper" class="dataTables_wrapper form-inline" role="grid">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="sample_1" role="grid" aria-describedby="sample_1_info">
                                    <thead>
                                        <tr class="gradeX odd" role="row">
                                            <th class="hidden-480 sorting" style="width: 148.006px;" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1">Banner Image</th>
                                            <th class="hidden-480 sorting" style="width: 655.0057px;" role="columnheader" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1">Edit</th>
                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">

                                        <?php for ($i = 0; $i < count($image_mgmt); $i++) { ?>

                                            <tr class="gradeX odd">
                                                <td class="hidden-480 ">
                                                    <div class="tile image selected">
                                                        <div class="tile-body">
                                                            <img src="<?php echo base_url() . 'public/assets/cms/' . $image_mgmt[$i]->image_link; ?>" alt="">
                                                        </div>
                                                    </div>
                                                </td>
                                                <td class="hidden-480 "><a href="javascript:;" data-tooltip="Delete" class="deleteClient btn mini red" data-href="<?php echo base_url() . 'superadmin/Artists/deleteImage/' . $this->utility->newencode($image_mgmt[$i]->image_id); ?>">
                                                        <i class="icon-trash"></i> Delete</a></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <div class="row-fluid"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->  
    </div>
    <!-- END PAGE CONTAINER-->
</div>