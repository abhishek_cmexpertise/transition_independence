<style>
    label.error{
        color:red;
    }
</style>

<div class="page-content">
    <!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <div id="portlet-config" class="modal hide">
        <div class="modal-header">
            <button data-dismiss="modal" class="close" type="button"></button>
            <h3>portlet Settings</h3>
        </div>
        <div class="modal-body">
            <p>Here will be a configuration form</p>
        </div>
    </div>
    <!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
    <!-- BEGIN PAGE CONTAINER-->
    <div class="container-fluid">
        <!-- BEGIN PAGE HEADER-->   
        <div class="row-fluid">
            <div class="span12">
                <h3 class="page-title">
                    <?php echo $page_title; ?>
                </h3>
                <ul class="breadcrumb">
                    <li>
                        <i class="icon-home"></i>
                        <a href="#">Tools</a> 
                        <span class="icon-angle-right"></span>
                    </li>
                    <li>
                        <a href="#"><?php echo $page_title; ?></a>
                    </li>
                </ul>

            </div>
        </div>
        <!-- END PAGE HEADER-->
        <!-- BEGIN PAGE CONTENT-->
        <div class="row-fluid">
            <div class="span12">
                <!-- BEGIN SAMPLE FORM PORTLET-->   
                <div class="portlet box light-grey">
                    <div class="portlet-title">
                        <h4><i class="icon-reorder"></i><?php echo $page_title; ?></h4>
<!--                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            <a href="javascript:;" class="reload"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>-->
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->session->flashdata('myMessage'); ?>
                        <form action="<?php echo $formAction; ?>" id="change_password" class="form-horizontal"  method="post">
                            <input type="hidden" name="changepass" value="yes">
                            <div class="control-group">
                                <label class="control-label">Old Password</label>
                                <div class="controls">
                                    <input type="password" class="span6 m-wrap" name="oldpass" id="oldpass"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">New Password</label>
                                <div class="controls">
                                    <input type="password" class="span6 m-wrap"  name="newpass" id="newpass"/>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">Confirm Password</label>
                                <div class="controls">
                                    <input type="password" class="span6 m-wrap"  name="conpass" id="conpass"/>
                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn blue">Submit</button>
                                <button type="button" class="btn"  id="cancel">Cancel</button>
                            </div>
                        </form>
                        <!-- END FORM-->           
                    </div>
                </div>
                <!-- END SAMPLE FORM PORTLET-->
            </div>
        </div>
        <!-- END PAGE CONTENT-->         
    </div>
    <!-- END PAGE CONTAINER-->
</div>