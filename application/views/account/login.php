<section class="login p-fixed d-flex text-center bg-primary common-img-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="login-card card-block auth-body mr-auto ml-auto">
                    <form class="md-float-material" method="POST" action="<?php echo SITE_URL . "account/login"; ?>" id="accountLogin">
                        <div class="text-center">
                            <img src="<?php echo base_url() . "assets/images/logo.png" ?>" alt="logo.png">
                        </div>
                        <div class="auth-box">
                            <div class="row m-b-20">
                                <div class="col-md-12">
                                    <h3 class="text-left txt-primary">Sign In</h3>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group">
                                <input type="text" class="form-control" name="txtUsername" autocomplete="off" placeholder="Your Username">
                            </div>
                            <label id="txtUsername-error" class="error" for="txtUsername"></label>
                            <div class="form-group">
                                <input type="password" class="form-control" name="pwdPassword" autocomplete="off" placeholder="Your Password">
                            </div>
                            <label id="pwdPassword-error" class="error" for="pwdPassword"></label>
                            <div class="form-group">
                                <select class="form-control" name="login_type" id="login_type">
                                    <option value="">Select login type</option>
                                    <option value="Super Admin">Super Admin</option>
                                    <option value="Supervisor">Supervisor</option>
                                    <option value="Employee">Employee</option>
                                </select>
                            </div>
                            <label id="login_type-error" class="error" for="login_type"></label>

                            <div class="row m-t-25 text-left">
                                <div class="col-sm-7 col-xs-12">
                                    <div class="checkbox-fade fade-in-primary">
                                        <label>
                                            <input type="checkbox" value="">
                                            <span class="cr"><i class="cr-icon icofont icofont-ui-check txt-primary"></i></span>
                                            <span class="text-inverse">Remember me</span>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-5 col-xs-12 forgot-phone text-right">
                                    <a href="<?php echo base_url() . 'account/forgot_password' ?>" class="text-right f-w-600 text-inverse"> Forgot Your Password?</a>
                                </div>
                            </div>
                            <div class="row m-t-30">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Sign in</button>
                                </div>
                            </div>
                            <!--<hr/>-->
<!--                            <div class="row">
                                <div class="col-md-10">
                                    <p class="text-inverse text-left m-b-0">Thank you and enjoy our website.</p>
                                    <p class="text-inverse text-left"><b>Your Authentication Team</b></p>
                                </div>
                                <div class="col-md-2">
                                    <img src=<?php //echo base_url() . "assets/images/auth/Logo-small-bottom.png" ?> alt="small-logo.png">
                                </div>
                            </div>-->

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
