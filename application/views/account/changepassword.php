<!-- Pre-loader end -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="card-block">
                <h5 class="m-b-10">Change Password</h5>
                <hr/>
            </div>
            <div class="card-block">
                <form method="POST" id="Changepass" action=<?php echo base_url() . "account/changepassword"; ?>>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label class="col-form-label" for="inputSuccess1"> Current Password
                            </label>
                        </div>
                        <div class="col-md-4">
                            <input type="password" class="form-control form-control " name="currpass" id="currpass">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <label class="col-form-label" for="inputWarning1">New Password</label>
                        </div>
                        <div class="col-md-4">
                            <input type="password" class="form-control form-control" name="newpass" id="newpass">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-2">
                            <label class="col-form-label" for="inputDanger1">Confirm Password</label>
                        </div>
                        <div class="col-md-4">
                            <input type="password" class="form-control form-control" name="confirmpass">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <input type="submit" class="btn btn-primary" value="change" name="change" >
                        </div>
                    </div>
                </form>                           
            </div>
        </div>
    </div>
</div>
