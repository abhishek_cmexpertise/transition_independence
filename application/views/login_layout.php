<!DOCTYPE html>
<html lang="en">

<head>
    
<?php $this->load->view('admin/includes/header');?>
</head>

<body class="fix-menu">
        <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="loader-track">
            <div class="loader-bar"></div>
        </div>
    </div>

     <?php $this->load->view($page); ?>
  
   <?php $this->load->view('admin/includes/footer'); ?>
</body>

</html>
