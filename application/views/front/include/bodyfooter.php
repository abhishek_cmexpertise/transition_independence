<div class="clr"></div>
<footer>
    <a href="<?php echo SITE_URL; ?>">Home</a>
    <a href="<?php echo SITE_URL . 'front/about'; ?>">About</a>
    <a href="<?php echo SITE_URL . 'front/contact'; ?>">Contact</a>    
    <a href="<?php echo SITE_URL . 'front/link'; ?>">Helpful Links</a> 
    <a href="<?php echo SITE_URL . 'front/employment_application'; ?>">Employment Opportunities</a><br>
    <span>Copyright Max Herzig. All Rights Reserved.</span>
</footer>