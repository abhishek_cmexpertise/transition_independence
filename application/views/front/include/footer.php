<?php
if (isset($js)) {
    foreach ($js as $alljs) {
        if (filter_var($alljs, FILTER_VALIDATE_URL) === FALSE) {

            if (file_exists('public/assets/front/js/javascript/' . $alljs . '.js')) {
                $path = JS_FILE . 'javascript/' . $alljs . '.js?v=' . rand(1, 999);
            } else {
                $path = JS_FILE . $alljs . '.js?v=' . rand(1, 999);
            }
            ?>
            <script type="text/javascript" src="<?php echo $path; ?>"></script>
        <?php } else { ?>
            <script type="text/javascript" src="<?php echo $alljs; ?>"></script>
            <?php
        }
    }
}
?>

<script>
    jQuery(document).ready(function () {
    <?php
        if (isset($init)) {
            foreach ($init as $value) {
                echo $value . ';';
            }
        }
    ?>
    });
</script>
<script>
    $(window).bind('load', function () {
        var hold = "";
        var len = $(".testi-text .text-area").length;
        for (var i = 1; i <= len; i++)
        {
            hold += '<div class="bull2" id="rnd' + i + '"></div>';
        }
        $(".bullets2").append(hold);
        $("#rnd1").addClass("selected");

        $(document).on('click', '.bull2', function (e) {
            $(".bull2").removeClass("selected");
            $(this).addClass("selected");
            var id = $(this).attr("id");
            id = id.replace("rnd", "");
            $(".text-area").hide();
            $("#txt" + id).fadeIn();
        });
    });
</script>