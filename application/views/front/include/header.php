<head>
    
    <meta charset="utf-8">
    
    <title><?php echo SITE_TITLE; ?></title>
    
    <?php
    if (isset($css)) {
        foreach ($css as $allCss) {
            if (filter_var($allCss, FILTER_VALIDATE_URL) === FALSE) {
                ?>
                <link  rel="stylesheet" type="text/css" href="<?php echo CSS_FILE . $allCss . '.css'; ?>">
            <?php } else { ?>
                <link  rel="stylesheet" type="text/css" href="<?php echo $allCss; ?>">
                <?php
            }
        }
    }
    ?>
                
    <script src="<?php echo base_url(); ?>public/assets/front/js/jQueryv1.11.1.js"></script>
    
    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
    </script>
    
</head>