<header>
    <a href="<?php echo SITE_URL; ?>" class="logo"></a>
    <nav>
        <ul>
            <li class="<?php if($var_meta_title == 'home'){ echo "current-page"; } ?>"><a href="<?php echo SITE_URL; ?>">Home</a></li>
            <li class="<?php if($var_meta_title == 'about'){ echo "current-page"; } ?>"><a href="<?php echo SITE_URL.'front/about'; ?>">About</a></li>
            <li class="<?php if($var_meta_title == 'contact'){ echo "current-page"; } ?>"><a href="<?php echo SITE_URL.'front/contact'; ?>">Contact</a></li>      
            <li class="<?php if($var_meta_title == 'link'){ echo "current-page"; } ?>"><a href="<?php echo SITE_URL.'front/link'; ?>">Helpful Links</a></li> 
            <li class="<?php if($var_meta_title == 'team'){ echo "current-page"; } ?>"><a href="<?php echo SITE_URL.'front/team'; ?>">Team</a></li> 
            <li class="<?php if($var_meta_title == 'employment_application'){ echo "current-page"; } ?>"><a href="<?php echo SITE_URL.'front/employment_application'; ?>">Employment Opportunities</a></li>
        </ul>
    </nav>
    <a href="javascript:void(0);" class="login-button">Login</a>
</header>