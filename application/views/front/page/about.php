
<section class="banner-inner about_banner_image">
    <img src="<?php echo UPLOAD_IMAGE . $show_banner[0]->banner_image; ?>" alt="<?php echo $show_banner[0]->banner_title; ?>" width="1600" height="350">
    <section>
        <div class="container">
            <h1 class="heading"><?php echo stripslashes($show_cms[0]->page_name); ?></h1>
            <?php echo stripslashes($show_cms[0]->page_desc); ?>
        </div>
    </section>

    <section>
        <div class="container">
            <?php
            $s = 1;
            foreach ($about as $row_about) {
                
                
                if ($s % 2 == 0) {
                    $current_image_side = "left";
                    $current_content_side = "right";
                } else {
                    $current_image_side = "right";
                    $current_content_side = "left";
                }
                ?>
                <div class="row bg-row">
                    <div class="pull-<?php echo $current_image_side; ?> col-5">
                        <img src="<?php echo UPLOAD_IMAGE . $row_about->writer_pic; ?>" alt="<?php echo stripslashes($row_about->writer_name); ?>" width="600" height="428">
                    </div>
                    <div class="pull-<?php echo $current_content_side; ?> col-5">
                        <div class="cont-box">
                            <h1 class="con-heading"><?php echo stripslashes($row_about->writer_name); ?> <br><?php echo stripslashes($row_about->writer_designation); ?></h1>
                            <div class="box-meta">
                                <h3>
    <?php if ($row_about->writer_phone != "") { ?><strong>Cell :</strong> <?php echo stripslashes($row_about->writer_phone); ?>; <?php } ?>
    <?php if ($row_about->writer_fax != "") { ?>Fax: <?php echo stripslashes($row_about->writer_fax); ?> <?php } ?>
                                </h3>
                                <h3><?php if ($row_about->writer_email != "") { ?><strong>Email :</strong> <a href="mailto:<?php echo stripslashes($row_about->writer_email); ?>"><?php echo stripslashes($row_about->writer_email); ?></a> <?php } ?></h3>
                            </div>
                            <div class="content-scroll contentscroll">
    <?php echo stripslashes($row_about->writer_desc); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clr"></div>
                </div>
            <?php
            $s++;
        }
        ?>
        </div>
    </section>

