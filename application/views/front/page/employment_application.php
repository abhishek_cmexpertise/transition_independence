<style>
    label.error{
        color:red; 
        display: block;
        font-size: 11px;
        left: 30%;
        margin-top: -3px;
        position: absolute;

    }
    .sats{ margin-left: -172px;}
    .satsa{ margin-left: -340px;}
</style>
<?php
$autogenerate = mt_rand(111111 , 999999);
$cap_code='GfT'.$autogenerate;
?>

<section class="banner-inner">
    <img src="<?php echo UPLOAD_IMAGE . $show_banner[0]->banner_image; ?>" alt="<?php echo $show_banner[0]->banner_title; ?>" width="1600" height="350">
</section>

<section>
    <div class="container">
        <h1 class="heading"><?php echo stripslashes($show_cms[0]->page_name); ?></h1>
        <div class="eoplink-sec">
            <a href="<?php echo SITE_URL . 'front/Employment_application/Job_Description_Instructor'; ?>" target="new">Job Description: Instructor</a>
            <a href="<?php echo SITE_URL . 'front/Employment_application/Job_Description_Supervisor'; ?>" target="new">Job Description: Instructor</a>
        </div>
    </div>

</section>


<section>
    <div class="container">
        <div class="clr"></div>
        <div id="err_msgbox" class="container left-site">
            <?php
//            if ($_GET['mail_msg'] == 1) {
//               echo $repsMsg = '<div class="thankyou-sussmsg">You Have Successfully Sent Your Mail </div>';
//            }
            ?>
        </div>
        <div class="clr"></div>
        <h2 class="heading-inn">Interested Instructors should Fill up the Application Form and Submit</h2>
        <form  method="post" class="frm_empapp" id="employment_application" action="<?php echo SITE_URL . "front/Employment_application/employeement_application"; ?>" name="trasition" enctype="multipart/form-data" >
            <div class="prt_btn"><a href="javascript: void(0)"  onclick="window.open('<?php echo base_url() . 'public/assets/front/pdf'; ?>/transition-form.pdf', 'windowname2', 'width=auto, height=auto'); return false;">Print</a></div>
            <div class="table-box table-box-1">
                <div class="table-th"><h2>Position Desired</h2></div>
                <div class="table-box-inner">
                    <h3 class="form-heading">How did you hear about the position opening?</h3>
                    <div class="row form-row-wrap">
                        <div class="form-row"><label>Internet Site :</label><input type="text" name="internet_site" placeholder="Name of Site"></div>
                        <div class="form-row"><label>Employee Name :</label><input type="text" name="employee" placeholder=""></div>
                        <div class="form-row"><label>Flyer :</label><input type="text" name="flyer" placeholder="Location"></div>
                        <div class="form-row"><label>Other :</label><input type="text" name="other" placeholder=""></div>
                        <div class="clr"></div> 
                    </div>
                </div>  
            </div>
            <div class="table-box table-box-2">
                <div class="table-th"><h2>Personal Information</h2></div>
                <div class="table-box-inner">
                    <div class="row form-row-wrap">
                        <div class="form-row"><label>Last Name *:</label><input type="text" name="l_name" placeholder="">
                            <span id="err_l_name">
                                <label for="l_name" class="error"></label>
                            </span>
                        </div>
                        <div class="form-row"><label>First Name *:</label><input type="text" name="f_name" placeholder="">
                            <span id="err_f_name"></span></div>
                        <div class="form-row"><label>Middle Initial :</label><input type="text" name="mid_name" placeholder=""></div>
                        <div class="form-row"><label>Street Address :</label><input type="text" name="street_name" placeholder="">
                            <span id="err_street_name"></span></div>
                        <div class="form-row"><label>City *:</label><input type="text"  name="city_name"  placeholder="">
                            <span id="err_city_name"></span></div>
                        <div class="form-row"><label>State *:</label>
                            <select name="state_name"><option>Select</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                            <span id="err_state_name"></span></div>
                        <div class="form-row"><label>Email :</label><input type="text" name="uemail" placeholder=""></div>
                        <div class="form-row"><label>Zip Code *:</label><input class="half-input" type="text" name="zip_name"  placeholder=""><span id="err_zip_name"></span></div>
                        <div class="form-row"><label>Home Phone :</label><input type="text" name="hphone" placeholder=""></div>
                        <div class="form-row">
                           <label>Cell Phone :</label><!--<input type="text" name="cphone" placeholder="">-->
                            <span class="phone-row">
                                <input type="tel" name="celltxt1" class="form-control autotab" id="char1" maxlength="3" onkeyup="autoTab(this, char2)">
                                <input type="tel" name="celltxt2" class="form-control autotab" id="char2" maxlength="3" onkeyup="autoTab(this, char3)">
                                <input type="tel" name="celltxt3" class="form-control autotab" id="char3" maxlength="4">

                            </span>
                        </div>
                        <div class="form-row"><label>Other Phone :</label><input type="text" name="ophone" placeholder=""></div>
                        <div class="form-row"><label>Fax :</label><input class="half-input" type="text" name="fax" placeholder=""></div>
                        <div class="clr"></div> 
                    </div>
                </div>  
            </div>          
            <div class="table-box table-box-3">
                <div class="table-th"><h2>In Case of Emergency Please Notify</h2></div>
                <div class="table-box-inner">
                    <div class="row form-row-wrap">
                        <div class="form-row"><label>Last Name *:</label><input type="text" name="l_name_em" placeholder=""><span id="err_f_name_em"></span></div>
                        <div class="form-row"><label>First Name *:</label><input type="text" name="f_name_em" placeholder=""><span id="err_l_name_em"></div>
                        <div class="form-row"><label>Relationship :</label><input type="text" name="f_rel_em" placeholder=""></div>
                        <div class="form-row"><label>Street Address :</label><input type="text" name="street_name_em" placeholder=""><span id="err_street_name_em"></span></div>
                        <div class="form-row"><label>City *:</label><input type="text" name="city_name_em" placeholder=""><span id="err_city_name_em"></span></div>
                        <div class="form-row"><label>State *:</label>
                            <select  name="state_name_em"><option>Select</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District Of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                            <span id="err_state_name_em"></span></div>
                        <div class="form-row"><label>Email :</label><input type="text" name="uemail_em" placeholder=""></div>
                        <div class="form-row"><label>Zip Code :</label><input class="half-input" type="text" name="zip_name_em" placeholder=""><span id="err_zip_name_em"></span></div>
                        <div class="form-row"><label>Home Phone :</label><input type="text" name="hphone_em" placeholder=""></div>
                        <div class="form-row"><label>Cell Phone :</label>
                            <span class="phone-row">
                                <input type="tel" name="celltxt1_em" class="form-control autotab" id="char1a" maxlength="3" onkeyup="autoTab(this, char2a)">
                                <input type="tel" name="celltxt2_em" class="form-control autotab" id="char2a" maxlength="3" onkeyup="autoTab(this, char3a)">
                                <input type="tel" name="celltxt3_em" class="form-control autotab" id="char3a" maxlength="4">
                                <span id="err_call_phone_em"></span>
                            </span></div>
                        <div class="form-row"><label>Other Phone :</label><input type="text" name="ophone_em" placeholder=""></div>
                        <div class="form-row"><label>Fax :</label><input class="half-input" type="text" name="fax_em" placeholder=""></div>
                        <div class="clr"></div> 
                    </div>
                </div>  
            </div>         
            <div class="table-box table-box-5">
                <div class="table-th"><h2>Work Eligibility</h2></div>
                <div class="table-box-inner">
                    <h3 class="form-heading">The following minimum conditions must be met for initial and/or continued employment with Transition to Independence</h3>
                    <div class="row form-row-wrap">
                        <div class="form-row form-row-border"><label>Are you at least 18 years of age or older?</label>
                            <div class="fr-ri-box">
                                <select name="age" id="age">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Are you eligible to work in the United States?</label>
                            <div class="fr-ri-box">
                                <select name="eligible" id="eligible">
                                    <option value="No">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Can you provide at least two documents that prove your eligibility to work in the United States (e.g. Valid California Driver License, Social Security Card,Birth Certificate, Passport, etc.)?</label>
                            <div class="fr-ri-box">
                                <select name="docu" id="docu">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Do you possess a Valid California Driver License?</label>
                            <div class="fr-ri-box">
                                <select name="license" id="license">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Can you provide a copy of Proof of a “clean” California DMV Driving Record?</label>
                            <div class="fr-ri-box">
                                <select name="clean" id="clean">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label><span>Do you own a vehicle? </span>
                                <input class="inn-input" type="text" id="mk" placeholder="Make" name="make" readOnly > 
                                <input class="inn-input" type="text" id="md" placeholder="Model" name="model" readOnly > 
                                <input class="inn-input" type="text" id="yr" placeholder="Year" name="year" maxlength="4" readOnly > 
                            </label>
                            <div class="fr-ri-box">
                                <select name="vehicle" id="vehicle" onchange="makeedit()">
                                    <option value="No">No</option>
                                    <option value="Yes">Yes</option>

                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Can you provide proof of current California Automobile Registration for the vehicle that you own?</label>
                            <div class="fr-ri-box">
                                <select name="reg" id="reg">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="clr"></div> 
                    </div>
                </div>  
            </div>          
            <div class="table-box table-box-6">
                <div class="table-th"><h2>Work Eligibility Continued</h2></div>
                <div class="table-box-inner">
                    <h3 class="form-heading">The following minimum conditions must be met for initial and/or continued employment with Transition to Independence</h3>
                    <div class="row form-row-wrap">
                        <div class="form-row form-row-border"><label>Can you provide Proof of current valid automobile insurance for the vehicle that you own?</label>
                            <div class="fr-ri-box">
                                <select name="automobile" id="automobile">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Can you provide evidence of good physical health including tuberculosis clearance?</label>
                            <div class="fr-ri-box">
                                <select name="health" id="health">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Can you obtain a California Criminal Clearance?</label>
                            <div class="fr-ri-box">
                                <select name="criminal" id="criminal">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Do you hold a valid CPR and/or First Aid Certificate?</label>
                            <div class="fr-ri-box">
                                <select name="cpr" id="cpr">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="clr"></div> 
                    </div>
                </div>  
            </div>           
            <div class="table-box table-box-7">
                <div class="table-th"><h2>Education</h2></div>
                <div class="table-box-inner">
                    <h3 class="form-heading">Please provide the following information about your educational background</h3>
                    <div class="row form-row-wrap">
                        <div class="form-row form-row-border"><label>Name and Location of High School</label>
                            <div class="fr-ri-box border-right">
                                <span>Did you Graduate?</span>
                                <select name="hs" id="hs">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="fr-ri-box">
                                <span>Type of Degree</span>
                                <input class="inn-input" type="text" name="degree_type" >
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Name and Location of College/University</label>
                            <div class="fr-ri-box border-right">
                                <span>Did you Graduate?</span>
                                <select name="graduate" id="graduate">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="fr-ri-box">
                                <span>Type of Degree</span>
                                <input class="inn-input" type="text" name="degree_type2">
                            </div>
                        </div>
                        <div class="form-row form-row-border"><label>Name and Location of Business or Trade School</label>
                            <div class="fr-ri-box border-right">
                                <span>Did you Graduate?</span>
                                <select name="ts" id="ts">
                                    <option value="">Select</option><option value="Yes">Yes</option>
                                    <option value="No">No</option>
                                </select>
                            </div>
                            <div class="fr-ri-box">
                                <span>Type of Degree</span>
                                <input class="inn-input" type="text" name="degree_type3" >
                            </div>
                        </div>
                        <div class="form-row form-row-border"><span class="small-text">Describe any special training or skills (additional spoken languages, computer knowledge,etc.)</span>
                            <div class="clr"></div> 
                            <textarea style="width: 100%;" name="special_training"></textarea>
                            <div class="clr"></div> 
                        </div>
                        <div class="clr"></div> 
                    </div>
                </div>  
            </div>
            <div class="table-box table-box-8">
                <div class="table-th"><h2>Work History</h2></div>
                <div class="table-box-inner">
                    <h3 class="form-heading">Provide your full-time employment history for the last 5 years starting with your present or most recent employer. You may attach a resume or additional pages in addition to completing this portion of the employment application if necessary.</h3>
                    <div class="row form-row-wrap">
                        <div class="form-row"><label>Employer :</label><input type="text" name="employer" placeholder=""></div>
                        <div class="form-row"><label>Dates of Employment :</label><input type="text" name="employment"placeholder=""></div>
                        <div class="form-row"><label>Street Address :</label><input type="text" name="sadd" placeholder=""></div>
                        <div class="form-row"><label>City :</label><input type="text" name="h_city" placeholder=""></div>
                        <div class="form-row"><label>State :</label><input type="text" name="h_state" placeholder=""></div>
                        <div class="form-row"><label>Zip Code :</label><input class="half-input" type="text" name="h_zip" placeholder=""></div>
                        <div class="form-row"><label>Position Title :</label><input type="text" name="h_ptitle" placeholder=""></div>
                        <div class="form-row"><label>Supervisor Name :</label><input type="text" name="h_supervisor" placeholder=""></div>
                        <div class="form-row"><label>Phone :</label><input type="text" name="h_phone" placeholder=""></div>
                        <div class="form-row"><label>Ending Salary :</label><input type="text" name="h_salary" placeholder=""></div>
                        <div class="form-row"><label class="vertical_align_top">Duties and Responsibilities :</label><textarea name="h_duties"></textarea></div>
                        <div class="form-row"><label class="vertical_align_top">Reason for Leaving :</label><textarea name="h_reason"></textarea></div>
                        <div class="clr"></div> 
                    </div>
                </div>  
            </div>

            <div class="table-box table-box-9">
                <div class="table-box-inner">
                    <div class="small-text"><?php echo stripslashes($show_cms2[0]->page_desc); ?></div>
                </div>  
                <div class="table-box-inner border-top">
                    <span class="small-text text-color-blue">
                        <input type="checkbox" name="terms_con" id="terms_con"> I Agree with these  Terms & Conditions</span>
                        <label for="terms_con" class="error satsa"></label>
                </div>
            </div>
            <div class="table-box table-box-10 border-off">
                <h4 class="text-color-blue">Attachment</h4>               
                <div class="form-row fileuploadrow margin-left-off form-row-cust">
                    <div class="fileinputbox"></div>

                    <input type="file" name="upload[]" id="uploadBtn" multiple >
                    <label for="uploadBtn" class="error"></label>
                </div>
                <!--<span id="err_upload"></span>-->
                <div class="form-row margin-left-off form-row-cust">Note: File types : PDF | DOC | DOCX, Max-limit:10MB</div>
                <div class="form-row margin-left-off form-row-cust">
                    <img src="images/capimg.png" alt=""> <span id="cap_img" ><?php echo $cap_code; ?></span><br>
                    <input type="hidden" name="cap_str" id="cap_str" value="<?php echo $cap_code; ?>">
                    <input type="text" placeholder="" class="half-input" name="cap_char" id="cap_char">
                    <span id="err_cap_char"></span>
                    <label for="cap_char"  class="error sats"></label>
                </div>
                <div class="form-row margin-left-off form-row-cust">
                    <input type="submit" name="submit" class="submit" value="Submit">
                </div>
            </div>
        </form>
    </div>
</section>
<div class="clr"></div>

<section>
    <div class="container padding-off border-bottom">
        <div class="table-box border-off">
            <h4 class="text-color-blue"><?php echo stripslashes($show_cms3[0]->page_name); ?></h4>    
            <?php echo stripslashes($show_cms3[0]->page_desc); ?>
        </div>
    </div>
</section>

<script>
    (function ($) {
        $.fn.bootstrapFileInput = function () {
            this.each(function (i, elem) {
                var $elem = $(elem);
                // Add [processed] class to avoid double processing of input file element
                if (typeof $elem.attr('data-bfi-processed-class') != 'undefined') {
                    // Check if the element already has the [processed] flag on it and skip it if it does
                    if ($elem.hasClass($elem.attr('data-bfi-processed-class'))) {
                        return;
                    }
                    $elem.addClass($elem.attr('data-bfi-processed-class'));
                }
                // Maybe some fields don't need to be standardized.
                if (typeof $elem.attr('data-bfi-disabled') != 'undefined') {
                    return;
                }
                // Set the word to be displayed on the button
                var buttonWord = 'Browse..';
                if (typeof $elem.attr('title') != 'undefined') {
                    buttonWord = $elem.attr('title');
                }
                var className = '';
                if (!!$elem.attr('class')) {
                    className = ' ' + $elem.attr('class');
                }
                // Now we're going to wrap that input field with a Bootstrap button.
                // The input will actually still be there, it will just be float above and transparent (done with the CSS).
                $elem.wrap('<a class="file-input-wrapper btn btn-default ' + className + '"></a>').parent().prepend($('<span></span>').html(buttonWord));
            })
                    // After we have found all of the file inputs let's apply a listener for tracking the mouse movement.
                    // This is important because the in order to give the illusion that this is a button in FF we actually need to move the button from the file input under the cursor. Ugh.
                    .promise().done(function () {
                // As the cursor moves over our new Bootstrap button we need to adjust the position of the invisible file input Browse button to be under the cursor.
                // This gives us the pointer cursor that FF denies us
                $('.file-input-wrapper').mousemove(function (cursor) {
                    var input, wrapper,
                            wrapperX, wrapperY,
                            inputWidth, inputHeight,
                            cursorX, cursorY;
                    // This wrapper element (the button surround this file input)
                    wrapper = $(this);
                    // The invisible file input element
                    input = wrapper.find("input");
                    // The left-most position of the wrapper
                    wrapperX = wrapper.offset().left;
                    // The top-most position of the wrapper
                    wrapperY = wrapper.offset().top;
                    // The with of the browsers input field
                    inputWidth = input.width();
                    // The height of the browsers input field
                    inputHeight = input.height();
                    //The position of the cursor in the wrapper
                    cursorX = cursor.pageX;
                    cursorY = cursor.pageY;
                    //The positions we are to move the invisible file input
                    // The 20 at the end is an arbitrary number of pixels that we can shift the input such that cursor is not pointing at the end of the Browse button but somewhere nearer the middle
                    moveInputX = cursorX - wrapperX - inputWidth + 20;
                    // Slides the invisible input Browse button to be positioned middle under the cursor
                    moveInputY = cursorY - wrapperY - (inputHeight / 2);
                    // Apply the positioning styles to actually move the invisible file input
                    input.css({
                        left: moveInputX,
                        top: moveInputY
                    });
                });
                $('body').on('change', '.file-input-wrapper input[type=file]', function () {
                    var fileName;
                    fileName = $(this).val();
                    // Remove any previous file names
                    $(this).parent().next('.file-input-name').remove();
                    if (!!$(this).prop('files') && $(this).prop('files').length > 1) {
                        fileName = $(this)[0].files.length + ' files';
                    } else {
                        fileName = fileName.substring(fileName.lastIndexOf('\\') + 1, fileName.length);
                    }
                    // Don't try to show the name if there is none
                    if (!fileName) {
                        return;
                    }
                    var selectedFileNamePlacement = $(this).data('filename-placement');
                    if (selectedFileNamePlacement === 'inside') {
                        // Print the fileName inside
                        $(this).siblings('span').html(fileName);
                        $(this).attr('title', fileName);
                    } else {
                        // Print the fileName aside (right after the the button)
                        $(this).parent().after('<span class="file-input-name">' + fileName + '</span>');
                    }
                });
            });
        };
// Add the styles before the first stylesheet
// This ensures they can be easily overridden with developer styles
        var cssHtml = '<style>' +
                '.file-input-wrapper { overflow: hidden; position: relative; cursor: pointer; z-index: 1; }' +
                '.file-input-wrapper input[type=file], .file-input-wrapper input[type=file]:focus, .file-input-wrapper input[type=file]:hover { position: absolute; top: 0; left: 0; cursor: pointer; opacity: 0; filter: alpha(opacity=0); z-index: 99; outline: 0; }' +
                '.file-input-name { margin-left: 8px; }' +
                '</style>';
        $('link[rel=stylesheet]').eq(0).before(cssHtml);
    })(jQuery);
    $('input[type=file]').bootstrapFileInput();
</script>