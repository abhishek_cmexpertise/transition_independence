<section class="banner-inner">
    <img src="<?php echo UPLOAD_IMAGE . $show_banner[0]->banner_image; ?>" alt="<?php echo $show_banner[0]->banner_title; ?>" width="1600" height="350">
</section>

<section>
    <div class="container">
        <h1>Related Videos </h1>
        <div class="col-10 video-row">
            <div class="video-row-inn">
                <?php
                foreach ($video as $row_video) {
                    ?>
                    <div class="col-2 pull-left">
                        <iframe width="560" height="315" src="<?php echo stripslashes($row_video->video_link); ?>" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <?php
                }
                ?>
                <div class="clr"></div>
            </div>
        </div>
    </div>
</section>
