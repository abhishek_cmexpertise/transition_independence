<section>
    <div class="container">

        <h1>Our Team </h1>

        <div class="our_team">
           <?php 
            foreach ($team as $teamdetail)
            {
           ?>
            <div class="col-sm4">
                <a href="#">
                    <div class="team_images">
                        <div class="pic">
                            <img src="<?php echo UPLOAD_IMAGE . $teamdetail->writer_pic; ?>">
                        </div>

                        <div class="team_content">
                            <h2 class="text-center txt-head"><?php echo $teamdetail->writer_name; ?> </h2>
                            <p>  <?php echo stripslashes($teamdetail->writer_desc); ?></p>
                        </div>
                    </div>
                </a>
            </div>
           <?php
            }
           ?>
         
        
        </div>
    </div>
</section>


