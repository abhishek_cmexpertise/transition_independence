<section class="banner-inner">
    <img src="<?php echo UPLOAD_IMAGE . $show_banner[0]->banner_image; ?>" alt="<?php echo $show_banner[0]->banner_title; ?>" width="1600" height="350">
</section>

<section>
    <div class="container">
        <h1>Our Featured Clients</h1>
        <?php
        

        
        $s = 1;
        foreach ($patient as $row_patient) {
            if ($s % 2 == 0) {
                $current_image_side = "left";
                $current_content_side = "right";
            } else {
                $current_image_side = "right";
                $current_content_side = "left";
            }
            ?>
            <div class="row bg-row patientes-row">
                <div class="pull-<?php echo $current_image_side; ?> col-5">
                    <img src="<?php echo UPLOAD_IMAGE . $row_patient->patient_image; ?>" alt="<?php echo stripslashes($row_patient->patient_name); ?>" width="600" height="428">
                </div>
                <div class="pull-<?php echo $current_content_side; ?> col-5">
                    <div class="cont-box">
                        <h1 class="con-heading"><?php echo stripslashes($row_patient->patient_name); ?></h1>
                        <div class="content-scroll contentscroll">
                            <?php echo stripslashes($row_patient->patient_desc); ?>
                        </div>
                    </div>
                </div>
                <div class="clr"></div>
            </div>
            <?php
            $s++;
        }
        ?>
    </div>
</section>