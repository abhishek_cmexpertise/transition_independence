<style>
    label.error{color:red;}
</style>

<section class="banner">
    <div class="banner-images-wrapper">
        <?php
        foreach ($banner as $show_banner) {
            ?>
            <div class="banner-img">
                <img src="<?php echo UPLOAD_IMAGE . $show_banner->banner_image; ?>" width="1600" height="472" alt="<?php echo $show_banner->banner_title; ?>">
                <div class="texts">
                    <?php echo stripslashes($show_banner->banner_desc); ?>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</section>

<div class="selection2">
    <div class="inside">
        <div class="sec1">
            <div class="img"><img src="<?php echo UPLOAD_IMAGE . $row_art[0]->artist_image; ?>" width="59" height="56" alt="<?php echo $row_art[0]->artist_name; ?>"></div>
            <div class="det">
                <pre>NAME : <?php echo $row_art[0]->artist_name; ?><br>AGE : <?php echo $row_art[0]->artist_age; ?></pre>
            </div>
            <div class="clearfix"></div>
            <p><?php echo substr(stripcslashes($row_art[0]->artist_desc), 0, 95); ?></p>
            <a class="view-more-butt" href="<?php echo base_url() . 'front/Art/arts/' . $this->utility->newenCode($row_art[0]->artist_id); ?>">View</a>
        </div>
        <div class="sec2">
            <div class="sec2-row">
                <?php
                $g = 1;
                foreach ($art_gallery as $row_art_gallery) {
                    ?>
                    <a href="<?php echo base_url() . 'front/Art/arts/' . $this->utility->newenCode($row_art[0]->artist_id); ?>" class="images <?php if ($g == 4) { ?>no-margin<?php } ?>">
                        <img src="<?php echo UPLOAD_IMAGE . $row_art_gallery->image_link; ?>" width="199" height="167" alt="<?php echo $row_art_gallery->image_name; ?>">
                    </a>
                    <?php
                    $g++;
                }
                ?>
            </div>
        </div>
    </div>
</div>

<section class="section3">
    <div class="left">
        <h1><?php echo stripslashes($show_cms1[0]->page_name); ?></h1>
        <?php echo stripslashes($show_cms1[0]->page_desc); ?>
    </div>
    <div class="right">
        <h1>TESTIMONIALS <a href="<?php echo base_url() . 'front/Testimonial'; ?>">View All</a></h1>
        <div class="testi-text">
            <?php
            $t = 1;
            foreach ($show_testimonials as $row_testimonials) {
                ?>
                <div class="text-area" id="txt<?php echo $t; ?>">
                    <?php echo substr(stripcslashes($row_testimonials->testimonial_desc), 0, 250); ?> 
                    <a href="<?php echo base_url() . 'front/Testimonial'; ?>" class="richard-gear"><?php echo stripcslashes($row_testimonials->testimonial_title); ?></a>
                </div>
                <?php
                $t++;
            }
            ?>
        </div>
        <div class="bullets2">
        </div>
    </div>
    <div class="clearfix"></div>
</section>

<div class="blocks-holder">
    <div class="blocks-holder-row">
        <?php
        $a = 1;
        foreach ($show_patients as $row_patients) {
            ?>
            <a href="<?php echo base_url() . 'front/Patient'; ?>" class="blocks <?php
            if ($a == 2) {
                echo "no-margin";
            }
            ?>">
                <div class="image">
                    <img src="<?php echo UPLOAD_IMAGE . $row_patients->patient_image; ?>" width="384" height="420" alt="<?php echo stripcslashes($row_patients->patient_name); ?>">
                </div> 
                <div class="txt"><?php echo stripcslashes($row_patients->patient_name); ?></div>
            </a>
            <?php
            $a++;
        }
        ?>
    </div>
    <div class="clearfix"></div>
</div>

<section class="section4">
    <div class="inside">
        <div class="left">
            <h1><?php echo stripslashes($show_cms2[0]->page_name); ?></h1>
            <?php echo stripslashes($show_cms2[0]->page_desc); ?>

            <a href="<?php echo $latest_video[0]->video_link; ?>" class="video video-popup">
                <img src="<?php echo IMAGE . 'vid.jpg' ?>" width="520" height="299" alt="video">
            </a>
            <a class="video-view-all-butt" href="<?php echo base_url() . 'front/Video'; ?>">VIEW ALL</a>
        </div>

        <div class="right" id="thnx_msg">
            <h1><?php echo stripslashes($show_cms3[0]->page_name); ?></h1>
            <?php echo stripslashes($show_cms3[0]->page_desc); ?>

            <form method="post" action="<?php echo SITE_URL . "account/contact_us"; ?>" name="contact-form" id="contact-form" >
                <div class="form-row-wrapa ">
                    <input type="text" placeholder="First Name *" name="user_firstname" id="user_firstname">
                    <label for="user_firstname" class="error"></label>
                </div>
                <div class="form-row-wrapa ">
                    <input type="text" placeholder="Last Name *" name="user_lastname" id="user_lastname" class="no-margin">
                    <label for="user_lastname" class="error"></label>
                </div>
                <div class="form-row-wrapa ">
                    <input type="text" placeholder="Email *" name="user_email" id="user_email">
                    <label for="user_email" class="error"></label>
                </div>
                <div class="form-row-wrapa ">
                    <span class="phone-row">
                        <input type="tel" class="form-control autotab numbersOnly" id="char1" maxlength="3" onkeyup="autoTab(this, char2)" placeholder="Phone" name="celltxt1">
                        <input type="tel" class="form-control autotab numbersOnly" id="char2" maxlength="3" onkeyup="autoTab(this, char3)" name="celltxt2">
                        <input type="tel" class="form-control autotab numbersOnly" id="char3" maxlength="4" name="celltxt3">
                    </span>
                </div>
                <div class="form-row-wrapa text-area">
                    <textarea  name="user_message" id="user_message" placeholder="Text Area"></textarea>
                    <label for="user_message" class="error"></label><br><br>
                </div>
                <input type="submit" class="submit" value="SUBMIT">
            </form>
        </div>
    </div>
</section>

<!--Account Login-->
<div class="overlaymain">
    <div class="popup-wrapper">
        <div class="popupformbox pull-right">
            <div class="popupformbox-inner">
                <div class="skippopup">Skip</div>
                <div class="form-box-pop-up">
                    <div class="tabnav">
                        <ul>
                            <li><a class="currentTabNav" id="" href="javascript:void(0);">My Account</a></li>
                        </ul>
                    </div>
                    <div id="signUpbox" class="tab-content-box">
                        <form name="login-form" id="login-form" action="<?php echo SITE_URL . "account"; ?>" method="POST">
                            <div class="errownnmessage-box" id="login_error"></div>
                            <div class="pop-form-row">
                                <select name="login_type" id="login_type">
                                    <option value="">Select login type</option>
                                    <option value="Super Admin">Super Admin</option>
                                    <option value="Supervisor">Supervisor</option>
                                    <option value="Employee">Employee</option>
                                </select>
                            </div>      
                            <div class="pop-form-row">
                                <input type="text" placeholder="Username" name="login_username" id="login_username" autocomplete="off">
                            </div>
                            <div class="pop-form-row">
                                <input type="password" placeholder="Password" name="login_password" id="login_password" autocomplete="off">
                            </div>
                            <div class="pop-form-row submit">
                                <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">Go</button>
                            </div>
                            <div class="pop-form-row forgetpassword-roe">
                                <a class="forgetpass" href="javascript:void(0);">Forgot Password?</a>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="lost-password-box">
                    <form name="forgot-form" id="forgot-form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
                        <div class="thanmessage-box" id="forgot_error"></div>
                        <h2>Lost password?</h2>
                        <div class="pop-form-row"><input type="text" placeholder="User Email" name="forgot_email" id="forgot_email"></div>
                        <div class="pop-form-row submit"><input type="button" value="Send" id="forgot_button"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


