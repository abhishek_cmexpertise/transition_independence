<section class="banner-inner">
    <img src="<?php echo UPLOAD_IMAGE . $show_banner[0]->banner_image; ?>" alt="<?php echo $show_banner[0]->banner_title; ?>" width="1600" height="350">
</section>

<section>
    <div class="container">
        <div class="col-10 artic-row">
            <div class="artic-img-box pull-left"><img src="<?php echo UPLOAD_IMAGE . $row_art[0]->artist_image; ?>" alt="" width="400" height="351"></div>
            <div class="artic-bio pull-right">
                <h1><?php echo $row_art[0]->artist_name; ?> ​</h1>
                <p><?php echo stripcslashes($row_art[0]->artist_desc); ?>
            </div>
            <div class="clr"></div>
        </div>
        <div class="clr"></div>
        <div class="art-gallery-row">
            <h1>Art Gallery​</h1>
            <div class="art-gallery-row-inn">
                <?php
                $g = 1;
                foreach ($art_gallery as $row_art_gallery) {
                    ?>
                    <a href="<?php echo UPLOAD_IMAGE . $row_art_gallery->image_link; ?>" class="art-gallery-col lightbox">
                        <img src="<?php echo UPLOAD_IMAGE . $row_art_gallery->image_link; ?>" alt="<?php echo $row_art_gallery->image_name; ?>" width="199" height="167">
                        <div class="light-box-overlay"></div>
                    </a>    
                    <?php
                    $g++;
                }
                ?>
            </div>
            <div class="clr"></div>
        </div>
    </div>
</section>