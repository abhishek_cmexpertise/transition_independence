

<section class="banner-inner">
    <img src="<?php echo UPLOAD_IMAGE . $show_banner[0]->banner_image; ?>" alt="<?php echo $show_banner[0]->banner_title; ?>" width="1600" height="350">
</section>

<section>
    <div class="container">
        <h1>Testimonials</h1>
        <?php
        $t = 1;
        foreach ($testimonials as $row_testimonials) {
            ?>
            <div class="col-10 testimonial-row">
                <div class="img-box-textm">
                    <img width="81" height="74" alt="<?php echo stripcslashes($row_testimonials->testimonial_title); ?>" src="<?php echo UPLOAD_IMAGE . $row_testimonials->writer_pic; ?>" class="">
                </div>
                <div class="testimonial-col">
                    <div class="testimonial-meta">
                        <h3><?php echo stripcslashes($row_testimonials->testimonial_title); ?> ​</h3>
                        <div class="department-position"><?php echo stripcslashes($row_testimonials->writer_designation); ?></div>
                    </div>
                    <div class="testimonial-content"><?php echo stripcslashes($row_testimonials->testimonial_desc); ?></div>
                </div>
                <div class="clr"></div>
            </div>
            <?php
            $t++;
        }
        ?>
    </div>
</section>