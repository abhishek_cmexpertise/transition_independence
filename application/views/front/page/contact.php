<style>
    .col-12.pull-left.contact-box{float: left;margin-bottom: 5%;width: 100%;}
    .col-12.pull-left.contact-box h3{ margin: 0;}
    .no_mrgn{margin-top:0 !important;}
    .mrgn_tp_btm{margin:10px 0 30px 0 !important;}
    label.error{color:red;}
</style>

<section class="banner-inner">
    <img src="<?php echo UPLOAD_IMAGE . $show_banner[0]->banner_image; ?>" alt="<?php echo $show_banner[0]->banner_title; ?>" width="1600" height="350">
</section>


<div class="Container">
    <section class="section4 contact-section no_mrgn">
        <div class="inside">
            <div class="left">
                <div class="">
                    <h1 class="heading mrgn_tp_btm">Contact Info</h1>
                    <?php
                    $a = 1;
                    foreach ($contacts as $row_contacts) {
                        ?>
                        <div class="col-12 pull-left contact-box">
                            <img class="" src="<?php echo UPLOAD_IMAGE . $row_contacts->contact_pic; ?>" alt="<?php echo stripcslashes($row_contacts->contact_name); ?>" width="120" height="125">
                            <h3 style="margin-top: 27px;"><?php echo stripcslashes($row_contacts->contact_name); ?>, <?php echo stripcslashes($row_contacts->contact_designation); ?></h3>
                            <div class="box-meta">
                                <p><strong>Cell :</strong> <?php echo stripcslashes($row_contacts->contact_phone); ?>; Fax: <?php echo stripcslashes($row_contacts->contact_fax); ?></p>
                                <p><strong>Email :</strong> <a href="mailto:<?php echo stripcslashes($row_contacts->contact_email); ?>"><?php echo stripcslashes($row_contacts->contact_email); ?></a></p>
                            </div>
                        </div>
                        <?php
                        $a++;
                    }
                    ?>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>                
            </div> 

            <div class="right">
                <h1>Contact Details</h1>
                <form method="post" action="<?php echo SITE_URL . "account/contact_us"; ?>" name="contact-form" id="contact-form">
                    <div class="form-row-wrapa ">
                        <input type="text" placeholder="First Name *" name="user_firstname" id="user_firstname">
                        <span id="err_user_firstname"></span> 
                    </div>
                    <div class="form-row-wrapa ">
                        <input type="text" placeholder="Last Name *" name="user_lastname" id="user_lastname" class="no-margin"><span id="err_user_lastname"></span>
                    </div>
                    <div class="form-row-wrapa ">
                        <input type="text" placeholder="Email *" name="user_email" id="user_email">
                        <span id="err_user_email"></span>
                    </div>
                    <div class="form-row-wrapa ">
                        <span class="phone-row">
                            <input type="tel" class="form-control autotab numbersOnly" id="char1" maxlength="3" onkeyup="autoTab(this, char2)" placeholder="Phone" name="celltxt1">
                            <input type="tel" class="form-control autotab numbersOnly" id="char2" maxlength="3" onkeyup="autoTab(this, char3)" name="celltxt2">
                            <input type="tel" class="form-control autotab numbersOnly" id="char3" maxlength="4" name="celltxt3">
                        </span>
                    </div>
                    <div class="form-row-wrapa text-area">
                        <textarea placeholder="Text Area" name="user_message" id="user_message"></textarea>
                        <div id="err_msgbox" class="container left-site"></div>
                    </div>
                    <input type="submit" class="submit" value="SUBMIT">
                </form>
            </div>
            <div class="clr"></div>
        </div>
        <div class="clr"></div>
    </section>
</div>

