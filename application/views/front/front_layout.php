<!DOCTYPE html>

<html lang="en">

    <?php $this->load->view('front/include/header'); ?>

    <body id="body">

        <?php $this->load->view('front/include/bodyheader'); ?>

        <?php $this->load->view($page); ?>

        <?php $this->load->view('front/include/bodyfooter'); ?>

        <?php $this->load->view('front/include/footer'); ?>

    </body>

</html>
