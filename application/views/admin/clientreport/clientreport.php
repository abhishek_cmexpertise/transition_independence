<style>
    .downloadbutton{
     text-align: right;
    display: block;
    width: 100%;
    } 
    
</style>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Client report</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                               <a href=<?php echo admin_url().'dashboard'?>> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href=<?php echo admin_url().'dashboard'?>>Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Client report
                        </li>
                    </ul>
                </div>
            </div>
           
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">
                                     <div class="row bg-row" style="float: right;padding-bottom: 15px;padding-right: 15px;">
                <div class="downloadbutton" style="color:white">
                 <a class="btn btn-primary addbutt" >Download All</a>
                </div>
                 
             </div>
                                    <table class="table table-striped table-bordered table-hover" id="clientreport_table">
                                        <thead>
                                            <tr>
                                                
                                            <th><label class="chkspace"><input type="checkbox" class="allCheck" id="chk0"><label for="chk0">Select All</label></label></th>
                                              <th>Client Name</th>
                                            <th>UCI #</th>
                                            <th>Date Of Birth</th> 
                                            <th>Date of Report</th>
                                            <th>Employees</th>
                                            <th>Client Monthly Report Download</th>
                                     </tr>
                                        </thead>
                                        <tbody>
                                       
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

