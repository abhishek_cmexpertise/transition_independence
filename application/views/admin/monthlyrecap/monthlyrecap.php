<style>
    .card-block2{
        display: flex;
    }  
    @media(max-width:768px){
      .card-block{
        flex-direction: column
    }   
        
    }
</style>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Monthlyrecap</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                            <a href=<?php echo admin_url().'dashboard'?>> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href=<?php echo admin_url().'dashboard'?>>Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Monthlyrecap</li>
                    </ul>
                </div>
            </div>
           
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div>
                      <form method="POST" action=<?php echo admin_url()."monthlyrecap/genarateMonthlyreport"?>>
                    <div class="page-header card">
                      
                        <div class="row" style="padding:25px;">
                          
                                    <div class="form-group col-lg-4 col-sm-12"  style="display:flex;margin-top:15px">
                                    <label style="padding-right:10px">Select Month </label>
                                    <select class="form-control month" name="month">
                                        <option value="January" <?php if(date('F',strtotime('first day of last month'))=="January"){echo "selected";}?>>January</option>
                                        <option value="February" <?php if(date('F',strtotime('first day of last month'))=="February"){echo "selected";}?>>February</option>
                                        <option value="March" <?php if(date('F',strtotime('first day of last month'))=="March"){echo "selected";}?>>March</option>
                                        <option value="April" <?php if(date('F',strtotime('first day of last month'))=="April"){echo "selected";}?>>April</option>
                                        <option value="May" <?php if(date('F',strtotime('first day of last month'))=="May"){echo "selected";}?>>May</option>
                                        <option value="June" <?php if(date('F',strtotime('first day of last month'))=="June"){echo "selected";}?>>June</option>
                                        <option value="July" <?php if(date('F',strtotime('first day of last month'))=="July"){echo "selected";}?>>July</option>
                                        <option value="August" <?php if(date('F',strtotime('first day of last month'))=="August"){echo "selected";}?>>August</option>
                                        <option value="September" <?php if(date('F',strtotime('first day of last month'))=="September"){echo "selected";}?>>September</option>
                                        <option value="October" <?php if(date('F',strtotime('first day of last month'))=="October"){echo "selected";}?>>October</option>
                                        <option value="November" <?php if(date('F',strtotime('first day of last month'))=="November"){echo "selected";}?>>November</option>
                                        <option value="December" <?php if(date('F',strtotime('first day of last month'))=="December"){echo "selected";}?>>December</option>
                                    </select>    
                                    </div >
                                    <div class="form-group  col-lg-4 col-sm-12"  style="display:flex;margin-top:15px">
                                    <label style="padding-right:10px">Select Year </label>
                                    <select class="form-control year" name="year">
                                      <option value="<?php echo date('Y');?>"><?php echo date('Y');?></option>
                                      <option value="<?php echo date("Y",strtotime("-1 year"));?>"><?php echo date("Y",strtotime("-1 year"));?></option>
                                    </select>
                                    </div>
                                     <div class="row bg-row col-lg-4 col-sm-12" style="margin-top:15px">
                                          <div class="col-sm-5">
                                             <input type="submit" class="btn btn-primary"  value="Monthly Recap Report">
                                         </div>
                                   </div>
                          
                                
                            </div>
                         
                       </div>
                            </form>
                </div>
            </div>
        </div>
    </div>
</div>

