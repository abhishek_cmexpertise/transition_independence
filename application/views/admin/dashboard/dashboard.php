<style>
    .my-custom-scrollbar {
        position: relative;
        height: 100px;
        overflow: auto;
    }
    .table-wrapper-scroll-y {
        display: block;
    }
</style>
<?php $cdate = date('d'); ?>

<div class="main-body">
    <div class="page-wrapper">
        <div class="page-header card" style=" background-color:#79a3ff">
            <div class="card-block">
              <h1 align="center" style="color:white"><?php echo "Welcome ".$this->session->userdata['valid_login']['firstname'].' '.$this->session->userdata['valid_login']['lastname']?></h1>
            </div>
        </div>
        <br>
        <div class="box box-primary box-primary-custom">
            <?php
            if ($cdate == 2 || $cdate == 25 || $cdate == 26 || $cdate == 27 || $cdate == 28 || $cdate == 29 || $cdate == 30 || $cdate == 31) {
                echo '<div class="stiker"><img src=' . base_url() . 'public/assets/front/images/stiker-bg.png' . '><p>Client monthly reports due by end of month</p></div>';
            }
            ?>
            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-body">
                            <div class="table-wrapper-scroll-y">
                                <table class="table table-bordered table-striped mb-0" id="dashboard_table">
                                    <thead class="thead-dark">
                                    <th colspan="4">
                                        <h4><center>Semi Annual Report Due</h4></center>
                                    </th>
                                    </thead>
                                    <thead>
                                        <tr>
                                            <th>Client's Name</th>
                                            <th>Date Of Birth</th>
                                            <th>Report Due</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $current = date("Y-m-d");
                                        if (count($semianualreport) > 0) {
                                            foreach ($semianualreport as $semianualreports) {
                                                $semi_annual_client_dob = date('Y-m-d', strtotime($semianualreports->client_dob . "6 Months"));

                                                $now = strtotime($current);
                                                $client_dob_year = date('Y', strtotime($semi_annual_client_dob));
                                                $replace_year = date('Y');
                                                $client_dob_this_year_date = str_replace($client_dob_year, $replace_year, $semi_annual_client_dob);

                                                if ($now > strtotime($client_dob_this_year_date)) {
                                                    $client_dob_this_year_date = date('Y-m-d', strtotime($client_dob_this_year_date . ' +1 year'));
                                                }
                                                $birth_date = strtotime($client_dob_this_year_date);
                                                $datediff = $birth_date - $now;
                                                $day_left = floor($datediff / (60 * 60 * 24));

                                                if ($day_left == 0) {
                                                    $due_report = "DUE TODAY";
                                                } else if ($day_left == 1) {
                                                    $due_report = $day_left . " Day Left";
                                                } else {
                                                    $due_report = $day_left . " Days Left";
                                                }
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $semianualreports->consumer_name; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo date('m / d / Y', strtotime($semianualreports->client_dob)); ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $due_report; ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                        <td colspan="3" style="text-align: center">
                                            <?php echo 'No Report Alert Available.'; ?>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped mb-0" id="dashboard_table">
                                    <thead class="thead-dark">
                                    <th colspan="4">
                                        <h4><center>Annual ISP Update Due</h4></center>
                                    </th>
                                    </thead>
                                    <thead>
                                        <tr>
                                            <th>Client's Name</th>
                                            <th>Date Of Birth</th>
                                            <th>Report Due</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (count($annualispreport) > 0) {
                                            foreach ($annualispreport as $annualispreports) {

                                                $now = strtotime($current);
                                                $client_dob_year = date('Y', strtotime($annualispreports->client_dob));
                                                $replace_year = date('Y');
                                                $client_dob_this_year_date = str_replace($client_dob_year, $replace_year, $annualispreports->client_dob);

                                                if ($now > strtotime($client_dob_this_year_date)) {
                                                    $client_dob_this_year_date = date('Y-m-d', strtotime($client_dob_this_year_date . ' +1 year'));
                                                }

                                                $birth_date = strtotime($client_dob_this_year_date);
                                                $datediff = $birth_date - $now;
                                                $day_left = floor($datediff / (60 * 60 * 24));

                                                if ($day_left == 0) {
                                                    $due_report = "DUE TODAY";
                                                } else if ($day_left == 1) {
                                                    $due_report = $day_left . " Day Left";
                                                } else {
                                                    $due_report = $day_left . " Days Left";
                                                }
                                                ?>
                                                <tr>
                                                    <td>
                                                        <?php echo $annualispreports->consumer_name; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo date('m / d / Y', strtotime($annualispreports->client_dob)); ?>
                                                    </td>

                                                    <td>
                                                        <?php echo $due_report; ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                        <td colspan="3" style="text-align: center">
                                            <?php echo 'No Report Alert Available.'; ?>
                                        </td>
                                        <?php
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container" style="margin-top:50px;">
            <div class="page-header card" style="background-color:#79a3ff; color: white">
                <div class="card-block">
                    <h1 align="center"><?php echo "Clients Birthday" ?></h1>
                </div>
            </div>
        </div>
        <div class="container">

            <h2 style="margin-top:50px;visibility: visible;" align="center">Today Birthday</h2>
            <div class="table-wrapper-scroll-y my-custom-scrollbar">

                <table class="table table-bordered  table-striped md-0 ">
                    <tbody>
                        <?php
                        if (count($client_birthday) > 0) {

                            foreach ($client_birthday as $client_birthdays) {
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $client_birthdays->consumer_name ?>
                                    </td>
                                    <td>
                                        <?php echo date('m - d - Y', strtotime($client_birthdays->client_dob)) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td style="color:red">No Birthday Today</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="container">
            <h2 style="margin-top:50px;" align="center">This Week Birthday</h2>
            <div class="table-wrapper-scroll-y my-custom-scrollbar">

                <table class="table table-bordered table-striped mb-0">
                    <tbody>
                        <?php
                        if ($client_weekbirthday > 0) {

                            foreach ($client_weekbirthday as $client_weekbirthdays) {
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $client_weekbirthdays->consumer_name ?>
                                    </td>
                                    <td>
                                        <?php echo date('m - d - Y', strtotime($client_weekbirthdays->client_dob)) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>

                                <td style="color:red">No Birthday This Week</td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>

        <div class="container">
            <h2 style="margin-top:50px;visibility: visible;" align="center">This Month Birthday</h2>
            <div class="table-wrapper-scroll-y my-custom-scrollbar">

                <table class="table table-bordered table-striped mb-0">
                    <tbody>
                        <?php
                        if ($client_monthbirthday > 0) {

                            foreach ($client_monthbirthday as $client_monthbirthdays) {
                                ?>
                                <tr>
                                    <td>
                                        <?php echo $client_monthbirthdays->consumer_name ?>
                                    </td>
                                    <td>
                                        <?php echo date('m - d - Y', strtotime($client_monthbirthdays->client_dob)) ?>
                                    </td>
                                </tr>
                                <?php
                            }
                        } else {
                            ?>
                            <tr>
                                <td style="color: red">No Birthday This Month</td>
                            </tr>

                            <?php
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
    </div>