
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Miscellaneous File</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                            <a href=<?php echo admin_url().'dashboard'?>> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href=<?php echo admin_url().'dashboard'?>>Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Miscellaneous File
                        </li>
                    </ul>
                </div>
            </div>
         
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">
                                       <div class="row bg-row" style="float: right;padding-bottom: 15px;padding-right: 15px;">
                <a class="btn btn-primary addbutt" href=<?php echo admin_url().'miscellaneousfile/addMiscellaneousfile'?>>Add New</a>  
             </div>
                                    <table class="table table-striped table-bordered table-hover" id="miscellaneousfile_table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                 <th>File Name</th>
                                                 <th>Download</th>
                                                 <th>Delete</th>
                                            </tr>   
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

