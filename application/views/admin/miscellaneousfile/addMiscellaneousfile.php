<style>
    label.error{
        color: red;
    }
</style>
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="card-block">
                <h5 class="m-b-10">Add Miscellaneous file</h5>
                <hr/>
                <form method="POST" id="addMiscellaneousfile"  action="<?php echo admin_url(). 'miscellaneousfile/addMiscellaneousfile/'?>"  enctype="multipart/form-data">

                    <div class="form-group row">
                        <label class="col-sm-2 col-form-label">Upload File</label>
                        <div class="col-sm-10">
                            <input type="file" name="files"  class="form-control">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-sm-5">
                            <input class="btn btn-primary addbutt" type="submit" name="" value="Save">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>