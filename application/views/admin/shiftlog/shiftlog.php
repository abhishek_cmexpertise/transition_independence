<style>
    .downloadbutton{
     text-align: right;
    display: block;
    width: 100%;
    } 
    
</style>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Shift Log</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                            <a href=<?php echo admin_url().'dashboard'?>> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href=<?php echo admin_url().'dashboard'?>>Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Shift Log</li>
                    </ul>
                </div>
            </div>
            <div class="row bg-row" style="margin-bottom: 50px">
                <div class="downloadbutton">
                   <a class="btn btn-primary addbutt"  href=<?php echo admin_url().'shiftlog/shiftLogallexcel'?>>Download All In Excel</a>
                   <a class="btn btn-primary addbutt" hidden="" href="#">Download All In PDF</a>   
                 </div>
            </div> 
            <div class="page-header card">
        <div class="card-block">
              
                <form method="POST" id="filtershiftlog">
                    <div class="row">
                        <div class="col-md-2">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">From</label>
                            <div class="col-sm-7">
                            <input class="form-control" data-date-formate="yyyy-mm-dd" id="startDate" width="276" />
                            </div>
                        </div>
                            </div>
                         <div class="col-md-2">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">to</label>
                            <div class="col-sm-7">
                              <input class="form-control" data-date-formate="yyyy-mm-dd" id="endDate" width="276" />
                            </div>
                        </div>
                         </div>
                         <div class="col-md-3">
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">Employee Name</label>
                            <div class="col-sm-7">
                                <select class="form-control emp_name">
                                     <option>Select Employee</option>
                                    <?php
                                    
                                        foreach ($filterEmployeedata as $filterEmployeedatas)
                                        {
                                    ?>
                                     <option value=<?php echo $filterEmployeedatas->user_id?>><?php echo $filterEmployeedatas->first_name.' '.$filterEmployeedatas->last_name?></option>
                                   <?php
                                        } 
                                   ?>
                                </select>
                            </div>
                        </div>
                         </div>
                         <div class="col-md-3">
                        <div class="form-group row">
                            <label class="col-sm-5 col-form-label">Client Name</label>
                            <div class="col-sm-7">
                                <select class="form-control client_name">
                                    <option>Select client</option>
                                          <?php
                                    
                                        foreach ($filterClientdata as $filterClientdatas)
                                        {
                                    ?>
                                    <option value=<?php echo $filterClientdatas->client_id?>><?php echo $filterClientdatas->consumer_name?></option>
                                   <?php
                                        } 
                                   ?>
                                    </select>
                            </div>
                        </div>  
                         </div>
                         <div class="col-md-1">
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <input type="button" class="btn btn-primary filtershiftlog" value="Filter">
                            </div>
                        </div>  
                       </div> 
                    </div>
                    </form>
            </div>
        </div>
          
            
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="shiftlog_table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Date</th>
                                                 <th>Client Name</th>
                                                 <th>Staff Name</th>     
                                                 <th>Total Hours</th>
                                                 <th>Total Travel Time</th>
                                                 <th>Report Show & Download</th>
                                                 <th>Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

