<table style="border-collapse: collapse; margin-bottom:20px; width:700px; border-spacing: 0;">
    <tr><td align="center"><img src="public/assets/front/images/logo.png"></td></tr>
</table>
<table cellpadding="0" cellspacing="0" style="width:100%; font-family:Arial, Helvetica, sans-serif; color: #585858; font-size:14px;border-collapse: collapse;">
    <tr>   
        <td align="left" style="border:1px solid #0277bd; width:25%; padding:5px 10px;">Date:<br /><?php echo date('m/d/Y',strtotime( $shiftlogview[0]->shiftlog_date));?></td>
        <td align="center" style="border:1px solid #0277bd; width:25%; padding:5px 10px;" align="center">Client Name:<br /><?php echo $shiftlogview[0]->consumer_name?></td>
        <td align="right" style="border:1px solid #0277bd; width:25%; padding:5px 10px;" align="center">Staff Name:<br /> <?php echo $shiftlogview[0]->first_name.' '.$shiftlogview[0]->middle_name.' '.$shiftlogview[0]->last_name;?></td>
    </tr>
</table>

<table cellpadding="0" cellspacing="0" style="width:100%; font-family:Arial, Helvetica, sans-serif; color: #585858; font-size:14px;border-collapse: collapse;">  
    <tr>
        <td style="background:#e5e9ec; width:25%; border:1px solid #0277bd; padding:5px 10px; font-style:italic;" align="left" ></td>
        <td style="background:#e5e9ec; width:50%; border:1px solid #0277bd; padding:5px 10px; font-style:italic;" align="center">Be sure to address Consumer Goals, Location, and Activities in the narrative.</td>			        
        <td style="background:#e5e9ec; width:25%; border:1px solid #0277bd; padding:5px 10px; font-style:italic;" align="right">Travel Time</td>
    </tr>

  <?php
        foreach ($shiftlogview as $shiftlogdetail)
    {
   ?>
<tr>
        <td style="width:25%; border:1px solid #0277bd; padding:5px 10px;" align="left" >
            Time Start: <?php echo $shiftlogdetail->start_time?><br/><hr />
            Time End: <?php echo $shiftlogdetail->end_time?><br /><hr />
            Total: <?php echo $shiftlogdetail->total_time?><br />
        </td>
        <td style="width:50%; border:1px solid #0277bd; padding:5px 10px;" align="center"> <?php echo $shiftlogdetail->consumer_details?></td>
        <td style="width:25%; border:1px solid #0277bd; padding:5px 10px;" align="right"> <?php echo $shiftlogdetail->travel_time?></td>
    </tr>
   <?php
    }
   ?>
</table>  

<table cellpadding="0" cellspacing="0" style="width:100%; font-family:Arial, Helvetica, sans-serif; color: #585858; font-size:14px;border-collapse: collapse;">
    <tr>
        <td align="left" style="border:1px solid #0277bd; width:25%; padding:5px 10px;">Total Hours:<br /><?php echo $shiftlogview[0]->total_hours?></td>
        <td align="center" style="border:1px solid #0277bd; width:25%; padding:5px 10px;">Total Miles Driven:<br /><?php echo $shiftlogview[0]->total_travel_time?></td>
        <td align="right" style="border:1px solid #0277bd; width:25%; padding:5px 10px;">Staff Signature:<br /><?php echo $shiftlogview[0]->first_name.' '.$shiftlogview[0]->middle_name.' '.$shiftlogview[0]->last_name;?></td>
    </tr>
</table>
