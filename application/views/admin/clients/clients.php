<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Clients</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                            <a href=<?php echo admin_url() . 'dashboard' ?>> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href=<?php echo admin_url() . 'dashboard' ?>>Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Clients</li>
                    </ul>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">
                                       <div class="row bg-row" style="float: right;padding-bottom: 15px;padding-right: 15px;">
                                        <a class="btn btn-primary addbutt" href=<?php echo admin_url() . 'clients/addClients' ?>>Add New</a>  
                                    </div>
                                    <table class="table table-striped table-bordered table-hover" id="client_table">
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th>ID</th>
                                                <th>UCI #</th>
                                                <th>Client Name</th>
                                                <th>Address</th>
                                                <th>Date Of Birth</th> 
                                                <th>Allocated Hours</th>
                                                <th>Remaining Hours</th>
                                                <th>Case Manager Name</th>
                                                <th>Case Manager Phone Number</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

