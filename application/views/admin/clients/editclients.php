<!-- Pre-loader end -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="card-block">
                <h5 class="m-b-10">Edit Client</h5>
                <hr/>
                <form method="POST" id="addclient" action=<?php echo base_url(). $formAction; ?>>
                         <div class="form-group row">
                            <div class="col-sm-5">
                                <input type="hidden" class="form-control" name="client_id" value="<?php echo  $singleClient[0]->client_id;?>"/>
                            </div>
                        </div>   
                    <div class="form-group row">
                            <label class="col-sm-2 col-form-label">UCI#</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="client_uci" maxlength="15" placeholder="Enter UCI#" value=<?php echo $singleClient[0]->client_uci?>>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Client Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="consumer_name" placeholder="Enter Client Name" value=<?php echo $singleClient[0]->consumer_name?>>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Address Line 1</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="address1" placeholder="Enter Address Line 1" value=<?php echo $singleClient[0]->address1?>>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">City, State, Zip</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name = "address2" placeholder="Enter City, State, Zip" value=<?php echo $singleClient[0]->address2?>>
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Date Of Birth</label>
                            <div class="col-sm-5">
                                <input  class="form-control" type="text" name="client_dob" data-date-format="yyyy-mm-dd" placeholder="Click Date Of Birth"  id="date" value=<?php echo $singleClient[0]->client_dob?>>

                            </div>
                        </div>  
                     <div class="form-group row">
                            <label class="col-sm-2 col-form-label"> Emergency Contact No</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name = "emergency_contact"  maxlength="10" placeholder="Enter Emergency Contact No" value=<?php echo $singleClient[0]->emergency_contact?>>
                            </div>
                        </div>  
                   	
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Disability</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="disability" placeholder="Enter Disability" value=<?php echo $singleClient[0]->disability?>>
                            </div>
                        </div>  
                      <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Case Manager Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="manager_name" placeholder="Enter Case Manager Name" value=<?php echo $singleClient[0]->manager_name?>>
                            </div>
                        </div>  	
                         <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Case Manager Phone Number</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="manager_phone" maxlength="10" placeholder="Enter Case Manager Phone Number" value=<?php echo $singleClient[0]->manager_phone?>>
                            </div>
                        </div>	
                         <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Client's Allocated Hours</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="allocated_hours" placeholder="00:00:00" value=<?php echo $singleClient[0]->allocated_hours?>>
                            </div>
                        </div>
                    <div class="form-group row">
                            <div class="col-sm-5">
                                <input type="submit" class="btn btn-primary"  value="Edit">
                            </div>
                        </div>  
                        
                    </form>
            </div>
        </div>
    </div>
</div>
 