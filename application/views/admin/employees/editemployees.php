<!-- Pre-loader end -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="card-block">
                <h5 class="m-b-10">Edit Employee</h5>
                <hr/>
                <form method="POST" id="editemployee" action=<?php echo base_url(). $formAction; ?>>
                    <div class="form-group row">
                         
                            <div class="col-sm-5">
                                <input type="hidden" class="form-control" name="user_id" value="<?php echo  $singleEmployee[0]->user_id;?>"/>
                            </div>
                        </div>   
                   
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Username</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" readonly="" value="<?php echo $singleEmployee[0]->username?>">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Change Password</label>
                            <div class="col-sm-5">
                                <input type="password" class="form-control"  name="fresh_password" value="<?php echo $singleEmployee[0]->fresh_password?>" placeholder="Enter Password">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="first_name" value="<?php echo $singleEmployee[0]->first_name?>" placeholder="Enter First Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Middle Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="middle_name" value="<?php echo $singleEmployee[0]->middle_name?>" placeholder="Enter Middle Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Last Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="last_name" value="<?php echo $singleEmployee[0]->last_name?>"placeholder="Enter Last Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-5">
                                <input type="email" class="form-control" name="user_email" id="user_email" value="<?php echo $singleEmployee[0]->user_email?>" placeholder="Enter Email">
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Date Of Birth</label>
                            <div class="col-sm-5">
                                <input  class="form-control" type="text" placeholder="Click Date Of Birth" name="dob"  id="date"  value="<?php echo $singleEmployee[0]->dob?>">
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Social Security Number</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="social_security_no" value="<?php echo $singleEmployee[0]->social_security_no?>" placeholder="Enter Social Security Number">
                            </div>
                        </div>  
                      <div class="form-group row">
                            <div class="col-sm-5">
                                <input type="submit" class="btn btn-primary"  value="Edit">
                            </div>
                        </div>  
                    </form>
            </div>
        </div>
    </div>
</div>






