<nav class="pcoded-navbar" navbar-theme="themelight1" active-item-theme="theme4" sub-item-theme="theme2" active-item-style="style0" pcoded-navbar-position="fixed">
    <div class="sidebar_toggle"><a href="#"><i class="icon-close icons"></i></a>
    </div>
    <div class="pcoded-inner-navbar main-menu mCustomScrollbar _mCS_1" style="height: calc(100% - 90px);"><div id="mCSB_1" class="mCustomScrollBox mCS-light mCSB_vertical mCSB_inside" style="max-height: none;" tabindex="0"><div id="mCSB_1_container" class="mCSB_container" style="position: relative; top: 0px; left: 0px;" dir="ltr">
                <div class="pcoded-navigatio-lavel" data-i18n="nav.category.navigation" menu-title-theme="theme1"></div>
                <ul class="pcoded-item pcoded-left-item" item-border="true" item-border-style="none" subitem-border="true">
                    <li class="<?php
                    if ($this->uri->segment(2, 0) == 'dashboard') {
                        echo 'active';
                    }
                    ?>">
                        <a href="<?php echo admin_url() . "dashboard"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-tachometer" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Dashboard</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="<?php
                    if ($this->uri->segment(2, 0) == 'supervisors') {
                        echo 'active';
                    }
                    ?>">
                        <a href="<?php echo admin_url() . "supervisors"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-user" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Supervisors</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="<?php
                    if ($this->uri->segment(2, 0) == 'employees') {
                        echo 'active';
                    }
                    ?>">
                        <a href="<?php echo admin_url() . "employees"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-user-plus" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Employees</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="<?php
                    if ($this->uri->segment(2, 0) == 'clients') {
                        echo 'active';
                    }
                    ?>">
                        <a href="<?php echo admin_url() . "clients"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-users" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Cliants</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="<?php
                        if ($this->uri->segment(2, 0) == 'shiftlog') {
                            echo 'active';
                        }
                    ?>">
                        <a href="<?php echo admin_url() . "shiftlog"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-calendar" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Shift Log</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="<?php
                        if ($this->uri->segment(2, 0) == 'miscellaneousfile') {
                            echo 'active';
                        }
                        ?>">
                        <a href="<?php echo admin_url() . "miscellaneousfile"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-file" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Miscellaneous Files</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="<?php
                        if ($this->uri->segment(2, 0) == 'clientreport') {
                            echo 'active';
                        }
                        ?>">
                        <a href="<?php echo admin_url() . "clientreport"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-flag" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Client's Monthly Report</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>   
                    <li class="<?php
                        if ($this->uri->segment(2, 0) == 'employeetimesheet') {
                            echo 'active';
                        }
                        ?>">
                        <a href="<?php echo admin_url() . "employeetimesheet"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-clock-o" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Employee Timesheet</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>   
                    <li class="<?php
                    if ($this->uri->segment(2, 0) == 'Trainingtimesheet') {
                        echo 'active';
                    }
                        ?>">
                        <a href="<?php echo admin_url() . "Trainingtimesheet"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-list" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Training Time sheet</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                    <li class="<?php
                    if ($this->uri->segment(2, 0) == 'Monthlyrecap') {
                        echo 'active';
                    }
                        ?>">
                        <a href="<?php echo admin_url() . "Monthlyrecap"; ?>">
                            <span class="pcoded-micon"><i class="fa fa-search" aria-hidden="true"></i><b>D</b></span>
                            <span class="pcoded-mtext" data-i18n="nav.dash.main">Monthly Recap</span>
                            <span class="pcoded-mcaret"></span>
                        </a>
                    </li>
                </ul>
            </div>
            <div id="mCSB_1_scrollbar_vertical" class="mCSB_scrollTools mCSB_1_scrollbar mCS-light mCSB_scrollTools_vertical" style="display: block;"><div class="mCSB_draggerContainer"><div id="mCSB_1_dragger_vertical" class="mCSB_dragger" style="position: absolute; min-height: 30px; display: block; height: 341px; max-height: 393px; top: 0px;"><div class="mCSB_dragger_bar" style="line-height: 30px;"></div></div><div class="mCSB_draggerRail"></div></div></div></div></div>

</nav>
