<head>
    <title><?php echo $var_meta_title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <meta name="author" content="">

    <style type="text/css">
        label.error{
            color: red;
        }
    </style>

    <link rel="icon" href=<?php echo base_url() . "public/assets/front/images/favicon.ico"; ?> type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css" rel="stylesheet">
    
    
   
   
    <?php
    if (isset($css)) {
        foreach ($css as $allCss) {
            if (filter_var($allCss, FILTER_VALIDATE_URL) === FALSE) {
                ?>
                <link  rel="stylesheet" type="text/css" href="<?php echo CSS_FILES . $allCss . '.css'; ?>">
            <?php } else { ?>
                <link  rel="stylesheet" type="text/css" href="<?php echo $allCss; ?>">
                <?php
            }
        }
    }
    ?>
 
    <script type="text/javascript">
        var base_url = '<?php echo base_url(); ?>';
        var supervisor_url = '<?php echo supervisor_url(); ?>';
    </script>

</head>