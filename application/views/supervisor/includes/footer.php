<!--Delete Record Alert -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" id = "modelTitle">Modal Header</h4>
            </div>
            <div class="modal-body" id = "modelContent"></div>
            <div class="modal-footer" id = "modelFooter"></div>
        </div>
    </div>
</div>




    
<?php
if (isset($js)) {
    foreach ($js as $alljs) {
        if (filter_var($alljs, FILTER_VALIDATE_URL) === FALSE) {
            if (file_exists('public/assets/back/js/supervisor/' . $alljs . '.js')) {
                $path = JS_FILES . 'supervisor/' . $alljs . '.js?v=' . rand(1, 999);
            } else {
                $path = JS_FILES . $alljs . '.js?v=' . rand(1, 999);
            }
            ?>
            <script type="text/javascript" src="<?php echo $path; ?>"></script>
        <?php } else { ?>
            <script type="text/javascript" src="<?php echo $alljs; ?>"></script>
        <?php
        }
    }
}
?>

<script>
jQuery(document).ready(function () {
    <?php
    if (!empty($init)) {
        foreach ($init as $value) {
            echo $value . ';';
        }
    }
    ?>
});
</script>