<style>
    .shiftlog table tr td label{
        padding-bottom: 5px;
	display: block;
    }
    .shiftlog table tr td{
        white-space: normal;
        text-align: justify;
    }
    .shiftlog{max-width: 1215px;
    margin: 0 auto;
    width: 100%;}
    .shiftlog h4{margin: 30px 0px; display: inline-block; text-align: center; font-size: 28px;
    width: 100%;}
    .shiftlog .table > thead > tr > th,.shiftlog .table > tfoot > tr > td{border: 0;border-bottom: 1px solid #0071f3;width: 33.33%;}
    .shiftlog .table > thead > tr > th label{display: block;}
    .shiftlog .table  tfoot  tr  td.full-td{text-align: center;}
    .shiftlog .table  tfoot  tr  td.full-td a{padding: 5px 20px;
    margin: 0 10px;
    border: 0;
    cursor: pointer;display: inline-block;background: linear-gradient(to right, #004ef9, #79a3ff) !important;
    color: #fff;
    font-size: 16px;text-transform: capitalize;}
   .shiftlog .table-bordered td,.shiftlog .table-bordered th,.shiftlog .table-bordered {
    border: 1px solid #0071f3;
     background: #fff;
}
</style>
        
<div class="shiftlog">
    <h4>Shift log View</h4>
     <table class="table table-bordered">
    <thead>
      <tr>
            <th>
                <label>Date</label>
                <?php echo date('m/d/Y',strtotime($shiftlogview[0]->shiftlog_date));?>
            </th>
            <th>
                <label>Client Name:</label>
                <?php echo $shiftlogview[0]->consumer_name;?>
             </th>
             <th>
                <label>Staff Name:</label>
                 <?php echo $shiftlogview[0]->first_name.' '.$shiftlogview[0]->middle_name.' '.$shiftlogview[0]->last_name;?>
             </th>
      </tr>
    </thead>
    <tbody>
      <tr>
         <td></td>
        <td>Be sure to address Client Goals, Location, and Activities in the narrative.</td>
        <td>Travel Time</td>
      </tr>
<!--      stat-->
<?php
        if(count($shiftlogview)>0)
        {
            foreach ($shiftlogview as $shiftlogdetail)
            {
?>
      <tr>
        <td>Time Start: <?php echo $shiftlogdetail->start_time;?></td>
        <td rowspan="3"><p> <?php echo $shiftlogdetail->consumer_details;?></p></td>
        <td rowspan="3"><?php echo $shiftlogdetail->travel_time;?></td>
      </tr>
       <tr>
        <td>Time End:<?php echo $shiftlogdetail->end_time;?></td>
      </tr>
        <tr>
        <td>Total:<?php echo $shiftlogdetail->total_time;?></td>
       </tr>
       <?php
            }
        }
        else
        {
          ?>
    <td>Empty</td>
       <?php
        }
       ?>
<!--       end-->
    </tbody>
     <tfoot>
      <tr>
          <td>
              <label>Hours Worked:</label>
               <?php echo $shiftlogview[0]->total_hours;?>
          </td>
           <td>
              <label>Total Travel Time:</label>
              <?php echo $shiftlogview[0]->total_travel_time;?>
           </td>
           <td>
              <label>Staff Signature:</label>
               <?php echo $shiftlogview[0]->first_name.' '.$shiftlogview[0]->middle_name.' '.$shiftlogview[0]->last_name;?>
           </td>
      </tr>
      <tr>
          <td class="full-td" colspan="3">
              <a href="<?php echo supervisor_url().'shiftlog'?>">back</a>
          </td>
      </tr>
    </tfoot>
  </table>
</div>
 