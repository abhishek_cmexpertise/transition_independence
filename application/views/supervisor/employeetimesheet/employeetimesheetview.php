<script>
    var shiftlog_id = "<?php echo $id?>";
</script>
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Employee Timesheet</h5>
                   
                </div>
            </div>
            <div class="row bg-row" style="margin-bottom: 50px">
                 
                <a class="btn btn-primary btn-sm addbutt" href=<?php echo supervisor_url().'employeetimesheet'?>>back</a>  
             </div>
            <div class="box box-primary">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="employeereport_table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Date</th>
                                                <th>Client Name</th>
                                                <th>Staff Name</th>     
                                                <th>Total Hours</th>
                                                <th>Total Travel Time</th>
                                            </tr>
                                             <tr>
                                                <th colspan="3">Sub-Total Hours : <?php echo $emptimesheetview[0];?></th>
                                                <th colspan="3">Sub-Total Travel Time : <?php echo  $emptimesheetview[1];?></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                       
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

