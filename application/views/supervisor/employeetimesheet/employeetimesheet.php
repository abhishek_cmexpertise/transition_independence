
<div class="pcoded-inner-content">
    <div class="main-body">
        <div class="page-wrapper">
            <div class="page-header card">
                <div class="card-block">
                    <h5 class="m-b-10">Employee report</h5>
                    <ul class="breadcrumb-title b-t-default p-t-10">
                        <li class="breadcrumb-item">
                            <a href=<?php echo supervisor_url().'dashboard'?>> <i class="fa fa-home"></i> </a>
                        </li>
                        <li class="breadcrumb-item"><a href=<?php echo supervisor_url().'dashboard'?>>Dashboard</a>
                        </li>
                        <li class="breadcrumb-item">Employee report</li>
                    </ul>
                </div>
            </div>
          
            <div class="box box-primary">
                <div class="box-header with-border ">
                    <?php echo $this->session->flashdata('myMessage'); ?>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-body">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="employeereport_table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                    <th>Submission Date</th>
                                                    <th>Employee name</th>
                                                    <th>Report Month</th>
                                                    <th>Report Year</th>     
                                                    <th>Sub-Total Hours</th>
                                                    <th>Sub-Total Travel Time</th>
                                                    <th>Report Show & Download</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

