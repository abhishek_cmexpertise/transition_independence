<!-- Pre-loader end -->
<div class="main-body">
    <div class="page-wrapper">
        <!-- Page-header start -->
        <div class="page-header card">
            <div class="card-block">
                <h5 class="m-b-10">Add Employee</h5>
                <hr/>
                <form method="POST" id="addemployee" action=<?php echo supervisor_url().'employees/addEmployees'?>>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="first_name" placeholder="Enter First Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Middle Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="middle_name" placeholder="Enter Middle Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Last Name</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" name="last_name" placeholder="Enter Last Name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-5">
                                <input type="email" class="form-control" name = user_email id="user_email" placeholder="Enter Your Email">
                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Date Of Birth</label>
                            <div class="col-sm-5">
                                <input  class="form-control" type="text" name="dob" data-date-format="yyyy-mm-dd" placeholder="Click Date Of Birth"  id="date">

                            </div>
                        </div>  
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Social Security Number</label>
                            <div class="col-sm-5">
                                <input type="text" class="form-control" maxlength="9" name="social_security_no" placeholder="Enter Social Security Number">
                            </div>
                        </div>  
                        <div class="form-group row">
                            <div class="col-sm-5">
                                <input type="submit" class="btn btn-primary"  value="Add">
                            </div>
                        </div>  
                        
                    </form>
            </div>
        </div>
    </div>
</div>
 