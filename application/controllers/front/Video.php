<?php

require_once APPPATH . "config/tablenames_constants.php";

class Video extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('front/Video_model', 'this_model');
    }

    public function index() {
        $data['page'] = "front/page/video";
        $data['var_meta_title'] = 'video';
        $data['var_meta_description'] = 'video';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'toastr.min',
            'common_function'
        ];
        $data['show_banner'] = $this->this_model->show_banner();
        $data['video'] = $this->this_model->video();
        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>