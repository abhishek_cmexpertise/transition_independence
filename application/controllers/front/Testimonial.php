<?php

require_once APPPATH . "config/tablenames_constants.php";

class Testimonial extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('front/Testimonial_model', 'this_model');
    }

    public function index() {
        $data['page'] = "front/page/testimonial";
        $data['formAction'] = "front/page/Testimonial";
        $data['var_meta_title'] = 'testimonial';
        $data['var_meta_description'] = 'testimonial';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'toastr.min',
            'common_function'
        ];
        $data['show_banner'] = $this->this_model->show_banner();
        $data['testimonials'] = $this->this_model->testimonials();

        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>