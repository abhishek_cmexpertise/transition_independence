<?php
require_once APPPATH . "config/tablenames_constants.php";

class Art extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('front/Art_model', 'this_model');
    }

    public function arts($id) {

        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['page'] = "front/page/art";
        $data['formAction'] = "front/page/Art/arts/" . $id;
        $data['var_meta_title'] = 'art';
        $data['var_meta_description'] = 'art';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'art',
            'toastr.min',
            'common_function'
        ];
        $data['init'] = ['Art.init()'];
        $data['show_banner'] = $this->this_model->show_banner();
        $data['row_art'] = $this->this_model->row_art($ids);
        $data['art_gallery'] = $this->this_model->art_gallery($ids);
        $this->load->view(FRONT_LAYOUT, $data);
    }
}

?>