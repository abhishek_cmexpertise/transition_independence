<?php

require_once APPPATH . "config/tablenames_constants.php";

class Contact extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('front/Contact_model', 'this_model');
    }

    public function index() {

        $data['page'] = "front/page/contact";
        $data['var_meta_title'] = 'contact';
        $data['var_meta_description'] = 'contact';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'toastr.min',
            'common_function',
            'contact'
        ];
        $data['init'] = ['Contact.init()'];
        $data['show_banner'] = $this->this_model->show_banner();
        $data['contacts'] = $this->this_model->contacts();
        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>