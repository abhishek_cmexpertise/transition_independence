<?php

require_once APPPATH . "config/tablenames_constants.php";

class Patient extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('front/Patient_model', 'this_model');
    }

    public function index() {

        $data['page'] = "front/page/patient";
        $data['var_meta_title'] = 'patient';
        $data['var_meta_description'] = 'patient';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'toastr.min',
            'common_function'
        ];
        $data['show_banner'] = $this->this_model->show_banner();
        $data['patient'] = $this->this_model->patient();

        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>