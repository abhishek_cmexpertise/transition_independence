<?php

require_once APPPATH . "config/tablenames_constants.php";

class About extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('front/About_model', 'this_model');
    }

    public function index() {

        $data['page'] = "front/page/about";
        $data['var_meta_title'] = 'about';
        $data['var_meta_description'] = 'about';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
            ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'account',
            'toastr.min',
            'common_function',
        ];
        $data['show_banner'] = $this->this_model->show_banner();
        $data['show_cms'] = $this->this_model->show_cms();
        $data['about'] = $this->this_model->about();

        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>