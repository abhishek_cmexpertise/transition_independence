<?php

require_once APPPATH . "config/tablenames_constants.php";

class Employment_application extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('front/Employment_application_model', 'this_model');
    }

    public function index() {
        
        $data['page'] = "front/page/employment_application";
        $data['var_meta_title'] = 'employment_application';
        $data['var_meta_description'] = 'employment_application';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'toastr.min',
            'common_function',
            'employment_application'
        ];
        $data['init'] = ['Employment_application.init()'];
        $data['show_banner'] = $this->this_model->show_banner();
        $data['show_cms'] = $this->this_model->show_cms();
        $data['show_cms2'] = $this->this_model->show_cms2();
        $data['show_cms3'] = $this->this_model->show_cms3();
        $this->load->view(FRONT_LAYOUT, $data);
    }
    
    public function Job_Description_Instructor()
    {
           $data['page'] = "front/page/jobdescriptioninstructor";
        $data['var_meta_title'] = 'Job Description Instructor';
        $data['var_meta_description'] = 'Job Description Instructor';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'toastr.min',
            'common_function',
            'employment_application'
        ];
        
        $data['banner'] = $this->this_model->JobDescriptionInstructorbanner();
        $data['cms'] = $this->this_model-> JobDescriptionInstructorcms();
     
         $this->load->view(FRONT_LAYOUT, $data);
    }
    
    public function Job_Description_Supervisor()
    {
           $data['page'] = "front/page/jobdescriptionsupervisor";
        $data['var_meta_title'] = 'Job Description Supervisor';
        $data['var_meta_description'] = 'Job Description Supervisor';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'toastr.min',
            'common_function',
            'employment_application'
        ];
        
        $data['banner'] = $this->this_model->JobDescriptionsupervisorbanner();
        $data['cms'] = $this->this_model-> JobDescriptionsupervisorcms();
     
         $this->load->view(FRONT_LAYOUT, $data);
    }
    
    public function employeement_application()
    {
       if($this->input->post())
       {    
          $response = $this->this_model->Employeementapplication($this->input->post());
       
           echo json_encode($response);
            exit();
       }
    }
    
}

?>