<?php

require_once APPPATH . "config/tablenames_constants.php";

class Team extends CI_Controller {

    function __construct() {
        parent::__construct();
        
          $this->load->model('front/Team_model', 'this_model');
    }

    public function index() {

        $data['page'] = "front/page/team";
        $data['var_meta_title'] = 'team';
        $data['var_meta_description'] = 'team';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'toastr.min',
            'common_function'
        ];
        $data['team'] = $this->this_model->team();
        $this->load->view(FRONT_LAYOUT, $data);
        
        
    }

}

?>