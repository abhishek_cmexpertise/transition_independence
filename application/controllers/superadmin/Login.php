<?php

require_once APPPATH . "config/tablenames_constants.php";

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/dashboard');
        }

        $this->load->model('superadmin/Login_model', 'this_model');
    }

    function index() {

        $data['page'] = "superadmin/login";
        $data['formAction'] = SITE_URL . 'superadmin/login';
        $data['js'] = array('login.js', 'common');
        $data['init'] = array('Login.init()');

        if ($this->input->post()) {
            $loginCheck = $this->this_model->loginCheck($this->input->post());
            echo json_encode($loginCheck);
            exit;
        }

        $this->load->view(SUPERAMDIN_LAYOUT, $data);
    }

}

?>