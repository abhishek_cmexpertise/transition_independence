<?php

class Contact extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Contact_model', 'this_model');
    }

    public function index() {

        $data['page'] = "superadmin/contact_banner_list";
        $data['page_title'] = "Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'contact_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/contact';
        $data['banner_mgmt'] = $this->this_model->getContactListData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function contactBanner($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/contact/contactBanner/' . $id;
        $data['page'] = "superadmin/contact_banner_edit";
        $data['page_title'] = "Edit Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'contact_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/contact';
        $data['js'] = array('contact.js', 'common');
        $data['init'] = array('Contact.init()');
        $data ['ContactBannerData'] = $this->this_model->getContactBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editContactBannerData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/contact');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function contactInfo() {

        $data['page'] = "superadmin/contact_info_list";
        $data['page_title'] = "Contact Info Details";
        $data['module_name'] = "Home Page";
        $data['action'] = 'contact_info';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['js'] = array('contact.js', 'common');
        $data['init'] = array('Contact.list()');
        $data['formAction'] = base_url() . 'superadmin/contact/contactInfo';
        $data['banner_mgmt'] = $this->this_model->getContactInfoData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function addContactInfo() {

        $data['page'] = "superadmin/contact_info_add";
        $data['page_title'] = "Add Contact Info Details";
        $data['action'] = 'contact_info';
        $data['module_name'] = "Contact Info Details";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/contact/contactInfo';
        $data['js'] = array('contact.js', 'common');
        $data['init'] = array('Contact.info()');
        $data['formAction'] = base_url() . 'superadmin/contact/addContactInfo';

        if ($this->input->post()) {
            $response = $this->this_model->addContactInfoData($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/contact/contactInfo');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editContactInfo($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/contact/editContactInfo/' . $id;
        $data['page'] = "superadmin/contact_info_add";
        $data['page_title'] = "Edit Contact Info Details";
        $data['module_name'] = "Contact Info Details";
        $data['action'] = 'contact_info';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/contact/contactInfo';
        $data['js'] = array('contact.js', 'common');
        $data['init'] = array('Contact.info()');
        $data ['ContactInfoData'] = $this->this_model->getContactInfoEditDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editContactInfoData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/contact/contactInfo');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/contact/contactInfo');
        } else {
            $response = $this->this_model->deleteContactDetail($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/contact/contactInfo');
        }
    }

    public function updateStatus() {
        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->updateStatus($this->input->post());
        }
    }

    public function handleMultiple() {

        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->handleMultiples($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            echo json_encode($response);
            exit;
        }
    }

}

?>