<?php

class Empopportunities extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Empopportunities_model', 'this_model');
    }

    public function index() {

        $data['page'] = "superadmin/emp_banner_list";
        $data['page_title'] = "Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'emp_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Empopportunities';
        $data['banner_mgmt'] = $this->this_model->getEmpBannerData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editEmpBanner($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/empopportunities/editEmpBanner/' . $id;
        $data['page'] = "superadmin/emp_banner_edit";
        $data['page_title'] = "Edit Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'emp_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Empopportunities';
        $data['js'] = array('emp.js', 'common');
        $data['init'] = array('Emp.init()');
        $data ['EmpBannerData'] = $this->this_model->getEmpBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editEmpBannerData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Empopportunities');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function empOportunityList() {

        $data['page'] = "superadmin/emp_oportunity_list";
        $data['page_title'] = "Page Content";
        $data['module_name'] = "Home Page";
        $data['action'] = 'emp_oportunity';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Empopportunities/empOportunityList';
        $data['EmpData'] = $this->this_model->getEmpOportunityData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editEmpOportunity($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/Empopportunities/editEmpOportunity/' . $id;
        $data['page'] = "superadmin/emp_oportunity_edit";
        $data['page_title'] = "Edit Page Content";
        $data['module_name'] = "Page Content";
        $data['action'] = 'emp_oportunity';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Empopportunities/empOportunityList';
        $data['js'] = array('contact.js', 'common');
        $data['init'] = array('Contact.init()');
        $data ['EmpOpoData'] = $this->this_model->getEditEmpOportunityDetail($ids);


        if ($this->input->post()) {
            $response = $this->this_model->editEmpOportunityData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Empopportunities/empOportunityList');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function empInstructorJobList() {

        $data['page'] = "superadmin/emp_instructor_list";
        $data['page_title'] = "Page Content";
        $data['module_name'] = "Home Page";
        $data['action'] = 'emp_instructor_job';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Empopportunities/empInstructorJobList';
        $data['InstructorData'] = $this->this_model->getInstructorJobData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editEmpInstructorJobList($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/empopportunities/editEmpInstructorJobList/' . $id;
        $data['page'] = "superadmin/emp_instructor_edit";
        $data['page_title'] = "Edit Page Content";
        $data['module_name'] = "Page Content";
        $data['action'] = 'emp_instructor_job';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Empopportunities/empInstructorJobList';
        $data['js'] = array('emp.js', 'common');
        $data['init'] = array('Emp.instruct()');
        $data ['EmpInstructJobData'] = $this->this_model->getEmpInstuctorJobDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editInstructorData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Empopportunities/empInstructorJobList');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function empSupervisorJobList() {

        $data['page'] = "superadmin/emp_supervisor_list";
        $data['page_title'] = "Page Content";
        $data['module_name'] = "Home Page";
        $data['action'] = 'emp_supervisor_job';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Empopportunities/empSupervisorJobList';
        $data['SupervisorData'] = $this->this_model->getSupervisorJobData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editSupervisorJobList($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/empopportunities/editSupervisorJobList/' . $id;
        $data['page'] = "superadmin/emp_supervisor_edit";
        $data['page_title'] = "Edit Page Content";
        $data['module_name'] = "Page Content";
        $data['action'] = 'emp_supervisor_job';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Empopportunities/editSupervisorJobList';
        $data['js'] = array('emp.js', 'common');
        $data['init'] = array('Emp.supervisor()');
        $data['EmpSupervisorJobData'] = $this->this_model->getSupervisorJobDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editSupervisorData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Empopportunities/empSupervisorJobList');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

}

?>