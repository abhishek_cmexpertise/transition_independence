<?php

class Video extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Video_model', 'this_model');
    }

    public function index() {
        $data['page'] = "superadmin/video_banner_list";
        $data['page_title'] = "Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'video_list';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Video';
        $data['banner_mgmt'] = $this->this_model->getVideoBannerData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function videoBannerEdit($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/Video/videoBannerEdit/' . $id;
        $data['page'] = "superadmin/video_banner_edit";
        $data['page_title'] = "Edit Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'video_list';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Video';
        $data['js'] = array('video.js', 'common');
        $data['init'] = array('Video.init()');
        $data ['BannerData'] = $this->this_model->getVideBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editVideBannerDatas($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Video');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function mng_video() {

        $data['page'] = "superadmin/video_info_list";
        $data['page_title'] = "Video Details";
        $data['module_name'] = "Home Page";
        $data['action'] = 'mng_video';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['js'] = array('video.js', 'common');
        $data['init'] = array('Video.opt()');
        $data['formAction'] = base_url() . 'superadmin/Video/mng_video';
        $data['video_mgmt'] = $this->this_model->getVideoInfoData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function addVideoInfo() {

        $data['page'] = "superadmin/video_info_add";
        $data['page_title'] = "Add Video Details";
        $data['action'] = 'mng_video';
        $data['module_name'] = "Video Details";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Video/mng_video';
        $data['js'] = array('video.js', 'common');
        $data['init'] = array('Video.add()');
        $data['formAction'] = base_url() . 'superadmin/Video/addVideoInfo';

        if ($this->input->post()) {
            $response = $this->this_model->addVideoInfoData($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Video/mng_video');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editVideoInfo($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }


        $data['formAction'] = base_url() . 'superadmin/Video/editVideoInfo/' . $id;
        $data['page'] = "superadmin/video_info_add";
        $data['page_title'] = "Edit Video Details";
        $data['module_name'] = "Video Details";
        $data['action'] = 'mng_video';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Video/mng_video';
        $data['js'] = array('video.js', 'common');
        $data['init'] = array('Video.add()');
        $data ['VideoData'] = $this->this_model->getVidData($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editVid($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Video/mng_video');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/Video/mng_video');
        } else {
            $response = $this->this_model->deleteVideoDetail($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Video/mng_video');
        }
    }

    public function updateStatus() {
        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->updateStatus($this->input->post());
        }
    }

    public function handleMultiple() {

        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->handleMultiples($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            echo json_encode($response);
            exit;
        }
    }

}

?>