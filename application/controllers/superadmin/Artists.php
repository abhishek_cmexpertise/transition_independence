<?php

class Artists extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Artists_model', 'this_model');
    }

    public function index() {
        $data['page'] = "superadmin/artists_banner_list";
        $data['page_title'] = "Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'artists_list';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Artists';
        $data['banner_mgmt'] = $this->this_model->getArtistsBannerData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editArtistsBannerData($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/Artists/editArtistsBannerData/' . $id;
        $data['page'] = "superadmin/artists_banner_edit";
        $data['page_title'] = "Edit Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'artists_list';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Artists';
        $data['js'] = array('artists.js', 'common');
        $data['init'] = array('Artists.init()');
        $data ['BannerData'] = $this->this_model->getArtistBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editArtistBannerDatas($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Artists');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function mng_artist() {

        $data['page'] = "superadmin/artist_info_list";
        $data['page_title'] = "Artist Details";
        $data['module_name'] = "Home Page";
        $data['action'] = 'art_mgmt';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['js'] = array('artists.js', 'common');
        $data['init'] = array('Artists.add()');
        $data['formAction'] = base_url() . 'superadmin/Artists/mng_artist';
        $data['art_mgmt'] = $this->this_model->getArtistInfoData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function addArtist() {

        $data['page'] = "superadmin/artist_info_add";
        $data['page_title'] = "Add Artist Details";
        $data['action'] = 'art_mgmt';
        $data['module_name'] = "Artist";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Artists/mng_artist';
        $data['js'] = array('artists.js', 'common');
        $data['init'] = array('Artists.addArtist()');
        $data['formAction'] = base_url() . 'superadmin/Artists/addArtist';

        if ($this->input->post()) {
            $response = $this->this_model->addArtistData($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Artists/mng_artist');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editArtist($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/Artists/editArtist/' . $id;
        $data['page'] = "superadmin/artist_info_add";
        $data['page_title'] = "Edit Artist Details";
        $data['module_name'] = "Artist Details";
        $data['action'] = 'art_mgmt';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Artists';
        $data['js'] = array('artists.js', 'common');
        $data['init'] = array('Artists.addArtist()');
        $data ['ArtistData'] = $this->this_model->getArtistDetail($ids);


        if ($this->input->post()) {
            $response = $this->this_model->editArtistDatas($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Artists/mng_artist');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/Artists/mng_artist');
        } else {
            $response = $this->this_model->deleteTestimonialDetail($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Artists/mng_artist');
        }
    }

    public function updateStatus() {
        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->updateStatus($this->input->post());
        }
    }

    public function handleMultiple() {

        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->handleMultiples($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            echo json_encode($response);
            exit;
        }
    }

    public function AddArtistImage($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['page'] = "superadmin/artist_image_list";
        $data['page_title'] = "Manage Gallery Photos";
        $data['module_name'] = " Artists Gallery";
        $data['action'] = 'art_mgmt';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data ['tst'] = base_url() . 'superadmin/Artists/AddImg/' . $id;
        $data['formAction'] = base_url() . 'superadmin/Artists/mng_artist';

        $data['js'] = array('artists.js', 'common');
        $data['init'] = array('Artists.img()');

        $data['image_mgmt'] = $this->this_model->getArtistImageData($ids);
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function deleteImage($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/Artists/mng_artist');
        } else {
            $response = $this->this_model->deleteArtistImage($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Artists/mng_artist');
        }
    }

    public function AddImg($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['page'] = "superadmin/artist_image_add";
        $data['page_title'] = "Add Artist Image";
        $data['action'] = 'art_mgmt';
        $data['module_name'] = "Artist";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Artists/mng_artist';
        $data['js'] = array('artists.js', 'common');
        $data['init'] = array('Artists.img()');
        $data['formAction'] = base_url() . 'superadmin/Artists/AddImg/' . $id;

        if ($this->input->post()) {
            $response = $this->this_model->addimg($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Artists/AddArtistImage/' . $id);
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

}

?>