<?php

class Tools extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Tools_model', 'this_model');
    }

    public function index() {

        $data['page'] = "superadmin/settings";
        $data['page_title'] = "Settings";
        $data['action'] = 'tools_setting';
        $data['formAction'] = base_url() . 'superadmin/tools';
        $data['settings_data'] = $this->this_model->getSettingsData();
        $data['js'] = array('tools.js');
        $data['init'] = array('Tools.init()');

        if ($this->input->post()) {
            $result = $this->this_model->editSettings($this->input->post());
            if ($result) {
                $this->utility->setFlashMessage($result['status'], $result['message']);
                redirect(base_url() . 'superadmin/tools');
            }
        }
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function changePassword() {

        $data['page'] = "superadmin/change_password";
        $data['page_title'] = "Change Password";
        $data['action'] = 'tools_change_password';
        $data['formAction'] = base_url() . 'superadmin/tools/changePassword';
        $data['settings_data'] = $this->this_model->getSettingsData();
        $data['js'] = array('tools.js');
        $data['init'] = array('Tools.changePassword()');

        if ($this->input->post()) {
            $result = $this->this_model->change_pass($this->input->post());
            $this->utility->setFlashMessage($result['status'], $result['message']);
            redirect(base_url() . 'superadmin/tools/changePassword');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function logout() {
        $this->session->sess_destroy();
        redirect(base_url() . 'admin');
    }

}

?>