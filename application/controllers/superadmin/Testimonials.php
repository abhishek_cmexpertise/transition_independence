<?php

class Testimonials extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Testimonials_model', 'this_model');
    }

    public function index() {
        $data['page'] = "superadmin/testimonials_banner_list";
        $data['page_title'] = "Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'testimonials';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Testimonials';
        $data['banner_mgmt'] = $this->this_model->getTestimonialsBannerData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editTestimonialBannerData($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/Testimonials/editTestimonialBannerData/' . $id;
        $data['page'] = "superadmin/testimonial_banner_edit";
        $data['page_title'] = "Edit Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'testimonials';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Testimonials';
        $data['js'] = array('testimonials.js', 'common');
        $data['init'] = array('Testimonials.init()');
        $data ['BannerData'] = $this->this_model->getTestimonialBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editTestimonialBannerDatas($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Testimonials');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function mng_testimonials() {

        $data['page'] = "superadmin/testimonials_info_list";
        $data['page_title'] = "Testimonials";
        $data['module_name'] = "Home Page";
        $data['action'] = 'testimonials_mgmt';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['js'] = array('testimonials.js', 'common');
        $data['init'] = array('Testimonials.front()');
        $data['formAction'] = base_url() . 'superadmin/Testimonials/mng_testimonials';
        $data['testimonials_mgmt'] = $this->this_model->getTestimonialInfoData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function addTestimonials() {

        $data['page'] = "superadmin/testimonial_add";
        $data['page_title'] = "Add Testimonials";
        $data['action'] = 'testimonials_mgmt';
        $data['module_name'] = "Testimonials";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Testimonials/mng_testimonials';
        $data['js'] = array('testimonials.js', 'common');
        $data['init'] = array('Testimonials.addTestimonial()');
        $data['formAction'] = base_url() . 'superadmin/Testimonials/addTestimonials';

        if ($this->input->post()) {
            $response = $this->this_model->addTestimonialData($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Testimonials/mng_testimonials');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editTestimonials($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/Testimonials/editTestimonials/' . $id;
        $data['page'] = "superadmin/testimonial_add";
        $data['page_title'] = "Edit Testimonials";
        $data['module_name'] = "Testimonials";
        $data['action'] = 'testimonials_mgmt';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Testimonials/mng_testimonials';
        $data['js'] = array('testimonials.js', 'common');
        $data['init'] = array('Testimonials.addTestimonial()');
        $data ['TestimonialData'] = $this->this_model->getTestimonialsEditDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editTestimonialInfoData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Testimonials/mng_testimonials');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/Testimonials/mng_testimonials');
        } else {
            $response = $this->this_model->deleteTestimonialDetail($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Testimonials/mng_testimonials');
        }
    }

    public function updateStatus() {
        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->updateStatus($this->input->post());
        }
    }

    public function handleMultiple() {

        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->handleMultiples($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            echo json_encode($response);
            exit;
        }
    }
    
  
}

?>