<?php

class About extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/About_model', 'this_model');
    }

    public function index() {
        $data['page'] = "superadmin/about_banner_list";
        $data['page_title'] = "Banner Management";
        $data['module_name'] = "About Page";
        $data['action'] = 'about_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/about';
        $data['banner_mgmt'] = $this->this_model->getBannerData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editAboutBanner($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/about/editAboutBanner/' . $id;
        $data['page'] = "superadmin/about_banner_edit";
        $data['page_title'] = "Edit Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'about_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/about';
        $data['js'] = array('about.js', 'common');
        $data['init'] = array('About.init()');
        $data ['AboutBannerData'] = $this->this_model->getAboutBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editAboutBannerData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/about');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function aboutText() {
        $data['page'] = "superadmin/about_text_list";
        $data['page_title'] = "Page Content";
        $data['module_name'] = "About Page";
        $data['action'] = 'about_text';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/about/aboutText';
        $data['AboutTextData'] = $this->this_model->getAboutTextData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editAboutText($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/about/editAboutText/' . $id;
        $data['page'] = "superadmin/about_text_edit";
        $data['page_title'] = "Edit Page Content";
        $data['module_name'] = "Page Content";
        $data['action'] = 'about_text';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/about/aboutText';
        $data['js'] = array('about.js', 'common');
        $data['init'] = array('About.text()');
        $data ['AboutTextData'] = $this->this_model->getAboutTextDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editAboutTextData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/about/aboutText');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function aboutWriter() {
        $data['page'] = "superadmin/about_writer_list";
        $data['page_title'] = "About Writer Details";
        $data['module_name'] = "About Writer Details";
        $data['action'] = 'about_writer';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['js'] = array('about.js', 'common');
        $data['init'] = array('About.main()');
        $data['formAction'] = base_url() . 'superadmin/about/aboutWriter';
        $data['AboutWriterData'] = $this->this_model->getAboutWriterData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function aboutAddWritter() {

        $data['page'] = "superadmin/about_writer_add";
        $data['page_title'] = "Add About Writer Details";
        $data['module_name'] = "About Writer Details";
        $data['action'] = 'about_writer';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/about/aboutWriter';
        $data['js'] = array('about.js', 'common');
        $data['init'] = array('About.addWriter()');
        $data['formAction'] = base_url() . 'superadmin/about/aboutAddWritter';

        if ($this->input->post()) {
            $response = $this->this_model->addWriterData($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/about/aboutWriter');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function aboutEditWritter($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/about/aboutEditWritter/' . $id;
        $data['page'] = "superadmin/about_writer_add";
        $data['page_title'] = "Edit About Writer Details";
        $data['module_name'] = "About Writer Details";
        $data['action'] = 'about_writer';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/about/aboutWriter';
        $data['js'] = array('about.js', 'common');
        $data['init'] = array('About.addWriter()');
        $data ['WriterData'] = $this->this_model->getAboutEditWriterDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editAboutWriterData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/about/aboutWriter');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }
    
    
    
    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);
        
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/about/aboutWriter');
        } else {
            $response = $this->this_model->deleteWriter($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/about/aboutWriter');
        }
    }

    public function updateStatus() {
        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->updateStatus($this->input->post());
        }
    }

    public function handleMultiple() {

        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->handleMultiples($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            echo json_encode($response);
            exit;
        }
    }
    

}

?>