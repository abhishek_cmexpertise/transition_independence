<?php

class Dashboard extends CI_Controller {

    function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }
        require_once APPPATH . "config/tablenames_constants.php";
    }

    function index() {
        $data['page'] = "superadmin/dashboard";
        $data['page_title'] = "dashboard";
        $data['action'] = 'dashboard';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url().'admin');
    }

}

?>