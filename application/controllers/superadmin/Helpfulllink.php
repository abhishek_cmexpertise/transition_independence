<?php

class Helpfulllink extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Helpfulllink_model', 'this_model');
    }

    public function index() {

        $data['page'] = "superadmin/helpfulllink_banner_list";
        $data['page_title'] = "Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'link_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Helpfulllink';
        $data['banner_mgmt'] = $this->this_model->getLinkData();


        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editBannerList($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/helpfulllink/editBannerList/' . $id;
        $data['page'] = "superadmin/helpfulllink_banner_edit";
        $data['page_title'] = "Edit Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'link_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Helpfulllink';
        $data['js'] = array('link.js', 'common');
        $data['init'] = array('Link.init()');
        $data ['LinkBannerData'] = $this->this_model->getEditBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editHelpfulllinkBannerData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Helpfulllink');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function helpful_link_list() {

        $data['page'] = "superadmin/helpfulllink_link_list";
        $data['page_title'] = "Helpful Links Details";
        $data['module_name'] = "Links Details";
        $data['action'] = 'helpful_link';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['js'] = array('link.js', 'common');
        $data['init'] = array('Link.list()');
        $data['formAction'] = base_url() . 'superadmin/Helpfulllink/helpful_link_list';
        $data['banner_mgmt'] = $this->this_model->getHelpfullLinkList();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function addHelpfulLink() {

        $data['page'] = "superadmin/helpfulllink_link_add";
        $data['page_title'] = "Add Helpful Links Details";
        $data['action'] = 'helpful_link';
        $data['module_name'] = "Helpful Links Details";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Helpfulllink/helpful_link_list';
        $data['js'] = array('link.js', 'common');
        $data['init'] = array('Link.add()');
        $data['formAction'] = base_url() . 'superadmin/Helpfulllink/addHelpfulLink';

        if ($this->input->post()) {
            $response = $this->this_model->addHelpfulLink($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Helpfulllink/helpful_link_list');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editHelpfulLink($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/helpfulllink/editHelpfulLink/' . $id;
        $data['page'] = "superadmin/helpfulllink_link_add";
        $data['page_title'] = "Edit Helpful Links Details";
        $data['module_name'] = "Helpful Links Details";
        $data['action'] = 'helpful_link';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Helpfulllink/helpful_link_list';
        $data['js'] = array('link.js', 'common');
        $data['init'] = array('Link.add()');
        $data ['LinkEditData'] = $this->this_model->getEditLinkDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editHelpfulLinks($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Helpfulllink/helpful_link_list');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }
    
    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/Helpfulllink/helpful_link_list');
        } else {
            $response = $this->this_model->deleteLinkDetail($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Helpfulllink/helpful_link_list');
        }
    }

    public function updateStatus() {
        
        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->updateStatus($this->input->post());
        }
        
    }

    public function handleMultiple() {

        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->handleMultiples($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            echo json_encode($response);
            exit;
        }
    }


}

?>