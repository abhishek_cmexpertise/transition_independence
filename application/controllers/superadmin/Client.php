<?php

class Client extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Client_model', 'this_model');
    }

    public function index() {

        $data['page'] = "superadmin/client_banner_list";
        $data['page_title'] = "Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'client_list';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/Client';
        $data['banner_mgmt'] = $this->this_model->getClientBannerData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editClient($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/Client/editClient/' . $id;
        $data['page'] = "superadmin/client_banner_edit";
        $data['page_title'] = "Edit Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'client_list';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Client';
        $data['js'] = array('client.js', 'common');
        $data['init'] = array('Client.init()');
        $data ['BannerData'] = $this->this_model->getClientBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editClientBannerDatas($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Client');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function mng_client() {

        $data['page'] = "superadmin/client_info_list";
        $data['page_title'] = "Client Details";
        $data['module_name'] = "Home Page";
        $data['action'] = 'client_mgmt';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['js'] = array('client.js', 'common');
        $data['init'] = array('Client.list()');
        $data['formAction'] = base_url() . 'superadmin/Client/mng_client';
        $data['client_mgmt'] = $this->this_model->getClientInfoData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function addClient() {

        $data['page'] = "superadmin/client_info_add";
        $data['page_title'] = "Add Client Details";
        $data['action'] = 'client_mgmt';
        $data['module_name'] = "Client Details";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Client/mng_client';
        $data['js'] = array('client.js', 'common');
        $data['init'] = array('Client.add()');
        $data['formAction'] = base_url() . 'superadmin/Client/addClient';

        if ($this->input->post()) {
            $response = $this->this_model->addClients($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Client/mng_client');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editClientData($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/Client/editClientData/' . $id;
        $data['page'] = "superadmin/client_info_add";
        $data['page_title'] = "Edit Client Details";
        $data['module_name'] = "Client Details";
        $data['action'] = 'client_mgmt';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/Client/mng_client';
        $data['js'] = array('client.js', 'common');
        $data['init'] = array('Client.add()');
        $data ['clientData'] = $this->this_model->getClientDatas($ids);


        if ($this->input->post()) {
            $response = $this->this_model->editClientDatas($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Client/mng_client');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    
    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/Client/mng_client');
        } else {
            $response = $this->this_model->deleteClientDetail($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/Client/mng_client');
        }
    }

    public function updateStatus() {
        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->updateStatus($this->input->post());
        }
    }

    public function handleMultiple() {

        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->handleMultiples($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            echo json_encode($response);
            exit;
        }
    }
    
}

?>