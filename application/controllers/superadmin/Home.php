<?php

class Home extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata['valid_login'])) {
            redirect(base_url() . 'superadmin/login');
        }

        require_once APPPATH . "config/tablenames_constants.php";
        $this->load->model('superadmin/Home_model', 'this_model');
    }

    public function index() {
        
        $data['page'] = "superadmin/banner_list";
        $data['page_title'] = "Banner Management";
        $data['action'] = 'home_banner';
        $data['module_name'] = "Home Page";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['js'] = array('home.js', 'common');
        $data['init'] = array('Home.front()');
        $data['formAction'] = base_url() . 'superadmin/home';
        $data['banner_mgmt'] = $this->this_model->getBannerData();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function addbanner() {
        
        $data['page'] = "superadmin/banner_add";
        $data['page_title'] = "Add Banner Management";
        $data['action'] = 'home_banner';
        $data['module_name'] = "Banner Management";
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/home';
        $data['js'] = array('home.js', 'common');
        $data['init'] = array('Home.init()');
        $data['formAction'] = base_url() . 'superadmin/home/addbanner';

        if ($this->input->post()) {
            $response = $this->this_model->addBannerData($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/home');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editBanner($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/home/editBanner/' . $id;
        $data['page'] = "superadmin/banner_add";
        $data['page_title'] = "Add Banner Management";
        $data['module_name'] = "Banner Management";
        $data['action'] = 'home_banner';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/home';
        $data['js'] = array('home.js', 'common');
        $data['init'] = array('Home.init()');

        $data ['BannerData'] = $this->this_model->getBannerDetail($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editBannerData($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/home');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(base_url() . 'superadmin/home');
        } else {
            $response = $this->this_model->deleteBanner($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/home');
        }
    }

    public function updateStatus() {
        
        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->updateStatus($this->input->post());
        }
        
    }

    public function handleMultiple() {

        if (($this->input->post()) && ($this->input->is_ajax_request())) {
            $response = $this->this_model->handleMultiples($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            echo json_encode($response);
            exit;
        }
    }

    // Manage Home Text

    public function mngHomeText() {
        
        $data['page'] = "superadmin/home_text_list";
        $data['page_title'] = "Page Content";
        $data['module_name'] = "Home Page";
        $data['action'] = 'home_text';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['formAction'] = base_url() . 'superadmin/home/mngHomeText';
        $data['home_text'] = $this->this_model->getHomeText();
        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

    public function editHomeText($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect($this->url);
        }

        $data['formAction'] = base_url() . 'superadmin/home/editHomeText/' . $id;
        $data['page'] = "superadmin/home_text_edit";
        $data['page_title'] = "Edit Page Content";
        $data['module_name'] = "Page Content";
        $data['action'] = 'home_text';
        $data['url'] = base_url() . 'superadmin/dashboard';
        $data['url1'] = base_url() . 'superadmin/home/mngHomeText';
        $data['js'] = array('home.js', 'common');
        $data['init'] = array('Home.editText()');
        $data ['home_text'] = $this->this_model->getHomeEditText($ids);

        if ($this->input->post()) {
            $response = $this->this_model->editHomeText($this->input->post(), $ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(base_url() . 'superadmin/home/mngHomeText');
        }

        $this->load->view(SUPERAMDIN_DASHBOARD_LAYOUT, $data);
    }

}

?>