<?php

require_once APPPATH . "config/tablenames_constants.php";

class Account extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Account_model', 'this_model');
    }

    function index() {

        $data['page'] = "front/page/home";
        $data['var_meta_title'] = 'home';
        $data['var_meta_description'] = 'home';
        $data['css'] = [
            'style',
            'font-awesome',
            'jquery.mCustomScrollbar',
            'responsive',
            'toastr.min'
        ];
        $data['js'] = [
            'jquery.mCustomScrollbar.concat.min',
            'jquery.bxslider',
            'jquery.colorbox',
            'scriptall',
            'jquery.validate.min',
            'additional-methods',
            'account',
            'toastr.min',
            'common_function'
        ];
        $data['init'] = ['Account.init()'];
        $data['banner'] = $this->this_model->banner();
        $data['row_art'] = $this->this_model->art();
        $data['art_gallery'] = $this->this_model->art_gallery(isset($data['row_art'][0]->artist_id));
        $data['show_cms1'] = $this->this_model->show_cms1();
        $data['show_testimonials'] = $this->this_model->show_testimonials();
        $data['show_patients'] = $this->this_model->show_patients();
        $data['show_cms2'] = $this->this_model->show_cms2();
        $data['latest_video'] = $this->this_model->latest_video();
        $data['show_cms3'] = $this->this_model->show_cms3();

        if ($this->input->post()) {
            $loginCheck = $this->this_model->loginCheck($this->input->post());
            echo json_encode($loginCheck);
            exit();
        }

        $this->load->view(FRONT_LAYOUT, $data);
    }

    public function contact_us() {

        if ($this->input->post()) {
            $response = $this->this_model->AddContactUs($this->input->post());
            echo json_encode($response);
            exit();
        }
    }

    function changepassword() {

        $data['page'] = "account/changepassword";
        $data['var_meta_title'] = "Change Password";
        $data['var_meta_description'] = "Change Password";
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
        ];
        $data ['js'] = array(
            'jquery.min',
            'popper',
            'bootstrap.min',
            'jquery.validate',
            'additional-methods.min',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'account/changepass'
        );
        $data['init'] = array(
            'Changepass.changepass()',
        );

        if ($this->input->post()) {

            $change = $this->this_model->changepassword($this->input->post());

            if ($change) {
                $this->session->sess_destroy();
                redirect('account/index');
            }
        }

        $usertype = $this->session->userdata['valid_login']['Type'];
        if ($usertype == 'Super Admin') {
            $this->load->view(ADMIN_LAYOUT, $data);
        } else if ($usertype == 'Supervisor') {
            $this->load->view(SUPERVISOR_LAYOUT, $data);
        } else if ($usertype == 'Employee') {
            $this->load->view(EMPLOYEE_LAYOUT, $data);
        }
    }

    public function verifyOldPass() {
        echo json_encode($this->this_model->verifyOldPass());
        exit();
    }

    function signout() {
        $this->session->sess_destroy();
        $this->index();
    }

}

?>