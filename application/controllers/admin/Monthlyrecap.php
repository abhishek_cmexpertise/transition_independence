<?php
    
class Monthlyrecap extends Admin_Controller {

    public function __construct() {
        parent::__construct();
     
           $this->load->model('admin/Monthyrecape_model', 'this_model');   
    }

    function index() {
        $data['page'] = 'admin/monthlyrecap/monthlyrecap';
        $data['var_meta_title'] = 'Monthly recap';
        $data['var_meta_description'] = 'Monthly recap';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'common',
        ];
        $this->load->view(ADMIN_LAYOUT, $data);
    }
    
    public function genarateMonthlyreport()
    {   $this->load->library('html2pdf');
        $this->html2pdf->folder('public/assets/pdf/');
        $fileName = time() . '.pdf';
        $this->html2pdf->filename($fileName);
        $this->html2pdf->paper('a4', 'Landscape');
        $data['month'] = $this->input->post('month');
        $data['year'] = $this->input->post('year');
        $data['client'] = $this->this_model->genarateMonthlyreport($this->input->post());
        $string = $this->load->view('admin/monthlyrecap/monthlyrecap_pdf', $data, true);
        $response = $this->html2pdf->html($string);
        $this->html2pdf->create('save');
        $file_for_user = $fileName . '.pdf';
        $full_path_file = "public/assets/pdf/" . $fileName;
        header("Content-type: application/pdf");
        header('Content-Disposition: attachment; filename="' . $file_for_user . '"');
        readfile($full_path_file);
    }
    
}
?>