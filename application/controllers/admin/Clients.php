<?php

class Clients extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('admin/Clients_model', 'this_model');
    }

    function index() {
        $data['page'] = 'admin/clients/clients';
        $data['var_meta_title'] = 'Clients';
        $data['var_meta_description'] = 'Clients';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'datatables.min',
            'datatables.bootstrap',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'clients',
            'common',
        ];
        $data['init'] = ['Clients.init()'];
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function manageClients() {
        $this->load->library('Datatables');
        $result = $this->Datatable_model->getClientsData();
        echo json_encode($result);
        exit();
    }

    public function addClients() {
        $data['page'] = 'admin/Clients/addClient';
        $data['var_meta_title'] = 'Add Client';
        $data['var_meta_description'] = 'Add Client';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
            'bootstrap-datepicker',
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'clients',
            'common',
            'bootstrap-datepicker'
        ];
        $data['init'] = ['Clients.handleaddClients()'];
        if ($this->input->post()) {
            $response = $this->this_model->addClients($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(admin_url() . 'clients');
        }
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function editClients($id) {
        
        $ids = $this->utility->newdeCode($id);
       
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'clients');
        }

        $data['page'] = 'admin/clients/editclients';
        $data['formAction'] = 'admin/clients/editclients/' . $id;
        $data['var_meta_title'] = 'Edit Client';
        $data['var_meta_description'] = 'Edit Client';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
            'bootstrap-datepicker',
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'clients',
            'common',
            'bootstrap-datepicker',
        ];
        $data['init'] = ['Clients.handleaddClients()'];

        $data['singleClient'] = $this->this_model->editClients($ids);

        if ($this->input->post()) {

            $response = $this->this_model->updateClients($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(admin_url() . 'clients');
        }

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'clients');
        } else {
            $response = $this->this_model->deleteClients($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(admin_url() . 'clients');
        }
    }

}

?>