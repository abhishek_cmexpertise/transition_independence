<?php

class Supervisors extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('admin/Supervisors_model', 'this_model');
    }

    function index() {
        $data['page'] = 'admin/supervisors/supervisors';
        $data['var_meta_title'] = 'Supervisors';
        $data['var_meta_description'] = 'Supervisors';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'plugins.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'supervisors',
            'common',
        ];
        $data['init'] = ['Supervisors.init()'];
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function manageSupervisors() {
        $this->load->library('Datatables');
        $result = $this->Datatable_model->getSupervisorsData();
        echo json_encode($result);
        exit();
    }

    public function addSupervisor() {
        $data['page'] = 'admin/supervisors/addSupervisor';
        $data['var_meta_title'] = 'Add Supervisors';
        $data['var_meta_description'] = 'Supervisors';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
            'bootstrap-datepicker',
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'supervisors',
            'common',
            'bootstrap-datepicker'
        ];
        $data['init'] = ['Supervisors.handleaddSupervisors()'];
        if ($this->input->post()) {

            $response = $this->this_model->addSupervisor($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(admin_url() . 'supervisors');
        }
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function editSupervisor($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'supervisors');
        }

        $data['page'] = 'admin/supervisors/editsupervisors';
        $data['formAction'] = 'admin/supervisors/editSupervisor/' . $id;
        $data['var_meta_title'] = 'Edit Supervisors';
        $data['var_meta_description'] = ' Edit Supervisors';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
            'bootstrap-datepicker',
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'supervisors',
            'common',
            'bootstrap-datepicker',
        ];
        $data['init'] = ['Supervisors.handleeditSupervisors()'];

        $data['singleSupervisor'] = $this->this_model->EditSupervisor($ids);

        if ($this->input->post()) {
            $response = $this->this_model->updateSupervisor($this->input->post());
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(admin_url() . 'supervisors');
        }

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'supervisors');
        } else {
            $response = $this->this_model->deleteSupervisors($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(admin_url() . 'supervisors');
        }
    }

    public function verifyemail() {

        echo json_encode($this->this_model->verifyemail());
        exit();
    }

}

?>