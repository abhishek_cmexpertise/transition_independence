<?php

class Trainingtimesheet extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        //$this->load->model('admin/Shiftlog_model', 'this_model');
    }

    function index() {
        $data['page'] = 'admin/trainingtimesheet/trainingtimesheet';
        $data['var_meta_title'] = 'training time sheet';
        $data['var_meta_description'] = 'training time sheet';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
         $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'supervisors',
            'common',
        ];
        $data['init'] = ['Supervisors.init()'];
        $this->load->view(ADMIN_LAYOUT, $data);
    }
}
?>