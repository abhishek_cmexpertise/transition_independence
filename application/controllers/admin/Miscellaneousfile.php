<?php

class Miscellaneousfile extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('admin/Miscellaneousfile_model', 'this_model');
    }

    public function index() {
        $data['page'] = 'admin/miscellaneousfile/miscellaneousfile';
        $data['var_meta_title'] = 'Miscellaneous File';
        $data['var_meta_description'] = 'Miscellaneous File';
        $data['css'] = ['bootstrap.min', 'themify-icons', 'font-awesome', 'datatables.min', 'datatables.bootstrap', 'icofont', 'style', 'jquery.mCustomScrollbar', 'plugins.min', 'components'];
        $data['js'] = ['popper', 'jquery.min', 'bootstrap.min', 'jquery.validate', 'jquery-ui.min', 'jquery.slimscroll', 'script', 'pcoded.min', 'datatable', 'datatables.min', 'datatables.bootstrap', 'jquery.blockui.min', 'app.min', 'additional-methods.min', 'vartical-demo', 'jquery.mCustomScrollbar.concat.min', 'miscellaneousfile', 'common',];
        $data['init'] = ['Miscellaneousfile.init()'];
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function manageMiscellaneousfile() {
        $this->load->library('Datatables');
        $result = $this->Datatable_model->getMiscellaneousfileData();
        echo json_encode($result);
        exit();
    }

    public function addMiscellaneousfile() {
        
        $data['page'] = 'admin/miscellaneousfile/addMiscellaneousfile';
        $data['var_meta_title'] = 'Miscellaneous File';
        $data['var_meta_description'] = 'Miscellaneous File';
        $data['css'] = ['bootstrap.min', 'themify-icons', 'font-awesome', 'icofont', 'style', 'jquery.mCustomScrollbar', 'plugins.min', 'components'];
        $data['js'] = ['popper', 'jquery.min', 'bootstrap.min', 'jquery.validate', 'jquery-ui.min', 'jquery.slimscroll', 'script', 'pcoded.min', 'jquery.blockui.min', 'app.min', 'additional-methods.min', 'vartical-demo', 'jquery.mCustomScrollbar.concat.min', 'miscellaneousfile', 'common',];
        $data['init'] = ['Miscellaneousfile.handleaddMiscellaneousfile()'];

        if ($_FILES) {
            $response = $this->this_model->addMiscellaneousfile();
            $this->utility->setFlashMessage($response[0], $response[1]);
            redirect(admin_url() . 'miscellaneousfile');
        }
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function delete($id) {
        unset($ids);
        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'miscellaneousfile');
        } else {
            $response = $this->this_model->deleteMiscellaneousfile($ids);
            $this->utility->setFlashMessage($response[0], $response[1]);
            redirect(admin_url() . 'miscellaneousfile');
        }
    }

}

?>
