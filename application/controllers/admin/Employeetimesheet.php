<?php

class Employeetimesheet extends Admin_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('admin/Emptimesheet_model', 'this_model');
    }

    function index() {
        $data['page'] = 'admin/employeetimesheet/employeetimesheet';
        $data['var_meta_title'] = ' Employee timesheet';
        $data['var_meta_description'] = 'Employee timesheet';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'employeereport',
            'common',
        ];
        $data['init'] = ['Employeereport.init()'];
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    public function manageEmployeetimesheet() {

        $this->load->library('Datatables');
        $result = $this->Datatable_model->getEmployeetimesheet();
        echo json_encode($result);
        exit();
    }

    public function employeetimesheetview($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'Clients');
        }

        $data['id'] = $id;

        $data['page'] = 'admin/employeetimesheet/employeetimesheetview';
        $data['var_meta_title'] = 'Employee Timesheet';
        $data['var_meta_description'] = 'Employee Timesheet';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'employeereport',
            'common',
        ];
        $data['init'] = ['Employeereport.add()'];

        $data['emptimesheetview'] = $this->this_model->employeetimesheetview($ids);
       
        $this->load->view(ADMIN_LAYOUT, $data);
    }
    
     public function employeereportpdf($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'Employeetimesheet');
        }
        $this->load->library('html2pdf');
        $this->html2pdf->folder('public/assets/pdf/');
        $fileName = time() . '.pdf';
        $this->html2pdf->filename($fileName);
        $this->html2pdf->paper('a4', 'Landscape');
        $data['shiftlogreport'] = $this->this_model->shiftlogdata($ids);
        $string = $this->load->view('admin/employeetimesheet/emptimesheetpdf', $data, true);
       $response = $this->html2pdf->html($string);
        $this->html2pdf->create('save');
        $file_for_user = $fileName . '.pdf';
        $full_path_file = "public/assets/pdf/" . $fileName;
        header("Content-type: application/pdf");
        header('Content-Disposition: attachment; filename="' . $file_for_user . '"');
        readfile($full_path_file);
    }

     public function employeereportexcel($id) {
        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'employeerimesheet');
        }
        $contents = "SL NO,DATE,CLIENT NAME,UCI,DATE OF BIRTH,EMPLOYEE NAME,TOTAL HOURS,TOTAL TRAVEL TIMES\n";
        $data = $this->this_model->getLog($ids);
        
        
         $emptimesheetview = $this->this_model->employeetimesheetview($ids);
       
        $user_fullname = $data[0]->first_name . $data[0]->middle_name . $data[0]->last_name;
        $i = 1;
        foreach ($data as $row_data) {

            $consumer_name = $data[0]->consumer_name;
            $client_uci = $data[0]->client_uci;
            $client_dob = $data[0]->client_dob;

            $contents .= $i . ",";
            $contents .= $row_data->shiftlog_date . ",";
            $contents .= $consumer_name . ",";
            $contents .= $client_uci . ",";
            $contents .= $client_dob . ",";
            $contents .= $user_fullname . ",";
            $contents .= $row_data->total_hours . ",";
            $contents .= $row_data->total_travel_time . ",";
            $contents .= "\n";

            $i++;
        }
        $contents .= " , , , , , , , \n";
        $contents .= " , , , , , , , \n";
        $contents .= " , , , , , , SUB-TOTAL HOURS,SUB-TOTAL TRAVEL TIME \n";
        $contents .= " , , , , , , " . $emptimesheetview[0] . ",";
        $contents .= $emptimesheetview[1] . ",";
        $contents .= "\n";
        $contents = strip_tags($contents); // remove html and php tags etc.

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=' . str_replace(' ', '_', $user_fullname) . '_Monthly_Report(' . date('m-d-Y') . ').csv');
        header('Pragma: no-cache');

        print $contents;
    }

    public function manageLog($id) {
        $ids = $this->utility->newdeCode($id);
        $this->load->library('Datatables');
        $result = $this->Datatable_model->getLog($ids);
        echo json_encode($result);
        exit();
    }

}

?>