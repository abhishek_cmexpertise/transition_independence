<?php

class Miscellaneousfile extends Employee_Controller {

    public function __construct() {
        parent::__construct();

      //  $this->load->model('employee/Miscellaneousfile_model', 'this_model');
    }

    function index() {
        $data['page'] = 'employee/miscellaneousfile/miscellaneousfile';
        $data['var_meta_title'] = 'Miscellaneous File';
        $data['var_meta_description'] = 'Miscellaneous File';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'miscellaneousfile',
            'common',
        ];
        $data['init'] = ['Miscellaneousfile.init()'];
        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

    public function manageMiscellaneousfile() {
        $this->load->library('Datatables');
        $result = $this->Datatable_model->EmpgetMiscellaneousfileData();
        echo json_encode($result);
        exit();
    }

}

?>