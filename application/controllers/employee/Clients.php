<?php

class Clients extends Employee_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('employee/Clients_model', 'this_model');
    }

    function index() {
        $data['page'] = 'employee/clients/clients';
        $data['var_meta_title'] = 'Clients';
        $data['var_meta_description'] = 'Clients';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'clients',
            'common',
        ];
        $data['init'] = ['Clients.init()'];
        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

    public function manageClients() {
        $this->load->library('Datatables');
        $result = $this->Datatable_model->getEmpClientsData();
        echo json_encode($result);
        exit();
    }

    public function clientView($id) {

        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(employee_url() . 'Clients');
        }

        $data['id'] = $id;
        $data['page'] = 'employee/clients/clientview';
        $data['var_meta_title'] = 'Clientview';
        $data['var_meta_description'] = 'Clientview';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'clients',
            'common',
        ];
        $data['singleClient'] = $this->this_model->getClientview($ids);
        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

    public function clientshiftlog($id) {

        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(employee_url() . 'Clients');
        }

        $data['id'] = $id;
        $data['page'] = 'employee/clients/shiftlog';
        $data['var_meta_title'] = 'Client shiftlog';
        $data['var_meta_description'] = 'Clients shiftlog';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'clients',
            'common',
        ];
        $data['init'] = ['Clients.shift()'];
        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

    public function manageClientslog($id) {
        $ids = $this->utility->newdeCode($id);
        $this->load->library('Datatables');
        $result = $this->Datatable_model->getClientlog($ids);
        echo json_encode($result);
        exit();
    }

    public function Showshiftlog($id) {
        $ids = $this->utility->newdeCode($id);
 
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(employee_url() . 'Clients');
        }
        $data['page'] = 'employee/clients/shiftlogview';
        $data['var_meta_title'] = 'Shiftlog view';
        $data['var_meta_description'] = 'Shiftlog view';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
        ];
        $data['shiftlogview'] = $this->this_model->shiftlogview($ids);
        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

    public function shiftlog_form($id) {
        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(employee_url() . 'Clients');
        }

        $data['id'] = $id;
        $data['page'] = 'employee/clients/Addclientlog';
        $data['var_meta_title'] = 'Client Report';
        $data['var_meta_description'] = 'Client Report';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
            'bootstrap-datepicker',
            'bootstrap-timepicker.min',
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'clients',
            'bootstrap-datepicker',
            'bootstrap-timepicker.min',
        ];
        $data['init'] = ['Clients.handleaddClientlog()'];
        $data['clientdata'] = $this->this_model->getclientshiftlog($ids);
        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

    public function addShiftlog() {
        if ($this->input->post()) {
            $ids = $this->utility->newenCode($this->input->post('client_id'));
             $response = $this->this_model->addShiftlog($this->input->post());
             $this->utility->setFlashMessage($response [0], $response [1]);
             redirect(employee_url() . 'Clients/clientshiftlog/'.$ids);
           }
    }
}
?>