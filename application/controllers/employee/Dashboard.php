<?php

class Dashboard extends Employee_Controller {

    function __construct() {
        parent::__construct();
         $this->load->model('employee/Dashboard_model', 'this_model');
    }

    function index() {
        $data['page'] = 'employee/dashboard/dashboard';
        $data['var_meta_title'] = 'Employee Dashboard';
        $data['var_meta_description'] = 'Employee Dashboard';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
             'common',
        ];
        $data['semianualreport']= $this->this_model->getsemianualreport();
        $data['annualispreport']= $this->this_model->getAnnualreport();
        
        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

}

?>