<?php

class Clientreport extends Employee_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('employee/Clientreport_model', 'this_model');
    }

    function index() {
        $data['page'] = 'employee/clientreport/clientreport';
        $data['var_meta_title'] = 'Client Report';
        $data['var_meta_description'] = 'Client Report';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
               'toastr.min'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
               'toastr.min',
            'clientreport',
            'common',
        ];
        $data['init'] = ['Clientreport.init()'];
        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

    public function addClientreport() {
        $data['page'] = 'employee/clientreport/addclientreport';
        $data['var_meta_title'] = 'Add Client report';
        $data['var_meta_description'] = 'Add Client report';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'clientreport',
            'common',
        ];
        $data['clientdata'] = $this->this_model->getClientdata();
        $data['init'] = ['Clientreport.init()'];
        $data['init'] = ['Clientreport.handleaddClientreport()'];

        if ($this->input->post()) {
            $this->this_model->addClientreport($this->input->post());
            redirect(employee_url() . 'Clientreport');
        }

        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }

    public function addsingleclientreport($id) {

        $ids = $this->utility->newdeCode($id);

        $data['page'] = 'employee/clientreport/addsingleclientreport';
        $data['var_meta_title'] = 'Add Client report';
        $data['var_meta_description'] = 'Add Client report';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'common',
        ];

        $data['clientreport'] = $this->this_model->getclientreport($ids);
        if ($this->input->post()) {
            $this->this_model->addClientreport($this->input->post());
            redirect(employee_url() . 'Clientreport');
        }

        $this->load->view(EMPLOYEE_LAYOUT, $data);
    }
    
    public function manageClientreport() {
        $this->load->library('Datatables');
        $result = $this->Datatable_model->clientmonthreport();
        echo json_encode($result);
        exit();
    }

    public function getclientdetail() {
        if ($this->input->post()) {
            $clientdetail = $this->this_model->clientdetail($this->input->post());
        }
        echo json_encode($clientdetail);
        exit;
    }

    public function clientreportpdf($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'Supervisors');
        }

        $this->load->library('html2pdf');
        $this->html2pdf->folder('public/assets/pdf/');
        $fileName = time() . '.pdf';
        $this->html2pdf->filename($fileName);
        $this->html2pdf->paper('a4', 'portrait');
        $data['clientreport'] = $this->this_model->clientreportdata($ids);
//   echo "<pre>";
//   print_r($data);
//   exit();
        $string = $this->load->view('admin/clientreport/pdf_download_clientreport', $data, true);
        $response = $this->html2pdf->html($string);
        $this->html2pdf->create('save');
        $file_for_user = $fileName . '.pdf';
        $full_path_file = "public/assets/pdf/" . $fileName;
        header("Content-type: application/pdf");
        header('Content-Disposition: attachment; filename="' . $file_for_user . '"');
        readfile($full_path_file);
    }

    public function clientreportexcel($id) {
        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(employee_url() . 'Supervisors');
        }
        $contents = "ID,REPORT DATE,CONSUMER NAME,UCI,DATE OF BIRTH,EMPLOYEE NAME,COMMENT FOR OBJECTIVE 1,SERVICES FOR OBJECTIVE 1,COMMENT FOR OBJECTIVE 2,SERVICES FOR OBJECTIVE 2,COMMENT FOR OBJECTIVE 3,SERVICES FOR OBJECTIVE 3,COMMENT FOR OBJECTIVE 4,SERVICES FOR OBJECTIVE 4,COMMENT FOR OBJECTIVE 5,SERVICES FOR OBJECTIVE 5\n";
        $data = $this->this_model->clientreportdata($ids);

        $i = 1;
        foreach ($data as $datas) {
            $user_fullname = $data[0]->first_name . $data[0]->middle_name . $data[0]->last_name;
            $consumer_name = $data[0]->consumer_name;
            $client_uci = $data[0]->client_uci;
            $client_dob = $data[0]->client_dob;
            $contents .= $i . ",";
            $contents .= $datas->report_date . ",";
            $contents .= $consumer_name . ",";
            $contents .= $client_uci . ",";
            $contents .= $client_dob . ",";
            $contents .= $user_fullname . ",";
            $contents .= $datas->comment1 . ",";
            $contents .= $datas->objective1 . ",";
            $contents .= $datas->comment2 . ",";
            $contents .= $datas->objective2 . ",";
            $contents .= $datas->comment3 . ",";
            $contents .= $datas->objective3 . ",";
            $contents .= $datas->comment4 . ",";
            $contents .= $datas->objective4 . ",";
            $contents .= $datas->comment5 . ",";
            $contents .= $datas->objective5 . ",";
            $contents .= "\n";
            $i++;
        }
        $contents = strip_tags($contents); // remove html and php tags etc.

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=Clients_Monthly_Report(' . date('m-d-Y') . ').csv');
        header('Pragma: no-cache');


        print $contents;
    }
    
      public function clientreportexcels() {

        $id = $this->input->post();
        $array = array("ID", "REPORT DATE", "CONSUMER NAME", "UCI", "DATE OF BIRTH", "EMPLOYEE NAME", "COMMENT FOR OBJECTIVE 1", "SERVICES FOR OBJECTIVE 1", "COMMENT FOR OBJECTIVE 2", "SERVICES FOR OBJECTIVE 2", "COMMENT FOR OBJECTIVE 3", "SERVICES FOR OBJECTIVE 3", "COMMENT FOR OBJECTIVE 4", "SERVICES FOR OBJECTIVE 4", "COMMENT FOR OBJECTIVE 5", "SERVICES FOR OBJECTIVE 5");
        $data = $this->this_model->clientreportdatas($id);
   
        foreach ($data as $datas) {

            $arrays = array(
                'ID' => $datas['report_id'],
                'REPORT DATE' => $datas['report_date'],
                "CONSUMER NAME" => $datas['consumer_name'],
                "UCI" => $datas['client_uci'],
                "DATE OF BIRTH" => $datas['client_dob'],
                "EMPLOYEE NAME" => $datas['first_name'],
                "COMMENT FOR OBJECTIVE 1" => $datas['comment1'],
                "SERVICES FOR OBJECTIVE 1" => $datas['objective1'],
                "COMMENT FOR OBJECTIVE 2" => $datas['comment2'],
                "SERVICES FOR OBJECTIVE 2" => $datas['objective2'],
                "COMMENT FOR OBJECTIVE 3" => $datas['comment3'],
                "SERVICES FOR OBJECTIVE 3" => $datas['objective3'],
                "COMMENT FOR OBJECTIVE 4" => $datas['comment4'],
                "SERVICES FOR OBJECTIVE 4" => $datas['objective4'],
                "COMMENT FOR OBJECTIVE 5 " => $datas['comment5'],
                "SERVICES FOR OBJECTIVE 5 " => $datas['objective5']
            );
                
            $datareport[] = $arrays; 
        
        }
            
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=Clients_Monthly_Report(' . date('m-d-Y') . ').csv');
        header('Pragma: no-cache');
        
    
        $file = fopen('public/Clients_Monthly_Report(' . date('m-d-Y') . ').csv', 'w');

        fputcsv($file, $array);
        foreach ($datareport as  $line) {
            fputcsv($file,$line);
        }
        fclose($file);
        exit;
    }

    

}

?>