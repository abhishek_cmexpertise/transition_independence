<?php

class Employees extends Supervisor_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('supervisor/Employees_model', 'this_model');
    }

    function index() {
        $data['page'] = 'supervisor/employees/employees';
        $data['var_meta_title'] = 'Employees';
        $data['var_meta_description'] = 'Employees';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'employees',
            'common',
        ];
        $data['init'] = ['Employees.init()'];
        $this->load->view(SUPERVISOR_LAYOUT, $data);
    }

    public function manageEmployees() {
        $this->load->library('Datatables');
        $result = $this->Datatable_model->getEmployeesData();
        echo json_encode($result);
        exit();
    }
    
     public function addEmployees()
    {   
        $data['page'] = 'supervisor/employees/addEmployee';
        $data['var_meta_title'] = 'Add Employee';
        $data['var_meta_description'] = 'Add Employees';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
            'bootstrap-datepicker',
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'employees',
            'common',
            'bootstrap-datepicker'
        ];
        $data['init'] = ['Employees.handleaddEmployees()'];
         if($this->input->post())
        {   
         $response =  $this->this_model->addEmployees($this->input->post());   
          $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(supervisor_url().'employees');
        }
        $this->load->view(SUPERVISOR_LAYOUT, $data);
        
    }

    public function editEmployees($id)
    {
        $ids = $this->utility->newdeCode($id);
        
         if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(supervisor_url().'employees');
        }
        
        $data['page'] = 'supervisor/Employees/editemployees';
        $data['formAction'] = 'supervisor/Employees/editEmployees/' . $id;
        $data['var_meta_title'] = 'Edit Employee';
        $data['var_meta_description'] = 'Edit Employee';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
            'bootstrap-datepicker',
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'employees',
            'common',
            'bootstrap-datepicker',
        ];
        $data['init'] = ['Employees.handleeditEmployees()'];
        
        $data['singleEmployee'] = $this->this_model->editEmployees($ids);
    
           if($this->input->post())
            {          
            $response = $this->this_model->updateEmployees($this->input->post());      
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(supervisor_url().'employees');
            }
     
        $this->load->view(SUPERVISOR_LAYOUT, $data);
        
    } 
    

        public function delete($id) {

        unset($ids);
        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(supervisor_url().'employees');
        } else {
            $response = $this->this_model->deleteEmployees($ids);
            $this->utility->setFlashMessage($response [0], $response [1]);
            redirect(supervisor_url().'employees');
        }
    }
    
    public function verifyemail() {
      
        echo json_encode($this->this_model->verifyemail());
        exit();
    }

}

?>