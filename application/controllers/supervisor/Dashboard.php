<?php

class Dashboard extends Supervisor_Controller {

    function __construct() {
        parent::__construct();
          $this->load->model('supervisor/dashboard_model', 'this_model');
    }

    function index() {
        $data['page'] = 'supervisor/dashboard/dashboard';
        $data['var_meta_title'] = 'Supervisor Dashboard';
        $data['var_meta_description'] = 'Supervisor Dashboard';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
            
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
             'common',
        ];
        $data['semianualreport']= $this->this_model->getsemianualreport();
        $data['annualispreport']= $this->this_model->getAnnualreport();
         $data['client_birthday'] = $this->this_model->gettodaybirthday();
        $data['client_weekbirthday'] = $this->this_model->getweekbirthday();
        $data['client_monthbirthday'] = $this->this_model->getmonthbirthday();
        $this->load->view(SUPERVISOR_LAYOUT, $data);
    }

}

?>