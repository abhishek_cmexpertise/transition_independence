<?php

class Shiftlog extends Supervisor_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('supervisor/Shiftlog_model', 'this_model');
    }

    function index() {
        $data['page'] = 'supervisor/shiftlog/shiftlog';
        $data['var_meta_title'] = 'Shift Log';
        $data['var_meta_description'] = 'Shift Log';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'datatables.min',
            'datatables.bootstrap',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components',
            'bootstrap-datepicker',
              'toastr.min'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'datatables.min',
            'datatables.bootstrap',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
              'toastr.min',
            'shiftlog',
            'common',
             'bootstrap-datepicker'
        ];
        $data['init'] = ['Shiftlog.init()'];
         $data['filterEmployeedata'] = $this->this_model->getEmployeedata();
         $data['filterClientdata'] = $this->this_model->getclient();
        $this->load->view(SUPERVISOR_LAYOUT, $data);
    }
    
       public function shiftLogview($id) {
        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(supervisor_url() . 'Supervisors');
        }
        $data['page'] = 'supervisor/shiftlog/shiftLogview';
        $data['var_meta_title'] = 'Shiftlog view';
        $data['var_meta_description'] = 'Shiftlog view';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
        $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'datatable',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'common',
        ];
        $data['shiftlogview'] = $this->this_model->shiftlogview($ids);
        $this->load->view(SUPERVISOR_LAYOUT, $data);
    }

    public function shiftlogpdfview($id) {

        $ids = $this->utility->newdeCode($id);

        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(supervisor_url() . 'Supervisors');
        }

        $this->load->library('html2pdf');
        $this->html2pdf->folder('public/assets/pdf/');
        $fileName = time() . '.pdf';
        $this->html2pdf->filename($fileName);
        $this->html2pdf->paper('a4', 'portrait');
        $data['shiftlogview'] = $this->this_model->shiftlogview($ids);
        $string = $this->load->view('supervisor/shiftlog/shiftlogpdf', $data, true);
        $response = $this->html2pdf->html($string);
        $this->html2pdf->create('save');
        $file_for_user = $fileName . '.pdf';
        $full_path_file = "public/assets/pdf/" . $fileName;
        header("Content-type: application/pdf");
        header('Content-Disposition: attachment; filename="' . $file_for_user . '"');
        readfile($full_path_file);
    }

   public function shiftlogexcelview($id) {
        $ids = $this->utility->newdeCode($id);
        if (!ctype_digit($ids)) {
            $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
            redirect(admin_url() . 'supervisors');
        }
        $contents = "SL NO.,DATE,CONSUMER NAME,STAFF NAME,START TIME,END TIME,TOTAL TIME,CONSUMER DETAIL,TRAVEL TIME\n";
        $data['shiftlogview'] = $this->this_model->shiftlogview($ids);

        $user_fullname = $data['shiftlogview'][0]->first_name . ' ' . $data['shiftlogview'][0]->middle_name . ' ' . $data['shiftlogview'][0]->last_name;
        $i = 1;
        foreach ($data as $datas) {
            $consumer_name = $data['shiftlogview'][0]->consumer_name;
            $contents .= $i . ",";
            $contents .= $datas[0]->shiftlog_date . ",";
            $contents .= $consumer_name . ",";
            $contents .= $user_fullname . ",";
            $contents .= $datas[0]->start_time . ",";
            $contents .= $datas[0]->end_time . ",";
            $contents .= $datas[0]->total_time . ",";
            $contents .= $datas[0]->consumer_details . ",";
            $contents .= $datas[0]->travel_time . ",";
            $contents .= "\n";
            $contents .= "\n";
            $i++;
        }
        $contents .= " , , , , , , , \n";
        $contents .= " , , , , , , , \n";
        $contents .= " , , , , , , SUB-TOTAL HOURS,SUB-TOTAL TRAVEL TIMES \n";
        $contents .= " , , , , , , " . $data['shiftlogview'][0]->total_hours . ",";
        $contents .= $data['shiftlogview'][0]->total_travel_time . ",";
        $contents .= "\n";
        $contents = strip_tags($contents); // remove html and php tags etc.
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=' . str_replace(' ', '_', $user_fullname) . '_Monthly_Report(' . date('m-d-Y') . ').csv');
        header('Pragma: no-cache');
        print $contents;
    }
    
     public function shiftLogallexcel() {
      
        $contents = "SL NO.,DATE,CONSUMER NAME,STAFF NAME,START TIME,END TIME,TOTAL TIME,CONSUMER DETAIL,TRAVEL TIME\n";
        $data = $this->this_model->shiftlogalldatas();
        $user_fullname = $data[0]->first_name . ' ' . $data[0]->middle_name . ' ' . $data[0]->last_name;
        $i = 1;
        foreach ($data as $datas) {
            $consumer_name = $data[0]->consumer_name;

            $contents .= $i . ",";
            $contents .= $datas->shiftlog_date . ",";
            $contents .= $consumer_name . ",";
            $contents .= $user_fullname . ",";
            $contents .= $datas->start_time . ",";
            $contents .= $datas->end_time . ",";
            $contents .= $datas->total_time . ",";
            $contents .= $datas->consumer_details . ",";

            $contents .= $datas->travel_time . ",";
            $contents .= "\n";

            $contents .= "\n";

            $i++;
        }
        $contents .= " , , , , , , , \n";
        $contents .= " , , , , , , , \n";
        $contents .= " , , , , , , SUB-TOTAL HOURS,SUB-TOTAL TRAVEL TIMES \n";
        $contents .= " , , , , , , " . $data[0]->total_hours . ",";
        $contents .= $data[0]->total_travel_time . ",";
        $contents .= "\n";
        $contents = strip_tags($contents); // remove html and php tags etc.

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=' . str_replace(' ', '_', $user_fullname) . '_Monthly_Report(' . date('m-d-Y') . ').csv');
        header('Pragma: no-cache');

        print $contents;
    }

     public function manageShiftlog($employee = NULL, $client = NULL, $startDate = NULL, $endDate = NULL) {
            
         $this->load->library('Datatables');
  $result = $this->Datatable_model->getShiftlogData($employee, $client, $startDate, $endDate);
        echo json_encode($result);
        exit();
    }
 }
?>