<?php

class Trainingtimesheet extends Supervisor_Controller {

    public function __construct() {
        parent::__construct();

        //$this->load->model('admin/Shiftlog_model', 'this_model');
    }

    function index() {
        $data['page'] = 'supervisor/trainingtimesheet/trainingtimesheet';
        $data['var_meta_title'] = 'training time sheet';
        $data['var_meta_description'] = 'training time sheet';
        $data['css'] = [
            'bootstrap.min',
            'themify-icons',
            'font-awesome',
            'icofont',
            'style',
            'jquery.mCustomScrollbar',
            'plugins.min',
            'components'
        ];
         $data['js'] = [
            'popper',
            'jquery.min',
            'bootstrap.min',
            'jquery.validate',
            'jquery-ui.min',
            'jquery.slimscroll',
            'script',
            'pcoded.min',
            'jquery.blockui.min',
            'app.min',
            'additional-methods.min',
            'vartical-demo',
            'jquery.mCustomScrollbar.concat.min',
            'common',
        ];
        $data['init'] = ['Supervisors.init()'];
        $this->load->view(SUPERVISOR_LAYOUT, $data);
    }
}
?>