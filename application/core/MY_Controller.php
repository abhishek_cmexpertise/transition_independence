<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        require_once APPPATH . "config/tablenames_constants.php";
    }

}

class Admin_Controller extends MY_Controller {

    public $user_data = null;
    public $user_type = null;
    public $user_id = null;

    function __construct() {
        parent::__construct();

        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {

            if (!isset($this->session->userdata['valid_login'])) {
                redirect(base_url());
            } else if ($this->session->userdata['valid_login']['Type'] != 'Super Admin') {
                redirect(base_url());
            }
        }
    }

}

class Supervisor_Controller extends MY_Controller {

    public $user_data = null;
    public $user_type = null;
    public $user_id = null;

    function __construct() {
        parent::__construct();

        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {

            if (!isset($this->session->userdata['valid_login'])) {
                redirect(base_url());
            } else if ($this->session->userdata['valid_login']['Type'] != 'Supervisor') {
                redirect(base_url());
            }
        }
    }

}

class Employee_Controller extends MY_Controller {

    public $user_data = null;
    public $user_type = null;
    public $user_id = null;

    function __construct() {
        parent::__construct();

        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {

            if (!isset($this->session->userdata['valid_login'])) {
                redirect(base_url());
            } else if ($this->session->userdata['valid_login']['Type'] != 'Employee') {
                redirect(base_url());
            }
        }
    }

}


