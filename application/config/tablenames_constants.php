<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * This file is for Keeping Tablenames in single place and can be used as CONSTANT
 */
/* Performer Related Tables */

define("TABLE_USER", "transition_users");
define("TABLE_BANNES", "transition_banners");
define("TABLE_ARTISTS", "transition_artists");
define("TABLE_ALL_PHOTOS", "transition_all_photos");
define("TABLE_CMS", "transition_cms");
define("TABLE_TESTIMONIALS", "transition_testimonials");
define("TABLE_PATIENTS", "transition_patients");
define("TABLE_VIDEO", "transition_videos");
define("TABLE_ABOUT_WRITERS", "transition_about_writers");
define("TABLE_CONTACTS", "transition_contacts");
define("TABLE_LINK", "transition_helpful_links");
define("TABLE_CLIENT", "transition_clients");
define("TABLE_SHIFTLOG", "transition_shiftlog");
define("TABLE_SHIFTLOG_VISIT", "transition_shiftlog_visits");
define("TABLE_MISCELLANEOUS_FILE", " transition_miscellaneous_files");
define("TABLE_CLIENT_REPORT", "transition_clients_reports");
define("TABLE_EMPLOYEE_REPORT", " transition_employee_report");
define("TABLE_CONTACT_US", " transition_contact_us");
define("TABLE_SETTINGS", " transition_settings");
define("TABLE_ADMIN", " transition_admin");
