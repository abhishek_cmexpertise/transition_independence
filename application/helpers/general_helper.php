<?php


function admin_url($url = '')
{
    $CI = &get_instance();
    return $CI->config->config['admin_url'] . $url;
}

function supervisor_url($url = '')
{
    $CI = &get_instance();
    return $CI->config->config['supervisor_url'] . $url;
}

function employee_url($url = '')
{
    $CI = &get_instance();
    return $CI->config->config['employee_url'] . $url;
}


function upload_single_image($file, $name, $path, $thumb = FALSE)
{
    $CI = &get_instance();

    $return['error'] = '';
    $image_name = $name . '_' . time();

    $CI->load->helper('form');
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|PNG|JPG';
    $config['file_name'] = $image_name;

    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);

    $CI->upload->set_allowed_types('gif|jpg|png|jpeg|JPEG|PNG|JPG|GIF');

    if (!$CI->upload->do_upload(key($file))) {
        $return['error'] = $CI->upload->display_errors();
    } else {
        $result = $CI->upload->data();
        $return['data'] = $result;
    }

    if ($thumb == TRUE && $return['error'] == '') {
        $CI->load->library('Mylibrary');
        $thumb_array = array(
            array('width' => '100', 'height' => '100', 'image_type' => 'SMALL'),
            array('width' => '250', 'height' => '250', 'image_type' => 'MEDIUM'));
        for ($i = 0; $i < count($thumb_array); $i++) {
            $imageinfo = getimagesize($result['full_path']);
            $thumbSize = $CI->mylibrary->calculateResizeImage($imageinfo[0], $imageinfo[1], $thumb_array[$i]['width'], $thumb_array[$i]['height']);

            $CI->load->library('image_lib');
            $conf['image_library'] = 'gd2';
            $conf['source_image'] = $path . $result['orig_name'];
            $conf['create_thumb'] = TRUE;
            $conf['maintain_ratio'] = TRUE;
            $conf['new_image'] = $result['orig_name'];
            $conf['thumb_marker'] = "_" . $thumb_array[$i]['image_type'];
            $conf['width'] = $thumbSize['width'];
            $conf['height'] = $thumbSize['height'];
            $CI->image_lib->clear();
            $CI->image_lib->initialize($conf);
            if (!$CI->image_lib->resize()) {
                $return['error'] = 'Thumb Not Created';
            }
        }
    }

    return $return;
}

function delete_single_image($fullPath, $fileName)
{
    unlink($fullPath . '/' . $fileName);
}

function delete_image($array, $path)
{
    $CI = &get_instance();
    $img = $CI->db->select($array['field'])->where('int_glcode', $array['id'])->get($array['table'])->row_array();
    $mainImg = $img[$array['field']];
    $expImg = explode('.', $mainImg);
    $imgdelete = $path . $mainImg;
    if (file_exists($imgdelete)) {
        unlink($imgdelete);
    }
    return TRUE;
}

function sendMail($data) {
    $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.gmail.com',
        'smtp_port' => 465,
        'smtp_user' => 'digvijay.cmexpertise@gmail.com',
        'smtp_pass' => 'cm@@123#',
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
    );

    $CI = &get_instance();

    $CI->load->library('email', $config);
    $CI->email->initialize($config);
    $CI->email->set_newline("\r\n");
    $CI->email->from('digvijay.cmexpertise@gmail.com', 'Transitiontoindependence');
    $CI->email->to('maulik.cmexpertise@gmail.com');
    $CI->email->subject($data['subject']);
    $CI->email->message($data['message']);

    if ($CI->email->send()) {
        return true;
    } else {
        return FALSE;
    }
}


/* Commonly upload all type of file */

function upload_file_commonaly($file, $name, $path) {
    $CI = &get_instance();

    $return['error'] = '';
    $image_name = $name;

    $CI->load->helper('form');
    $config['upload_path'] = $path;
    $config['allowed_types'] = '*';
    $config['file_name'] = $image_name;

    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);

    if (!$CI->upload->do_upload(key($file))) {
        $return['error'] = $CI->upload->display_errors();
    } else {
        $result = $CI->upload->data();
        $return['data'] = $result;
    }

    return $return;
}
