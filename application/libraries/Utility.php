<?php

class Utility {

    public $sPass = "BGV9UAw9wp5tT7I9wRcw%Xv!a@8aApqW";
    public $iv = "lkK#_YNpaujT1Q)S";
    public $method = 'aes-256-cbc';

//    public $skey = "PHISHEMPRO_PRODUCT-CRTD16092016\0";

    /* Crypto Encryption */

    function newenCode($value) {
        $key = substr(hash('sha256', $this->sPass, true), 0, 32);
        $encrypted = base64_encode(openssl_encrypt($value, $this->method, $key, OPENSSL_RAW_DATA, $this->iv));

        $data = str_replace(array(
            '+',
            '/',
                ), array(
            '--',
            '__',
                ), $encrypted);

        return $data;
    }

    /* Crypto Decryption */

    function newdeCode($value) {
        $data = str_replace(array(
            '--',
            '__'
                ), array(
            '+',
            '/'
                ), $value);
        $key = substr(hash('sha256', $this->sPass, true), 0, 32);
        $decrypted = openssl_decrypt(base64_decode($data), $this->method, $key, OPENSSL_RAW_DATA, $this->iv);
        return $decrypted;
    }

    function encodeText($value, $removeTags = false) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $val = $removeTags ? strip_tags($v) : $v;
                $val = addslashes($val);
                $value [$k] = $val;
            }
        } else {
            $value = $removeTags ? strip_tags($value) : $value;
            $value = addslashes($value);
        }
        return $value;
    }

    function decodeText($value, $htmlEntity = true) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $val = stripslashes($v);
                $value [$k] = $htmlEntity ? htmlentities($val) : $val;
            }
        } elseif (is_object($value)) {
            foreach ($value as $k => $v) {
                $val = stripslashes($v);
                $value->$k = $htmlEntity ? htmlentities($val) : $val;
            }
        } else {
            $value = stripslashes($value);
            $value = $htmlEntity ? htmlentities($value) : $value;
        }
        return $value;
    }

    public function safe_b64encode($string) {
        $data = base64_encode($string);
        $data = str_replace(array(
            '+',
            '/',
            '='
                ), array(
            '-',
            '_',
            ''
                ), $data);
        return $data;
    }

    public function safe_b64decode($string) {
        $data = str_replace(array(
            '-',
            '_'
                ), array(
            '+',
            '/'
                ), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public function encode($value) {
        if (!$value) {
            return false;
        }
        $text = $value;
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $crypttext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->skey, $text, MCRYPT_MODE_ECB, $iv);
        return trim($this->safe_b64encode($crypttext));
    }

    public function decode($value) {
        if (!$value) {
            return false;
        }
        $crypttext = $this->safe_b64decode($value);
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->skey, $crypttext, MCRYPT_MODE_ECB, $iv);
        return (trim($decrypttext));
    }

    public function setFlashMessage($type, $message) {

        $CI = & get_instance();
        $template = '<div class="alert alert-' . $type . '  login-msg alert-dismissible text-center showhide" role="alert">
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						   <span aria-hidden="true" class="close_padding">&times;</span>
							</button>' . $message . '</div>';

        $CI->session->set_flashdata("myMessage", $template);
    }

    public function sendMailSMTP($data) {

        $CI = & get_instance();
        $config ['protocol'] = "smtp";
        $config ['smtp_host'] = SMTP_HOST;
        $config ['smtp_port'] = SMTP_PORT;
        $config ['smtp_user'] = SMTP_USER;
        $config ['smtp_pass'] = SMTP_PASS;
        $config ['smtp_timeout'] = 50;
        $config ['priority'] = 1;
        $config ['charset'] = 'iso-8859-1';
        $config ['wordwrap'] = TRUE;
        $config ['newline'] = "\r\n";
        $config ['mailtype'] = "html";
        $mail_config['smtp_crypto'] = 'tls';

        $message = $data ["message"];

        $CI->load->library('email');
        $CI->email->initialize($config);
        $CI->email->clear(TRUE);
        $CI->email->to($data ["to"]);

        if (isset($data ['from'])) {
            $CI->email->from($data ['from'], $data ['from_title']);
        } else {
            $CI->email->from($config ['smtp_user'], EMAIL_TITLE);
        }
        if (isset($data ["bcc"])) {
            $CI->email->bcc($data ["bcc"]);
        }
        if (isset($data ["replyto"])) {
            $CI->email->reply_to($data ["replyto"], $data ['from_title']);
        }
        $CI->email->subject($data ["subject"]);
        $CI->email->message($message);
        if ((isset($data['attachment'])) && (!empty($data['attachment']))) {
            $CI->email->attach($data['attachment']);
            unset($data['attachment']);
        }

        $response = $CI->email->send();
        return $response;
    }

    // mail function
    function sendMail($row) {
        $CI = & get_instance();
        $to = $row ['to'];
        $subject = $row ['subject'];
        $message = $row ['message'];

        $headers = 'From: ' . 'StageGator' . '<noreply@stagegator.com>' . "\r\n";
        $headers .= 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type:  text/html; charset=utf-8' . "\r\n";
        $headers .= 'Reply-To: noreply@stagegator.com' . "\r\n";
        $headers .= 'Return-Path: noreply@stagegator.com' . "\r\n";
        return mail($to, $subject, $message, $headers);
    }

    /**
     * fbShareButton()
     * This function is used to facebook share button
     *
     * Developer - Pravin Dabhi
     * Datetime - 7-11-2016 03:44
     *
     * @param : $content: String content to share
     * @return : Google plus share button
     */
    public function fbShareButton($image = null, $title = null, $description = null, $url = null) {
        $description = urldecode($description);
        $description = str_replace([
            "</br>",
            "<br/>",
            "</p>"
                ], [
            "\r\n",
            "\r\n",
            "</p>\r\n"
                ], $description);
        $description = strip_tags($description);
        $description = urlencode($description);
        ?>

        <a href="https://www.facebook.com/sharer/sharer.php?u=<?= $url ?>&title=<?php echo trim($title); ?>
        <?php if ($image != "") { ?>&picture=<?php echo $image; ?> <?php } ?>&description=<?php echo $description; ?>"
           onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;"
           target="_blank" title="Share on Facebook"><img
                src="<?php echo EXTERNAL_PATH ?>images/fb_share_s.png" /> </a>

        <?php
    }

    function getWebPage($url) {

        $user_agent = $_SERVER['HTTP_USER_AGENT'];

        $options = array(
            CURLOPT_CUSTOMREQUEST => "POST", // set request type post or get
            CURLOPT_POST => false, // set to GET
            CURLOPT_USERAGENT => $user_agent, // set user agent
            CURLOPT_COOKIEFILE => "cookie.txt", // set cookie file
            CURLOPT_COOKIEJAR => "cookie.txt", // set cookie jar
            CURLOPT_RETURNTRANSFER => true, // return web page
            CURLOPT_HEADER => false, // don't return headers
            CURLOPT_FOLLOWLOCATION => true, // follow redirects
            CURLOPT_ENCODING => "", // handle all encodings
            CURLOPT_AUTOREFERER => true, // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
            CURLOPT_TIMEOUT => 120, // timeout on response
            CURLOPT_MAXREDIRS => 10
        );  // stop after 10 redirects

        $ch = curl_init($url);
        curl_setopt_array($ch, $options);
        $content = curl_exec($ch);
        $err = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        curl_close($ch);

        $header ['errno'] = $err;
        $header ['errmsg'] = $errmsg;
        $header ['content'] = $content;
        return $header;
    }

}
?>