<?php

class Clientreport_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getClientdata()
    {   
          $data['select']=['client_id','consumer_name']; 
       $data ['table'] =TABLE_CLIENT;
        $clientdata = $this->selectRecords($data);
        return $clientdata;
    }
    
    public function clientdetail($postdata)
    {
       $data['where'] = ['client_id'=>$postdata['client_id']];
        $data['select']=['client_uci','client_dob','consumer_name']; 
       $data ['table'] =TABLE_CLIENT;
       $clientdata = $this->selectRecords($data);
       return $clientdata;
    }
    
    public function addClientreport($postData)
    {
              $userid = $this->session->userdata['valid_login']['id'];
              $report_date =  date('Y-m-d',strtotime($postData['report_date']));
               
        $insertclientreport = [
            
            "client_id" => $postData['client_id'],
            "employee_id" => $userid,
            "report_date" => $report_date,
            "objective1" => $postData['objective1'],
            "objective2" => $postData['objective2'],
            "objective3" => $postData['objective3'],
            "objective4" => $postData['objective4'],
            "objective5" => $postData['objective5'],
            "comment1" => $postData['comment1'],
            "comment2" => $postData['comment2'],
            "comment3" => $postData['comment3'],
            "comment4" => $postData['comment4'],
            "comment5" => $postData['comment5'],
            "comment" => $postData['comment'],
            "dt_added" => date('Y-m-d')
        ];
        
        $data['insert'] = $insertclientreport;
        $data['table'] = TABLE_CLIENT_REPORT;
        $result = $this->insertRecord($data);
    }
    
    public function getclientreport($postdata)
    {
        $data['select'] = ['c.client_id','c.consumer_name','c.client_uci','c.client_dob'];
        $data['table'] = TABLE_CLIENT_REPORT.' as cr';
        $data['join'] = [
           TABLE_CLIENT.' as c' => [
                "c.client_id = cr.client_id",
            ],
         ];
       $data['where'] = ['cr.report_id' => $postdata];
      $result = $this->selectFromJoin($data);
    
       return $result;
    }
     
    public function clientreportdata($postdata)
    {
               $data['select']=['*']; 
        $data ['table'] = TABLE_CLIENT_REPORT.' as cr';
       $data['join'] = [
        
           TABLE_CLIENT.' as c' => [
                "c.client_id = cr.client_id"
            ],
             TABLE_USER.' as u' => [
                "u.user_id = cr.employee_id"
            ],
           ];
         $data ['where'] = ['cr.report_id' => $postdata];
        $singleclientreport = $this->selectFromJoin($data);
        
       
        return $singleclientreport;
    }
    
      public function clientreportdatas($postData) {
        $idArray = array();
        for ($i = 0; $i < count(@$postData['checkedId']); $i++) {

            $ids = $this->utility->newdeCode($postData['checkedId'][$i]);
            if (!ctype_digit($ids)) {
                $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
                redirect(admin_url() . 'clientreport');
            }
            $idArray[] = $ids;
        }

        $data['select'] = ['*'];
        $data['table'] = TABLE_CLIENT_REPORT . ' as cr';
        $data['join'] = [
            TABLE_CLIENT . ' as c' => [
                "c.client_id = cr.client_id",'LEFT'
            ],
            TABLE_USER . ' as u' => [
                "u.user_id = cr.employee_id",'LEFT'
            ],
        ];
        $data['where_in'] = ['cr.report_id' => $idArray];
        $clientreports = $this->selectFromJoin($data, TRUE);

        return @$clientreports;
    }

}

?>
