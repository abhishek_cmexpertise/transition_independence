<?php

class Dashboard_model extends My_model {

    public function __construct() {
        parent::__construct();
    }
    
    public function getsemianualreport()
    {
        
           $current = date("Y-m-d");
           $limit_last_date = strtotime(date("Y-m-d")." +6 days");
           $limit_end_date = date("Y-m-d",$limit_last_date);
           
           if(date("Y") == date('Y',$limit_last_date))
           {
           $data['select'] = ['*'];
           $data['where'] = "DATE_FORMAT(DATE_ADD(client_dob, INTERVAL 6 MONTH), '%m-%d') BETWEEN 
                                            DATE_FORMAT('".$current."', '%m-%d') AND DATE_FORMAT('".$limit_end_date."', '%m-%d')";
           $data['table'] = TABLE_CLIENT;
           $result =$this->selectRecords($data);
           }
           else
           {
            $limit_end_date2 = date("Y-12-31"); 
            $star_date2 = date('Y-01-01',strtotime('+1 year'));
                  
            $data['select'] = ['*'];
            $data['where'] = " DATE_FORMAT(DATE_ADD(client_dob, INTERVAL 6 MONTH), '%m-%d') BETWEEN DATE_FORMAT('".$current."', '%m-%d') AND DATE_FORMAT('".$limit_end_date2."', '%m-%d') OR DATE_FORMAT(DATE_ADD(client_dob, INTERVAL 6 MONTH), '%m-%d') BETWEEN DATE_FORMAT('".$star_date2."', '%m-%d') AND DATE_FORMAT('".$limit_end_date."', '%m-%d')";
           $data['table'] = TABLE_CLIENT;
           $result =$this->selectRecords($data);
           }
           return $result;
    }
    
     public function getAnnualreport() {
        $current = date("Y-m-d");
        $limit_last_date = strtotime(date("Y-m-d") . " +6 days");
        $limit_end_date = date("Y-m-d", $limit_last_date);
        if (date("Y") == date('Y', $limit_last_date)) {
            $data['select'] = ['*'];
            $data['where'] = "DATE_FORMAT(client_dob, '%m-%d') BETWEEN DATE_FORMAT('" . $current . "', '%m-%d') AND DATE_FORMAT('" . $limit_end_date . "', '%m-%d')";
            $data['table'] = TABLE_CLIENT;
            $result = $this->selectRecords($data);
        } else {
            $limit_end_date2 = date("Y-12-31");
            $star_date2 = date('Y-01-01', strtotime('+1 year'));

            $data['select'] = ['*'];
            $data['where'] = "DATE_FORMAT(client_dob, '%m-%d') BETWEEN DATE_FORMAT('" . $current . "', '%m-%d') AND DATE_FORMAT('" . $limit_end_date2 . "', '%m-%d') OR  DATE_FORMAT(client_dob, '%m-%d') BETWEEN DATE_FORMAT('".$star_date2."', '%m-%d') AND DATE_FORMAT('".$limit_end_date."', '%m-%d')";
            $data['table'] = TABLE_CLIENT;
            
            $result = $this->selectRecords($data);
        }
        return $result;
    }
}
?>
