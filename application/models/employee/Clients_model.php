<?php

class Clients_model extends My_model {

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/calcutta');
    }

    public function getClientview($postdata) {
        $data['select'] = ['*'];
        $data ['where'] = ['client_id' => $postdata];
        $data ['table'] = TABLE_CLIENT;
        $singleclientdata = $this->selectRecords($data);
        return $singleclientdata;
    }

    public function shiftlogview($postdata) {

        $data['select'] = ['sv.start_time', 'sv.end_time', 'sv.total_time', 'sv.travel_time', 'sv.consumer_details', 's.shiftlog_date', 's.total_hours', 's.total_travel_time', 'c.client_id', 'c.consumer_name', 'u.first_name', 'u.middle_name', 'u.last_name'];
        $data ['table'] = TABLE_SHIFTLOG_VISIT . ' as sv';
        $data['join'] = [
            TABLE_SHIFTLOG . ' as s' => [
                "s.shiftlog_id = sv.shiftlog_id", 'LEFT'
            ],
            TABLE_USER . ' as u' => [
                "u.user_id = s.staff_id", 'LEFT'
            ],
            TABLE_CLIENT . ' as c' => [
                "c.client_id = s.client_id", 'LEFT'
            ],
        ];
        $data ['where'] = ['sv.shiftlog_id' => $postdata];
        $singleshiftlogdata = $this->selectFromJoin($data);

        return $singleshiftlogdata;
    }

    public function getclientshiftlog($postdata) {


        $data['select'] = ['c.consumer_name', 'c.remaining_hours', 'c.allocated_hours', 'shiftlog_date', 'c.client_id'];
        $data ['table'] = TABLE_CLIENT . ' as c';
        $data['join'] = [
            TABLE_SHIFTLOG . ' as s' => [
                "c.client_id = s.client_id",
                "LEFT"
            ],
            TABLE_USER . ' as u' => [
                "u.user_id = s.staff_id",
                "LEFT"
            ],
        ];
        $data ['where'] = ['c.client_id' => $postdata];
        $singleshiftlogdata = $this->selectFromJoin($data);
        return $singleshiftlogdata;
    }

    public function addShiftlog($postData) {

        $data['select'] = ['remaining_hours', 'total_used_hours'];
        $data['where'] = ['client_id' => $postData['client_id']];
        $data['table'] = TABLE_CLIENT;
        $clientdata = $this->selectRecords($data);

        $here_used_hours = $postData['tot_hour'];
        $remaining_hours = $clientdata[0]->remaining_hours;
        $till_used_hours = $clientdata[0]->total_used_hours;

        $remaining_hours_arr = explode(':', $remaining_hours);
        $remainingTimestamp = 3600 * $remaining_hours_arr[0] + 60 * $remaining_hours_arr[1] + $remaining_hours_arr[2];

        $here_used_hours_arr = explode(':', $here_used_hours);
        $usedTimestamp = 3600 * $here_used_hours_arr[0] + 60 * $here_used_hours_arr[1] + $here_used_hours_arr[2];

        $till_used_hours_arr = explode(':', $till_used_hours);
        $tillUsedTimestamp = 3600 * $till_used_hours_arr[0] + 60 * $till_used_hours_arr[0] + $till_used_hours_arr[0];

        if ($remainingTimestamp >= $usedTimestamp) {

            $staff_id = $this->session->userdata['valid_login']['id'];
            $seconds = $remainingTimestamp - $usedTimestamp;
            $minutes = ($seconds / 60) % 60;
            $hours = floor($seconds / (60 * 60));
            $seconds2 = $tillUsedTimestamp + $usedTimestamp;
            $minutes2 = ($seconds2 / 60) % 60;
            $hours2 = floor($seconds2 / (60 * 60));
            $now_remaining = sprintf("%02d", $hours) . ":" . sprintf("%02d", $minutes) . ":00";
            $till_total_used = sprintf("%02d", $hours2) . ":" . sprintf("%02d", $minutes2) . ":00";
            $total_hours_arr = explode(':', $here_used_hours);
            $total_hours = sprintf("%02d", $total_hours_arr[0]) . ":" . sprintf("%02d", $total_hours_arr[1]) . ":00";
            $total_travel_time_arr = explode(':', $postData['tot_travel_time']);
            $total_travel_time = sprintf("%02d", $total_travel_time_arr[0]) . ":" . sprintf("%02d", $total_travel_time_arr[1]) . ":00";
            $insert = array("shiftlog_date" => date('Y-m-d', strtotime($postData['shiftlog_date'])),
                "client_id" => $postData['client_id'],
                "staff_id" => $staff_id,
                "total_hours" => $total_hours,
                "total_travel_time" => $total_travel_time,
                "dt_added" => date('y-m-d h:i:s'),
            );
            $data['insert'] = $insert;
            $data['table'] = TABLE_SHIFTLOG;
            $response = $this->insertRecord($data);
            $id = $this->db->insert_id();

            if ($response) {

                unset($data);
                if (count($postData['total_time']) > 0) {
                    foreach ($postData['total_time'] as $k => $val) {
                        unset($data);
                        if ($val != "" && $postData['start_time'][$k] != "" && $postData['end_time'][$k] != "" &&
                                $postData['total_time'][$k] != "" && $postData['consumer_detail'][$k] != "" && $postData['travel_time'][$k] != "") {
                            $insert_shiftlog_visit = array("shiftlog_id" => $id,
                                "start_time" => $postData['start_time'][$k],
                                "end_time" => $postData['end_time'][$k],
                                "total_time" => $postData['total_time'][$k],
                                "consumer_details" => $postData['consumer_detail'][$k],
                                "travel_time" => $postData['travel_time'][$k],
                                "dt_added" => date('y-m-d h:i:s'),
                            );
                            $data['insert'] = $insert_shiftlog_visit;
                            $data['table'] = TABLE_SHIFTLOG_VISIT;
                            $response = $this->insertRecord($data);
                        }
                    }
                }
            }

            $update = array("remaining_hours" => $now_remaining,
                "total_used_hours" => $till_total_used,
                "dt_updated" => date('y-m-d h:i:s'),
            );

            $data['update'] = $update;
            $data['table'] = TABLE_CLIENT;
            $data['where'] = ['client_id' => $postData['client_id']];
            $this->updateRecords($data);
            
            if(strtotime($here_used_hours) > strtotime('08:00:00'))
            {
                $data['where'] = ['user_id'=>$staff_id];
                $data['select'] = ['first_name','middle_name','last_name','user_email'];
                $data['table'] = TABLE_USER;
                
               $userdata = $this->selectRecords($data);
                
               $config = Array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.gmail.com',
                'smtp_port' => 465,
                'smtp_user' => 'digvijay.cmexpertise@gmail.com',
                'smtp_pass' => 'cm@@123#',
                'mailtype' => 'html',
                'charset' => 'iso-8859-1',
                'wordwrap' => TRUE
            );
            	
            $email_message = "One of your employee has been worked more than 8 hours on shift-log. Following is employee's details:<br/>
	                          Date :".$_REQUEST['shiftlog_date']."<br/>
	                          Employee Name :".$userdata[0]->first_name." ".$userdata[0]->middle_name." ".$userdata[0]->last_name."<br/>
	                          Employee Email :".$userdata[0]->user_email."<br/>
	                          Client Name :".$clientdata[0]->consumer_name."<br/>
	                          Total Hours :".$total_hours."<br/>
	                          Total Travel :".$total_travel_time."<br/>";
            $CI = &get_instance();

            $CI->load->library('email', $config);
            $CI->email->initialize($config);
            $CI->email->set_newline("\r\n");
            $CI->email->from('digvijay.cmexpertise@gmail.com', 'Transitiontoindependence');
            $CI->email->to('maulik.cmexpertise@gmail.com,');
            $CI->email->subject("Transition to Independence Password Generation For Employee Registration.");
            $CI->email->message($email_message);

            if ($CI->email->send()) {
                if ($response) {
                    $response = ['success', 'Employee Added successfully'];
                } else {
                    $response = ['danger', DEFAULT_MESSAGE];
                }

                return $response;
                return true;
            } else {
                return FALSE;
            }  
            }
           
        } else {
            $ids = $this->utility->newenCode($postData['client_id']);
            redirect(employee_url() . 'Clients/shiftlog_form/' . $ids);
        }
    }

}

?>
