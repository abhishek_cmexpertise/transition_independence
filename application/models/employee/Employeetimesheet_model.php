<?php

class Employeetimesheet_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function shiftlogdata($postData) {

        $data['where'] = ['report_id' => $postData];
        $data['select'] = ['*'];
        $data['table'] = TABLE_EMPLOYEE_REPORT . ' as er';
        $data['join'] = [
            TABLE_USER . ' as u' => [
                "u.user_id = er.employee_id", 'LEFT'
            ],
        ];
        $result = $this->selectFromJoin($data);

        $timestamp = strtotime($result[0]->report_month . " " . $result[0]->report_year);

        $search_duration = $result[0]->report_half;
        if ($search_duration == 1) {

            $first_date = date('Y-m-01', $timestamp);
            $last_date = date('Y-m-15', $timestamp);

            $start_day = 1;
            $end_day = 15;
        } else {
            $first_date = date('Y-m-16', $timestamp);
            $last_date = date('Y-m-t', $timestamp);

            $start_day = 16;
            $end_day = date('t', $timestamp);
        }

        $pay_period = $result[0]->report_month . " (From " . $first_date . " to " . $last_date . "),  " . $result[0]->report_year;

        $employee_id = $result[0]->employee_id;

        $data['where'] = ['staff_id' => $employee_id, 'shiftlog_date >=' => date('Y-m-d', strtotime($first_date)), 'shiftlog_date <=' => date('Y-m-d', strtotime($last_date))];
        $data['select'] = ['DISTINCT(s.client_id)'];
        $data['table'] = TABLE_SHIFTLOG . " as s";
        $data['order'] = 'shiftlog_id';
        $logresult = $this->selectRecords($data);

        unset($data);
        for ($i = 0; $i < count($logresult); $i++) {
            $data['where'] = "client_id =" . $logresult[$i]->client_id;
            $data['select'] = ['*'];
            $data['table'] = TABLE_CLIENT;
            $shiftlog[$i] = $this->selectRecords($data);

            unset($data);
            for ($j = $start_day; $j <= $end_day; $j++) {
                $select_date = $result[0]->report_year . "-" . date('m', strtotime($result[0]->report_month)) . "-" . sprintf("%02d", $j);
                $data['where'] = ['shiftlog_date' => $select_date, 'client_id' => $logresult[$i]->client_id];
                $data['select'] = ['*'];
                $data['table'] = TABLE_SHIFTLOG;
                $logs[$i][$j] = $this->selectRecords($data);
            }
        }
        return array($pay_period, $result, @$shiftlog, 'start_day' => $start_day, 'end_day' => $end_day, @$logs);
    }
    
    public function employeelog($postData)
    {
     
        $timestamp = strtotime($postData['month'] . " " . $postData['year']);

        $search_duration = $postData['search_duration'];
        if ($search_duration == 1) {

            $first_date = date('Y-m-01', $timestamp);
            $last_date = date('Y-m-15', $timestamp);

            $start_day = 1;
            $end_day = 15;
        } else {
            $first_date = date('Y-m-16', $timestamp);
            $last_date = date('Y-m-t', $timestamp);

            $start_day = 16;
            $end_day = date('t', $timestamp);
        }
        
        $employee_id = $this->session->userdata['valid_login']['id'];
       $data['select'] = ['s.*','c.consumer_name','u.first_name','middle_name','last_name'];
        $data['join'] = [
             TABLE_SHIFTLOG . ' as s' => [
                "c.client_id = s.client_id", 'LEFT'
            ], 
            TABLE_USER . ' as u' => [
                "u.user_id = s.staff_id", 'LEFT'
            ], 
           ];
        $data['table'] = TABLE_CLIENT. ' as c';
         $data['where'] = ['s.staff_id' => $employee_id, 's.shiftlog_date >=' => date('Y-m-d', strtotime($first_date)), 's.shiftlog_date <=' => date('Y-m-d', strtotime($last_date))];
        $data['order'] = 'shiftlog_id DESC';
        $result = $this->selectFromJoin($data);
        if(count($result)>0)
          {
            $sub_total_hours_in_minute = 0;
            $sub_total_hours_in_minute2 = 0;
            $sub_total_miles = 0;

            foreach($result as $results)
            {
                    $parts = explode(':', $results->total_hours);
                    $parts2 = explode(':', $results->total_travel_time);
                    $minutes = ($parts[0] * 60) + $parts[1];
                    $minutes2 = ($parts2[0] * 60) + $parts2[1];
                    $sub_total_hours_in_minute = $sub_total_hours_in_minute + $minutes;
                    $sub_total_hours_in_minute2 = $sub_total_hours_in_minute2 + $minutes2;
            }
                    $sub_hours = floor($sub_total_hours_in_minute / 60);
                    $sub_minutes = ($sub_total_hours_in_minute % 60);
                    $sub_hours2 = floor($sub_total_hours_in_minute2 / 60);
                    $sub_minutes2 = ($sub_total_hours_in_minute2 % 60);
                    $sub_total_hours = sprintf('%02d', $sub_hours) . ":" . sprintf('%02d', $sub_minutes) . ":00";
                    $sub_total_travel_hours = sprintf('%02d', $sub_hours2) . ":" . sprintf('%02d', $sub_minutes2) . ":00";
                    $insert = array("report_month" => $postData['month'],
                       "report_half" => $postData['search_duration'],
                       "report_year" => $postData['year'],                   
                       "submission_date" => date('Y-m-d'),
                       "employee_id" => $employee_id,
                       "subtotal_hours" => $sub_total_hours,
                       "subtotal_travel_time_google" => $sub_total_travel_hours,
                      "dt_added" => date('Y-m-d h:i:s')
                            );
                    $data['where'] = ['employee_id'=>$employee_id,'report_month'=>$postData['month'],'report_month'=>$postData['year'],'report_month'=>$postData['search_duration']];
                    $data['select'] = ['*'];
                    $data['table'] = TABLE_EMPLOYEE_REPORT;
                    $check_already = $this->countRecords($data);
                  
                    
                   if($check_already == 0)
                    {
                   $data['insert'] = $insert;
                   $data['table'] = TABLE_EMPLOYEE_REPORT;
                   $this->insertRecord($data);
                    }
                    else
                    {
                        return false;
                    }
                  }
        }

    public function employeetimesheetview($postdata) {
        $data['select'] = ['report_month', 'report_year'];
        $data['table'] = TABLE_EMPLOYEE_REPORT;
        $data['where'] = ['report_id' => $postdata];
        $result = $this->selectRecords($data);

        $month = $result[0]->report_month;
        $year = $result[0]->report_year;

        $timestamp = strtotime($month . " " . $year);
        $first_date = date('Y-m-01', $timestamp);
        $last_date = date('Y-m-t', $timestamp);


        $data['select'] = ['s.total_hours', 'total_travel_time'];
        $data['table'] = TABLE_SHIFTLOG . ' as s';
        $data['join'] = [
            TABLE_CLIENT . ' as c' => [
                "c.client_id = s.client_id", 'LEFT' 
            ],
        ];
        $data['where'] = ['s.shiftlog_date  >=' => $first_date, 's.shiftlog_date <=' => $last_date];
        $result = $this->selectFromJoin($data);
        $resultc = $this->countRecords($data);

        $sub_total_hours_in_minute = 0;
        $sub_total_hours_in_minute2 = 0;
        for ($i = 0; $i < $resultc; $i++) {

            $total_hours = $result[$i]->total_hours;
            
           
            $time = explode(':', $total_hours);
            $minutes = ($time[0] * 60) + $time[1];
            $sub_total_hours_in_minute = $sub_total_hours_in_minute + $minutes;

            $total_travel_time = $result[$i]->total_travel_time;
            $travel_time = explode(':', $total_travel_time);
           $travel_time_minutes = ($travel_time[0] * 60) + $travel_time[1];
            $sub_total_hours_in_minute2 = $sub_total_hours_in_minute2 + $travel_time_minutes;
        }
        $sub_hours = floor($sub_total_hours_in_minute / 60);
        
        $sub_minutes = ($sub_total_hours_in_minute % 60);
        $sub_total_hours = sprintf('%02d', $sub_hours) . ":" . sprintf('%02d', $sub_minutes) . ":00";

        $sub_hours2 = floor($sub_total_hours_in_minute2 / 60);
        $sub_minutes2 = ($sub_total_hours_in_minute2 % 60);
        $sub_total_travel_hours = sprintf('%02d', $sub_hours2) . ":" . sprintf('%02d', $sub_minutes2) . ":00";

        
        $subtotalhours = array(
            $sub_total_hours,
            $sub_total_travel_hours
        );

        return $subtotalhours;

    }

   function getLog($postdata) {

         $data['select'] = ['report_month', 'report_year'];
        $data['table'] = TABLE_EMPLOYEE_REPORT;
        $data['where'] = ['report_id' => $postdata];
        $result = $this->selectRecords($data);

        $month = $result[0]->report_month;
        $year = $result[0]->report_year;

        $timestamp = strtotime($month . " " . $year);
        $first_date = date('Y-m-01', $timestamp);
        $last_date = date('Y-m-t', $timestamp);

        $data['select'] = ['s.shiftlog_id', 's.shiftlog_date', 'c.client_uci', 'c.client_dob', 'c.consumer_name', 'u.first_name', 'u.middle_name', 'u.last_name', 's.total_hours', 'total_travel_time'];
        $data['table'] = TABLE_SHIFTLOG . ' as s';
        $data['join'] = [
            TABLE_CLIENT . ' as  c' => [
                "c.client_id = s.client_id"
            ],
            TABLE_USER . ' as  u' =>
                [
                "u.user_id = s.staff_id"
            ],
        ];
        $data['where'] = ['s.shiftlog_date >= ' => $first_date, 's.shiftlog_date <= ' => $last_date];
        $results = $this->selectFromJoin($data);
        return $results;
    }

}

?>