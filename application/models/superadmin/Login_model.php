<?php

class Login_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function loginCheck($postData) {

        $data['select'] = ['*'];
        $data['table'] = TABLE_USER;
        $data['where'] = ['username' => $postData['admin_username'], 'password' => md5($postData['admin_password']), 'user_type' => 'Super Admin'];
        $firstRow = $this->selectRecords($data);

        if (!empty($firstRow)) {

            if ($firstRow[0]->password == md5($postData['admin_password'])) {

                $sessionData['valid_login'] = [
                    'id' => $firstRow[0]->user_id,
                    'email' => $firstRow[0]->username,
                ];

                $this->session->set_userdata($sessionData);
                delete_cookie("admin_username");
                delete_cookie("admin_password");

                if ($this->input->post('rem') == 'yes') {

                    $userName = array(
                        'name' => 'admin_username',
                        'value' => $firstRow[0]->username,
                        'expire' => '86500',
                        'prefix' => '',
                        'secure' => FALSE
                    );
                    $this->input->set_cookie($userName);

                    $password = array(
                        'name' => 'admin_password',
                        'value' => $postData['admin_password'],
                        'expire' => '86500',
                        'prefix' => '',
                        'secure' => FALSE
                    );
                    $this->input->set_cookie($password);
                }

                $res['status'] = 'success';
                $res['message'] = 'Login successfully';
                $res['redirect'] = base_url() . 'superadmin/dashboard';
            } else {
                $res['status'] = 'error';
                $res['message'] = 'Password does not match';
            }
        } else {
            $res['status'] = 'error';
            $res['message'] = 'Please enter valid username and password';
        }

        return $res;
    }

}

?>
