<?php

class Testimonials_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getTestimonialsBannerData() {

        $data['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'Testimonials'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getTestimonialBannerDetail($id) {

        $data['select'] = ['*'];
        $data ['where'] = ['banner_id' => $id];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editTestimonialBannerDatas($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Testimonials Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getTestimonialInfoData() {

        $data ['select'] = ['*'];
        $data ['table'] = TABLE_TESTIMONIALS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function addTestimonialData($postData) {

        $uploadPath = 'public/assets/cms/upload/writer_pic/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
        $config['file_name'] = trim($_FILES['bigimg']['name']);

        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('bigimg')) {
            $result = $this->upload->data();

            $data['insert']['testimonial_title'] = $postData['writer_name'];
            $data['insert']['writer_designation'] = $postData['writer_desigmation'];
            $data['insert']['testimonial_date'] = date('Y-m-d', strtotime($postData['testimonial_date']));
            $data['insert']['testimonial_desc'] = $postData['banner_desc'];
            $data['insert']['writer_pic'] = 'upload/writer_pic/' . $result['file_name'];
            $data['insert']['status'] = 'Y';
            $data['insert']['dt_added'] = DATE_TIME;
            $data['insert']['dt_updated'] = DATE_TIME;
            $data['table'] = TABLE_TESTIMONIALS;
            $results = $this->insertRecord($data);
        }

        if ($results) {
            return ['success', 'Testimonials Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function getTestimonialsEditDetail($id) {

        $data ['select'] = ['*'];
        $data ['where'] = ['testimonial_id' => $id];
        $data ['table'] = TABLE_TESTIMONIALS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editTestimonialInfoData($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/writer_pic/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['writer_pic'] = 'upload/writer_pic/' . $result['file_name'];
            }
        }

        $data['update']['testimonial_title'] = $postData['writer_name'];
        $data['update']['writer_designation'] = $postData['writer_desigmation'];
        $data['update']['testimonial_date'] = date('Y-m-d', strtotime($postData['testimonial_date']));
        $data['update']['testimonial_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_TESTIMONIALS;
        $data['where'] = ['testimonial_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Testimonials Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function deleteTestimonialDetail($id) {

        $data ['where'] = ['testimonial_id' => $id];
        $data ['table'] = TABLE_TESTIMONIALS;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function updateStatus($postData) {

        $id = $this->utility->newdecode($postData['id']);
        $val = $postData['val'];

        $data['update']['status'] = $val;
        $data['table'] = TABLE_TESTIMONIALS;
        $data['where'] = ['testimonial_id' => $id];
        $results = $this->updateRecords($data);
    }

    public function handleMultiples($postData) {

        $ids = array();

        for ($i = 0; $i < count($postData['id']); $i++) {
            array_push($ids, $this->utility->newdecode($postData['id'][$i]));
        }

        $data['where_in'] = ['testimonial_id' => $ids];
        $data['table'] = TABLE_TESTIMONIALS;
        $res = $this->deleteRecords($data);

        if ($res) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

}

?>