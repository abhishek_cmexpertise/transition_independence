<?php

class Client_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getClientBannerData() {
        $data ['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'Patients'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getClientBannerDetail($id) {
        $data ['select'] = ['*'];
        $data ['where'] = ['banner_id' => $id];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editClientBannerDatas($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getClientInfoData() {

        $data ['select'] = ['*'];
        $data ['table'] = TABLE_PATIENTS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function addClients($postData) {

        $uploadPath = 'public/assets/cms/upload/patient_image/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
        $config['file_name'] = trim($_FILES['bigimg']['name']);

        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('bigimg')) {
            $result = $this->upload->data();

            $data['insert']['patient_name'] = $postData['client_name'];
            $data['insert']['patient_desc'] = $postData['banner_desc'];
            $data['insert']['patient_image'] = 'upload/patient_image/' . $result['file_name'];
            $data['insert']['status'] = 'Y';
            $data['insert']['dt_added'] = DATE_TIME;
            $data['insert']['dt_updated'] = DATE_TIME;
            $data['table'] = TABLE_PATIENTS;
            $results = $this->insertRecord($data);
        }

        if ($results) {
            return ['success', 'Recoed Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function getClientDatas($id) {

        $data ['select'] = ['*'];
        $data ['where'] = ['patient_id' => $id];
        $data ['table'] = TABLE_PATIENTS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editClientDatas($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['patient_image'] = 'upload/patient_image/' . $result['file_name'];
            }
        }


        $data['update']['patient_name'] = $postData['client_name'];
        $data['update']['patient_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_PATIENTS;
        $data['where'] = ['patient_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Record Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function deleteClientDetail($id) {

        $data ['where'] = ['patient_id' => $id];
        $data ['table'] = TABLE_PATIENTS;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function updateStatus($postData) {

        $id = $this->utility->newdecode($postData['id']);
        $val = $postData['val'];

        $data['update']['status'] = $val;
        $data['table'] = TABLE_PATIENTS;
        $data['where'] = ['patient_id' => $id];
        $results = $this->updateRecords($data);
    }

    public function handleMultiples($postData) {

        $ids = array();

        for ($i = 0; $i < count($postData['id']); $i++) {
            array_push($ids, $this->utility->newdecode($postData['id'][$i]));
        }

        $data['where_in'] = ['patient_id' => $ids];
        $data['table'] = TABLE_PATIENTS;
        $res = $this->deleteRecords($data);

        if ($res) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

}

?>