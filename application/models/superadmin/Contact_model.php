<?php

class Contact_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getContactListData() {
        $data['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'Contact'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getContactBannerDetail($id) {
        $data['select'] = ['*'];
        $data ['where'] = ['banner_id' => $id, 'banner_type' => 'Contact'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editContactBannerData($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Contact Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getContactInfoData() {
        $data['select'] = ['*'];
        $data ['table'] = TABLE_CONTACTS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function addContactInfoData($postData) {

        $uploadPath = 'public/assets/cms/upload/writer_pic/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
        $config['file_name'] = trim($_FILES['bigimg']['name']);

        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('bigimg')) {
            $result = $this->upload->data();

            $data['insert']['contact_name'] = $postData['contact_name'];
            $data['insert']['contact_designation'] = $postData['contact_desigmation'];
            $data['insert']['contact_phone'] = $postData['contact_phone'];
            $data['insert']['contact_fax'] = $postData['contact_fax'];
            $data['insert']['contact_email'] = $postData['contact_email'];
            $data['insert']['contact_pic'] = 'upload/writer_pic/' . $result['file_name'];
            $data['insert']['status'] = 'Y';
            $data['insert']['dt_added'] = DATE_TIME;
            $data['insert']['dt_updated'] = DATE_TIME;
            $data['table'] = TABLE_CONTACTS;
            $results = $this->insertRecord($data);
        }

        if ($results) {
            return ['success', 'Contact Information Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function getContactInfoEditDetail($id) {
        $data['select'] = ['*'];
        $data['where'] = ['contact_id' => $id];
        $data ['table'] = TABLE_CONTACTS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editContactInfoData($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/writer_pic/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['contact_pic'] = 'upload/writer_pic/' . $result['file_name'];
            }
        }

        $data['update']['contact_name'] = $postData['contact_name'];
        $data['update']['contact_designation'] = $postData['contact_desigmation'];
        $data['update']['contact_phone'] = $postData['contact_phone'];
        $data['update']['contact_fax'] = $postData['contact_fax'];
        $data['update']['contact_email'] = $postData['contact_email'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['where'] = ['contact_id' => $id];
        $data['table'] = TABLE_CONTACTS;

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Contact Detail Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function deleteContactDetail($id) {

        $data ['where'] = ['contact_id' => $id];
        $data ['table'] = TABLE_CONTACTS;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Contact Detail Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function updateStatus($postData) {

        $id = $this->utility->newdecode($postData['id']);
        $val = $postData['val'];

        $data['update']['status'] = $val;
        $data['table'] = TABLE_CONTACTS;
        $data['where'] = ['contact_id' => $id];
        $results = $this->updateRecords($data);
    }

    public function handleMultiples($postData) {

        $ids = array();

        for ($i = 0; $i < count($postData['id']); $i++) {
            array_push($ids, $this->utility->newdecode($postData['id'][$i]));
        }

        $data['where_in'] = ['contact_id' => $ids];
        $data['table'] = TABLE_CONTACTS;
        $res = $this->deleteRecords($data);

        if ($res) {
            $response = ['success', 'Contact Details Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

}

?>
