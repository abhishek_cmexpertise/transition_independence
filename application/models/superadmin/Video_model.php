<?php

class Video_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getVideoBannerData() {
        $data ['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'Videos'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getVideBannerDetail($id) {
        $data ['select'] = ['*'];
        $data ['where'] = ['banner_id' => $id];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editVideBannerDatas($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Video Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getVideoInfoData() {

        $data ['select'] = ['*'];
        $data ['table'] = TABLE_VIDEO;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function addVideoInfoData($postData) {

        $data['insert']['video_name'] = $postData['video_name'];
        $data['insert']['video_link'] = $postData['video_link'];
        $data['insert']['status'] = 'Y';
        $data['insert']['dt_added'] = DATE_TIME;
        $data['insert']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_VIDEO;
        $results = $this->insertRecord($data);

        if ($results) {
            return ['success', 'Record Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function getVidData($id) {

        $data ['select'] = ['*'];
        $data ['where'] = ['video_id' => $id];
        $data ['table'] = TABLE_VIDEO;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editVid($postData, $id) {

        $data['update']['video_name'] = $postData['video_name'];
        $data['update']['video_link'] = $postData['video_link'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['where'] = ['video_id' => $id];
        $data['table'] = TABLE_VIDEO;

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Record Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function deleteVideoDetail($id) {

        $data ['where'] = ['video_id' => $id];
        $data ['table'] = TABLE_VIDEO;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function updateStatus($postData) {

        $id = $this->utility->newdecode($postData['id']);
        $val = $postData['val'];

        $data['update']['status'] = $val;
        $data['table'] = TABLE_VIDEO;
        $data['where'] = ['video_id' => $id];
        $results = $this->updateRecords($data);
    }

    public function handleMultiples($postData) {

        $ids = array();

        for ($i = 0; $i < count($postData['id']); $i++) {
            array_push($ids, $this->utility->newdecode($postData['id'][$i]));
        }

        $data['where_in'] = ['video_id' => $ids];
        $data['table'] = TABLE_VIDEO;
        $res = $this->deleteRecords($data);

        if ($res) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

}

?>