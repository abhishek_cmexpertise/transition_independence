<?php

class Empopportunities_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getEmpBannerData() {

        $data['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'Employment Opportunities'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getEmpBannerDetail($id) {

        $data['select'] = ['*'];
        $data ['where'] = ['banner_id' => $id];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editEmpBannerData($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getEmpOportunityData() {

        $data['select'] = ['*'];
        $data ['where'] = ['page_type' => 'Employment Opportunities'];
        $data ['table'] = TABLE_CMS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getEditEmpOportunityDetail($id) {

        $data ['select'] = ['*'];
        $data ['where'] = ['page_id' => $id];
        $data ['table'] = TABLE_CMS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editEmpOportunityData($postData, $id) {

        $data['update']['page_name'] = $postData['banner_title'];
        $data['update']['page_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getInstructorJobData() {

        $data['select'] = ['*'];
        $data ['where'] = ['page_type' => 'Instructor Job Description'];
        $data ['table'] = TABLE_CMS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getEmpInstuctorJobDetail($id) {

        $data['select'] = ['*'];
        $data ['where'] = ['page_id' => $id];
        $data ['table'] = TABLE_CMS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editInstructorData($postData, $id) {

        $data['update']['page_name'] = $postData['banner_title'];
        $data['update']['page_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Instructor Job Description Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getSupervisorJobData() {

        $data['select'] = ['*'];
        $data ['where'] = ['page_type' => 'Supervisor Job Description'];
        $data ['table'] = TABLE_CMS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getSupervisorJobDetail($id) {
        $data['select'] = ['*'];
        $data ['where'] = ['page_id' => $id];
        $data ['table'] = TABLE_CMS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editSupervisorData($postData, $id) {

        $data['update']['page_name'] = $postData['banner_title'];
        $data['update']['page_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Supervisor Job Description Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

}

?>
