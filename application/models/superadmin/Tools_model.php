<?php

class Tools_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getSettingsData() {
        $data['select'] = ['*'];
        $data ['table'] = TABLE_SETTINGS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editSettings($postData) {

        $datas ['update']['config_val'] = $postData['facebook_link'];
        $datas ['where'] = ['id' => 1];
        $datas ['table'] = TABLE_SETTINGS;
        $this->updateRecords($datas);

        unset($data);

        $datas ['update']['config_val'] = $postData['twitter_link'];
        $datas ['where'] = ['id' => 2];
        $datas ['table'] = TABLE_SETTINGS;
        $this->updateRecords($datas);
        unset($data);

        $datas ['update']['config_val'] = $postData['googleplus_link'];
        $datas ['where'] = ['id' => 3];
        $datas ['table'] = TABLE_SETTINGS;
        $this->updateRecords($datas);
        unset($data);

        $datas ['update']['config_val'] = $postData['pinterest_link'];
        $datas ['where'] = ['id' => 4];
        $datas ['table'] = TABLE_SETTINGS;
        $this->updateRecords($datas);
        unset($data);

        $datas ['update']['config_val'] = $postData['googlemap_link'];
        $datas ['where'] = ['id' => 5];
        $datas ['table'] = TABLE_SETTINGS;
        $this->updateRecords($datas);

        $response['status'] = 'success';
        $response['message'] = 'Record Updated Successfully';

        return $response;
    }

    public function change_pass($postData) {

        $id = $this->session->userdata['valid_login']['id'];
        $data ['select'] = ['admin_password'];
        $data ['table'] = TABLE_ADMIN;
        $data ['where'] = ['admin_id' => $id];
        $result = $this->selectRecords($data);

        if ($result[0]->admin_password == md5($postData['oldpass'])) {

            unset($data);
            $data ['update']['admin_password'] = md5($postData['newpass']);
            $data ['table'] = TABLE_ADMIN;
            $data ['where'] = ['admin_id' => $id];
            $results = $this->updateRecords($data);

            $jsone_response['status'] = 'success';
            $jsone_response['message'] = 'Password updated successfully';
            return $jsone_response;
        } else {

            $jsone_response['status'] = 'danger';
            $jsone_response['message'] = 'Old password does not matched';
            return $jsone_response;
        }
    }


}

?>
