<?php

class Artists_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getArtistsBannerData() {
        $data ['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'Artists'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getArtistBannerDetail($id) {
        $data ['select'] = ['*'];
        $data ['where'] = ['banner_id' => $id];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editArtistBannerDatas($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Artist Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getArtistInfoData() {

        $data ['select'] = ['*'];
        $data ['table'] = TABLE_ARTISTS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function addArtistData($postData) {

        $uploadPath = 'public/assets/cms/upload/artist_image/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
        $config['file_name'] = trim($_FILES['bigimg']['name']);

        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('bigimg')) {
            $result = $this->upload->data();

            $data['insert']['artist_name'] = $postData['artist_name'];
            $data['insert']['artist_age'] = $postData['artist_age'];
            $data['insert']['artist_desc'] = $postData['artist_desc'];
            $data['insert']['artist_image'] = 'upload/artist_image/' . $result['file_name'];
            $data['insert']['status'] = 'Y';
            $data['insert']['dt_added'] = DATE_TIME;
            $data['insert']['dt_updated'] = DATE_TIME;
            $data['table'] = TABLE_ARTISTS;
            $results = $this->insertRecord($data);
        }

        if ($results) {
            return ['success', 'Artist Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function getArtistDetail($id) {

        $data ['select'] = ['*'];
        $data ['where'] = ['artist_id' => $id];
        $data ['table'] = TABLE_ARTISTS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editArtistDatas($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/artist_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['artist_image'] = 'upload/artist_image/' . $result['file_name'];
            }
        }

        $data['update']['artist_name'] = $postData['artist_name'];
        $data['update']['artist_age'] = $postData['artist_age'];
        $data['update']['artist_desc'] = $postData['artist_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['where'] = ['artist_id' => $id];

        $data['table'] = TABLE_ARTISTS;
        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Artist Details Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function deleteTestimonialDetail($id) {

        $data ['where'] = ['artist_id' => $id];
        $data ['table'] = TABLE_ARTISTS;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function updateStatus($postData) {

        $id = $this->utility->newdecode($postData['id']);
        $val = $postData['val'];

        $data['update']['status'] = $val;
        $data['table'] = TABLE_ARTISTS;
        $data['where'] = ['artist_id' => $id];
        $results = $this->updateRecords($data);
    }

    public function handleMultiples($postData) {

        $ids = array();

        for ($i = 0; $i < count($postData['id']); $i++) {
            array_push($ids, $this->utility->newdecode($postData['id'][$i]));
        }

        $data['where_in'] = ['artist_id' => $ids];
        $data['table'] = TABLE_ARTISTS;
        $res = $this->deleteRecords($data);

        if ($res) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function getArtistImageData($id) {

        $data ['select'] = ['*'];
        $data ['where'] = ['from_table_id' => $id];
        $data ['table'] = TABLE_ALL_PHOTOS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function deleteArtistImage($id) {

        $data ['where'] = ['image_id' => $id];
        $data ['table'] = TABLE_ALL_PHOTOS;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Image Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function addimg($postData, $id) {

        $uploadPath = 'public/assets/cms/upload/allphoto_gallery/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
        $config['file_name'] = trim($_FILES['bigimg']['name']);

        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('bigimg')) {
            $result = $this->upload->data();

            $data['insert']['from_table_id'] = $id;
            $data['insert']['image_type'] = 'Artists';
            $data['insert']['image_link'] = 'upload/allphoto_gallery/' . $result['file_name'];
            $data['insert']['status'] = 'Y';
            $data['insert']['dt_added'] = DATE_TIME;
            $data['insert']['dt_updated'] = DATE_TIME;
            $data['table'] = TABLE_ALL_PHOTOS;
            $results = $this->insertRecord($data);
        }

        if ($results) {
            return ['success', 'Image Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

}

?>