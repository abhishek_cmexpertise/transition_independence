<?php

class Home_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getBannerData() {
        $data['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'Home'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function addBannerData($postData) {

        $uploadPath = 'public/assets/cms/upload/banner_image/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
        $config['file_name'] = trim($_FILES['bigimg']['name']);

        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('bigimg')) {
            $result = $this->upload->data();

            $data['insert']['banner_type'] = 'Home';
            $data['insert']['banner_title'] = $postData['banner_title'];
            $data['insert']['banner_desc'] = $postData['banner_desc'];
            $data['insert']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            $data['insert']['status'] = 'Y';
            $data['insert']['sort_no'] = '0';
            $data['insert']['dt_added'] = DATE_TIME;
            $data['insert']['dt_updated'] = DATE_TIME;
            $data['table'] = TABLE_BANNES;
            $results = $this->insertRecord($data);
        }

        if ($results) {
            return ['success', 'Banner Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function getBannerDetail($id) {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editBannerData($postData, $id) {

        if ($_FILES) {

            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['banner_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function deleteBanner($id) {

        $data ['where'] = ['banner_id' => $id];
        $data ['table'] = TABLE_BANNES;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Banner Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function updateStatus($postData) {

        $id = $this->utility->newdecode($postData['id']);
        $val = $postData['val'];

        $data['update']['status'] = $val;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];
        $results = $this->updateRecords($data);
    }

    public function handleMultiples($postData) {

        $ids = array();

        for ($i = 0; $i < count($postData['id']); $i++) {
            array_push($ids, $this->utility->newdecode($postData['id'][$i]));
        }

        $data['where_in'] = ['banner_id' => $ids];
        $data['table'] = TABLE_BANNES;
        $res = $this->deleteRecords($data);

        if ($res) {
            $response = ['success', 'Banner Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    // Manage Home Text

    public function getHomeText() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'Home'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getHomeEditText($id) {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_id' => $id];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editHomeText($postData, $id) {

        $data['update']['page_name'] = $postData['banner_title'];
        $data['update']['page_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] =  DATE_TIME;
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_id' => $id,'page_type' => 'Home'];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Home Text Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

}

?>
