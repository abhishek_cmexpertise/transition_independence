<?php

class Helpfulllink_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getLinkData() {
        $data['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'Helpful Links'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getEditBannerDetail($id) {
        $data['select'] = ['*'];
        $data ['where'] = ['banner_id' => $id];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editHelpfulllinkBannerData($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Helpfull Link Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getHelpfullLinkList() {

        $data['select'] = ['*'];
        $data ['table'] = TABLE_LINK;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function addHelpfulLink($postData) {

        $data['insert']['helpful_link_name'] = $postData['link_name'];
        $data['insert']['helpful_link'] = $postData['link'];
        $data['insert']['helpful_link_desc'] = $postData['banner_desc'];
        $data['insert']['status'] = 'Y';
        $data['insert']['dt_added'] = DATE_TIME;
        $data['insert']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_LINK;
        $results = $this->insertRecord($data);

        if ($results) {
            return ['success', 'Link Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function getEditLinkDetail($id) {
        $data['select'] = ['*'];
        $data['where'] = ['helpful_link_id' => $id];
        $data ['table'] = TABLE_LINK;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editHelpfulLinks($postData, $id) {

        $data['update']['helpful_link_name'] = $postData['link_name'];
        $data['update']['helpful_link'] = $postData['link'];
        $data['update']['helpful_link_desc'] = $postData['banner_desc'];
        $data['update']['status'] = 'Y';
        $data['update']['dt_updated'] = DATE_TIME;
        $data['where'] = ['helpful_link_id' => $id];
        $data['table'] = TABLE_LINK;
        $results = $this->updateRecords($data);

        if ($results) {
            return ['success', 'Link Edit Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function deleteLinkDetail($id) {

        $data ['where'] = ['helpful_link_id' => $id];
        $data ['table'] = TABLE_LINK;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Link Detail Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function updateStatus($postData) {

        $id = $this->utility->newdecode($postData['id']);
        $val = $postData['val'];

        $data['update']['status'] = $val;
        $data['table'] = TABLE_LINK;
        $data['where'] = ['helpful_link_id' => $id];
        $results = $this->updateRecords($data);
    }

    public function handleMultiples($postData) {

        $ids = array();

        for ($i = 0; $i < count($postData['id']); $i++) {
            array_push($ids, $this->utility->newdecode($postData['id'][$i]));
        }

        $data['where_in'] = ['helpful_link_id' => $ids];
        $data['table'] = TABLE_LINK;
        $res = $this->deleteRecords($data);

        if ($res) {
            $response = ['success', 'Link Details Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

}

?>