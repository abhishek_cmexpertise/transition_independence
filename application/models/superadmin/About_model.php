<?php

class About_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function getBannerData() {
        $data['select'] = ['*'];
        $data ['where'] = ['banner_type' => 'About'];
        $data ['table'] = TABLE_BANNES;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getAboutBannerDetail($id) {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editAboutBannerData($postData, $id) {

        if ($_FILES) {
            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['banner_image'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['banner_title'] = $postData['banner_title'];
        $data['update']['banner_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'About Banner Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getAboutTextData() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'About'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function getAboutTextDetail($id) {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_id' => $id];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editAboutTextData($postData, $id) {

        $data['update']['page_name'] = $postData['banner_title'];
        $data['update']['page_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'About Text Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function getAboutWriterData() {

        $data['select'] = ['*'];
        $data ['table'] = TABLE_ABOUT_WRITERS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function addWriterData($postData) {

        $uploadPath = 'public/assets/cms/upload/banner_image/';
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
        $config['file_name'] = trim($_FILES['bigimg']['name']);

        $this->load->library('upload');
        $this->upload->initialize($config);

        if ($this->upload->do_upload('bigimg')) {
            $result = $this->upload->data();

            $data['insert']['writer_name'] = $postData['writer_name'];
            $data['insert']['writer_designation'] = $postData['writer_desigmation'];
            $data['insert']['writer_pic'] = 'upload/banner_image/' . $result['file_name'];
            $data['insert']['writer_phone'] = $postData['writer_phone'];
            $data['insert']['writer_fax'] = $postData['writer_fax'];
            $data['insert']['writer_email'] = $postData['writer_email'];
            $data['insert']['writer_desc'] = $postData['banner_desc'];
            $data['insert']['status'] = 'Y';
            $data['insert']['dt_added'] = DATE_TIME;
            $data['insert']['dt_updated'] = DATE_TIME;
            $data['table'] = TABLE_ABOUT_WRITERS;
            $results = $this->insertRecord($data);
        }

        if ($results) {
            return ['success', 'Writer Added Successfully'];
        } else {
            return ['danger', DEFAULT_MESSAGE];
        }
    }

    public function getAboutEditWriterDetail($id) {

        $data['select'] = ['*'];
        $data ['where'] = ['writer_id' => $id];
        $data ['table'] = TABLE_ABOUT_WRITERS;
        $result = $this->selectRecords($data);
        return $result;
    }

    public function editAboutWriterData($postData, $id) {

        if ($_FILES) {

            $uploadPath = 'public/assets/cms/upload/banner_image/';
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|JPG|PNG';
            $config['file_name'] = trim($_FILES['bigimg']['name']);

            $this->load->library('upload');
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bigimg')) {
                $result = $this->upload->data();
                $data['update']['writer_pic'] = 'upload/banner_image/' . $result['file_name'];
            }
        }

        $data['update']['writer_name'] = $postData['writer_name'];
        $data['update']['writer_designation'] = $postData['writer_desigmation'];
        $data['update']['writer_phone'] = $postData['writer_phone'];
        $data['update']['writer_fax'] = $postData['writer_fax'];
        $data['update']['writer_email'] = $postData['writer_email'];
        $data['update']['writer_desc'] = $postData['banner_desc'];
        $data['update']['dt_updated'] = DATE_TIME;
        $data['table'] = TABLE_ABOUT_WRITERS;
        $data['where'] = ['writer_id' => $id];

        $results = $this->updateRecords($data);

        if ($results) {
            $res = ['success', 'Writer Detail Edit Successfully'];
        } else {
            $res = ['danger', 'No changes performed'];
        }

        return $res;
    }

    public function deleteWriter($id) {

        $data ['where'] = ['writer_id' => $id];
        $data ['table'] = TABLE_ABOUT_WRITERS;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Record Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function updateStatus($postData) {

        $id = $this->utility->newdecode($postData['id']);
        $val = $postData['val'];

        $data['update']['status'] = $val;
        $data['table'] = TABLE_ABOUT_WRITERS;
        $data['where'] = ['writer_id' => $id];
        $results = $this->updateRecords($data);
    }

    public function handleMultiples($postData) {

        $ids = array();

        for ($i = 0; $i < count($postData['id']); $i++) {
            array_push($ids, $this->utility->newdecode($postData['id'][$i]));
        }

        $data['where_in'] = ['writer_id' => $ids];
        $data['table'] = TABLE_ABOUT_WRITERS;
        $res = $this->deleteRecords($data);

        if ($res) {
            $response = ['success', 'Writer Delete Successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

}

?>