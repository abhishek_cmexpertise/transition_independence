<?php

class Account_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'Home', 'status' => 'Y'];
        $data ["order"] = 'banner_id';
        $result = $this->selectRecords($data);
        return $result;
    }

    public function art() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_ARTISTS;
        $data['where'] = ['status' => 'Y', 'status' => 'Y'];
        $data ["order"] = 'artist_id DESC';
        $data ['limit'] = '1';
        $result = $this->selectRecords($data);
        return $result;
    }

    public function art_gallery($id) {
        $data['select'] = ['*'];
        $data['table'] = TABLE_ALL_PHOTOS;
        $data['where'] = ['status' => 'Y', 'image_type' => 'Artists', 'from_table_id' => $id];
        $data ["order"] = 'image_id';
        $data ['limit'] = '4';
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_cms1() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'Home', 'page_id' => '1'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_testimonials() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_TESTIMONIALS;
        $data['where'] = ['status' => 'Y'];
        $data ["order"] = 'testimonial_id DESC';
        $data ['limit'] = '4';
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_patients() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_PATIENTS;
        $data['where'] = ['status' => 'Y'];
        $data ["order"] = 'patient_id';
        $data ['limit'] = '3';
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_cms2() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'Home', 'page_id' => '2'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function latest_video() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_VIDEO;
        $data['where'] = ['status' => 'Y'];
        $data ["order"] = 'video_id DESC';
        $data ['limit'] = '1';
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_cms3() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'Home', 'page_id' => '3'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function loginCheck($postData) {
        $this->db->where('username', $postData['login_username']);
        $this->db->where('user_type', $postData['login_type']);
        $row = $this->db->get(TABLE_USER)->row_array();

        if (!empty($row)) {
            /* Set Session */
            if ($row['password'] == md5($postData['login_password']) && $row['status'] == 'Y') {

                $sessionData['valid_login'] = [
                    'id' => $row['user_id'],
                    'email' => $row['user_email'],
                    'firstname' => $row['first_name'],
                    'lastname' => $row['last_name'],
                    'Type' => $row['user_type'],
                ];

                $this->session->set_userdata($sessionData);

                delete_cookie("username");
                delete_cookie("password");
                if ($this->input->post('remember') == 'true') {

                    $userName = array(
                        'name' => 'username',
                        'value' => $row['email'],
                        'expire' => '86500',
                        'prefix' => '',
                        'secure' => FALSE
                    );
                    $this->input->set_cookie($userName);

                    $password = array(
                        'name' => 'password',
                        'value' => $row['password'],
                        'expire' => '86500',
                        'prefix' => '',
                        'secure' => FALSE
                    );
                    $this->input->set_cookie($password);
                }

                if ($row['user_type'] == 'Super Admin') {
                    $url = admin_url() . 'dashboard';
                } else if ($row['user_type'] == 'Supervisor') {
                    $url = supervisor_url() . 'dashboard';
                } else if ($row['user_type'] == 'Employee') {
                    $url = employee_url() . 'dashboard';
                }

                $json_response['status'] = 'success';
                $json_response['message'] = 'Well done Login Successfully Done';
                $json_response['redirect'] = $url;
            } else if ($row['password'] != md5($postData['login_password'])) {
                /* Check Password Match */
                $json_response['status'] = 'error';
                $json_response['message'] = 'Password does not match';
            } else if ($row['status'] == 'Y') {
                /* Check User is approve or not */
                $json_response['status'] = 'warning';
                $json_response['message'] = 'Your Account is not yet approve by admin';
            }
        } else {
            /* User name and passwod does not match */
            $json_response['status'] = 'error';
            $json_response['message'] = 'Email address and password does not match';
        }
        return $json_response;
    }

    function verifyOldPass() {
        if ($this->input->get_post("check_pass")) {
            $data['table'] = TABLE_USER;
            $data['where'] = ['password' => md5($this->input->get_post("check_pass"))];
            $data['select'] = ['*'];
            $cnt = $this->countRecords($data);
            if ((($cnt) && ($this->input->get_post("flag") == '1')) || (($cnt == 0) && ($this->input->get_post("flag") == '2'))) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Well done password Successfully Changed';
                return true;
            }
        }
        return false;
    }

    function changepassword($postdata) {
        $oldpassword = $postdata['currpass'];
        $this->db->where('user_id', $this->session->userdata['valid_login']['id']);
        $this->db->where('password', md5($postdata['currpass']));
        $row = $this->db->get(TABLE_USER)->row_array();
        if ($row['password'] == md5($postdata['currpass'])) {
            if ($postdata['currpass'] != $postdata['newpass']) {
                $data['where'] = ['user_id' => $this->session->userdata['valid_login']['id']];
                $data['update']['password'] = md5($postdata['newpass']);
                $data['update']['fresh_password'] = $postdata['newpass'];
                $data['update']['old_password'] = $oldpassword;
                $data['table'] = TABLE_USER;
                $response = $this->updateRecords($data);
                return $response;
            }
        }
    }

    function AddContactUs($postData) {
        $phone_no = $postData['celltxt1'] . '-' . $postData['celltxt2'] . '-' . $postData['celltxt3'];
        $insertcontactus = array(
            'first_name' => $postData['user_firstname'],
            'last_name' => $postData['user_lastname'],
            'user_email' => $postData['user_email'],
            'phone_no' => $phone_no,
            'user_message' => $postData['user_message'],
            'dt_created' => date('Y-m-d H:i:s')
        );
        $data['insert'] = $insertcontactus;
        $data['table'] = TABLE_CONTACT_US;
        $response = $this->insertRecord($data);

        if ($response) {
            $message = "<html>
                                <head>
                                <title> Contact Us </title>
                                </head>
                                <body>
                                <h2> Contact Us </h2>
                                <p> Name: " . $postData['user_firstname'] . " </p>
                                <p> Phone No: " . $phone_no . " </p>
                                <p> Email: " . $postData['user_email'] . " </p>
                                <p> Message: " . $postData['user_message'] . " </p>
                                </body>
                                </html>";
            $data1['subject'] = 'Herboglobal';
            $data1['message'] = $message;
            $asd = sendMail($data1);
            if ($asd) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Contact Email Send Successfully';
                $json_response['redirect'] = SITE_URL;
            }
        }
        return $json_response;
    }
}

?>
