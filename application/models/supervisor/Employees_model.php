<?php

class Employees_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function deleteEmployees($id) {

        $data ['where'] = ['user_id' => $id];
        $data ['table'] = TABLE_USER;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Employees delete successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function addEmployees($postdata) {
//           print_r($postdata);

        $username = $this->generate_username();
        $password = $this->generate_password(8, 3);
        $dataemployee = array(
            "user_type" => 'Employee',
            "username" => $username,
            "password" => md5($password),
            "fresh_password" => $password,
            "first_name" => $postdata['first_name'],
            "middle_name" => $postdata['middle_name'],
            "last_name" => $postdata['last_name'],
            "user_email" => $postdata['user_email'],
            "dob" => date('Y-m-d', strtotime($postdata['dob'])),
            "social_security_no" => $postdata['social_security_no'],
        );

        $data['insert'] = $dataemployee;
        $data['table'] = TABLE_USER;
        $response = $this->insertRecord($data);

      

        $config = Array(
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.gmail.com',
        'smtp_port' => 465,
        'smtp_user' => 'digvijay.cmexpertise@gmail.com',
        'smtp_pass' => 'cm@@123#',
        'mailtype' => 'html',
        'charset' => 'iso-8859-1',
        'wordwrap' => TRUE
    );
        
         $email_message = "Congrats!!! You have successfully registered in Transition to Independence as Employee.<br/>
                          Your Login Username :".$username."<br/>
                          Your Login Password :".$password."<br/>
                          To change your password: You can change your password once you login";
    $CI = &get_instance();

    $CI->load->library('email', $config);
    $CI->email->initialize($config);
    $CI->email->set_newline("\r\n");
    $CI->email->from('maulik.cmexpertise@gmail.com', 'Transitiontoindependence');
    $CI->email->to('maulik.cmexpertise@gmail.com,'.$postdata['user_email']);
    $CI->email->subject("Transition to Independence Password Generation For Employee Registration.");
    $CI->email->message($email_message);

    if ($CI->email->send()) {
          if ($response) {
            $response = ['success', 'Employee Added successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
        return true;
    } else {
        return FALSE;
    }
    }

    public function editEmployees($id) {
        $data['select'] = ['*'];
        $data ['where'] = ['user_id' => $id];
        $data ['table'] = TABLE_USER;
        $singleemployeedata = $this->selectRecords($data);
        return $singleemployeedata;
    }

    public function updateEmployees($postdata) {

        $dataemployee = array(
            "password" => md5($postdata['fresh_password']),
            "fresh_password" => $postdata['fresh_password'],
            "first_name" => $postdata['first_name'],
            "middle_name" => $postdata['middle_name'],
            "last_name" => $postdata['last_name'],
            "user_email" => $postdata['user_email'],
            "dob" => date('Y-m-d', strtotime($postdata['dob'])),
            "social_security_no" => $postdata['social_security_no'],
            "dt_updated" => DATE_TIME,
        );


        $data['update'] = $dataemployee;
        $data['table'] = TABLE_USER;
        $data['where'] = ['user_id' => $postdata['user_id']];
        $response = $this->updateRecords($data);
        if ($response) {
            $response = ['success', 'Employee Edit successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function generate_password($length, $complex) {
        $min = "abcdefghijklmnopqrstuvwxyz";
        $num = "0123456789";
        $maj = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $symb = "!@#$%^&*()_-=+;:,.?";
        $chars = $min;
        if ($complex >= 2) {
            $chars .= $num;
        }
        if ($complex >= 3) {
            $chars .= $maj;
        }
        if ($complex >= 4) {
            $chars .= $symb;
        }
        $password = substr(str_shuffle($chars), 0, $length);
        return $password;
    }

    public function generate_username() {
        $first_letter = strtolower($this->input->post('first_name')[0]);
        $last_name_add = strtolower($this->input->post('last_name'));
        $yeartwo_digit = date('y', strtotime($this->input->post['dob']));
        $monthtwo_digit = date('m', strtotime($this->input->post['dob']));
        $now_username = $first_letter . $last_name_add . $monthtwo_digit . $yeartwo_digit;

        $data ['where'] = ['username' => $now_username];
        $data ['table'] = TABLE_USER;
        $username = $this->countRecords($data);
        if ($username > 0) {
            $num = $username;

            $final_username = $now_username . "-" . $num;
        } else {
            $final_username = $now_username;
        }
        return $final_username;
    }

    function verifyemail() {
        if ($this->input->get_post("check_email")) {
            $data['table'] = TABLE_USER;
            $data['where'] = ['user_email' => $this->input->get_post("check_email")];
            $data['select'] = ['*'];
            $cnt = $this->countRecords($data);
            if ($cnt) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Well done password Successfully Changed';
                return false;
            }
        }
        return true;
    }

}

?>
