<?php

class Monthyrecape_model extends My_model {

    public function __construct() {
        parent::__construct();
    }
        
    public function genarateMonthlyreport($postData)
    {
        $month = $postData['month'];
        $year = $postData['year'];
        $timestamp    = strtotime($month." ".$year);
        $first_date = date('Y-m-01', $timestamp);
	$last_date  = date('Y-m-t', $timestamp);
	$start_day = 1;
	$end_day = date('t', $timestamp);
        
        $data['select'] = ['*'];
        $data['where'] = ['status'=> 'Y'];
        $data['table'] = TABLE_CLIENT;     
       $result =  $this->selectRecords($data);
       
       unset($data);
       foreach($result as $results)
       {
         for($i=$start_day;$i<=$end_day;$i++)
	{
       $selected_date = $year."-".date('m',strtotime($month))."-".sprintf("%02d", $i);
       $data['select'] = ['*'];
       $data['where'] = ['shiftlog_date'=>$selected_date,'client_id'=> $results->client_id];
       $data['table'] = TABLE_SHIFTLOG;
       $shiftlogdata[$i] = $this->selectRecords($data);
       }
        }
      
       return array( $result,'start_day'=>$start_day,'end_day'=>$end_day,$shiftlogdata);
    }
}

?>
