<?php

class Miscellaneousfile_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function deleteMiscellaneousfile($id) {

        $data['where'] = ['FILE_ID' => $id];
        $data['table'] = TABLE_MISCELLANEOUS_FILE;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Miscellaneous file delete successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function addMiscellaneousfile() {

       
            $file_ext = explode('.', $_FILES['files']['name']);
            $newfile_name = $file_ext[0];
            $file_showcase_name = str_replace("_", " ", $newfile_name);
            $file_extn = $file_ext[1];
            if (strpos($newfile_name, ' ') > 0) {
                echo $newfile_name1 = str_replace(" ", "_", $newfile_name);
            } else {
                echo $newfile_name1 = $newfile_name;
            }
            $miscellaneousfiledata = array(
                "FILE_NAME" => $newfile_name,
                "FILE_SHOWCASE_NAME" => $newfile_name,
                "FILE_SIZE" => $_FILES['files']['size'],
                "FILE_TYPE" => $_FILES['files']['type'],
                "FILE_EXT" => $file_extn,
            );

            $this->load->library('upload', $config);
             $uploadPath = 'public/assets/miscellaneousfile/';

           $path = $uploadPath;
            $image = upload_file_commonaly($_FILES, $_FILES['files']['name'], $path);
            
            $data['insert'] = $miscellaneousfiledata;
            $data['table'] = TABLE_MISCELLANEOUS_FILE;
            $response = $this->insertRecord($data);

            if ($response) {
                $response = ['success', 'Miscellaneous file added successfully'];
            } else {
                $response = ['danger', DEFAULT_MESSAGE];
            }
            return $response;
        }
    }



?>
