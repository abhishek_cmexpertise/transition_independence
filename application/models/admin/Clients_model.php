<?php

class Clients_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function deleteClients($id) {

        $data ['where'] = ['client_id' => $id];
        $data ['table'] = TABLE_CLIENT;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Client delete successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function addClients($postdata) {
        $dataclient = array(
            "client_uci" => $postdata['client_uci'],
            "consumer_name" => $postdata['consumer_name'],
            "address1" => $postdata['address1'],
            "address2" => $postdata['address2'],
            "client_dob" => date('Y-m-d', strtotime($postdata['client_dob'])),
            "emergency_contact" => $postdata['emergency_contact'],
            "disability" => $postdata['disability'],
            "manager_name" => $postdata['manager_name'],
            "manager_phone" => $postdata['manager_phone'],
            "allocated_hours" => $postdata['allocated_hours'],
            "remaining_hours" => $postdata['allocated_hours'],
        );

        $data['insert'] = $dataclient;
        $data['table'] = TABLE_CLIENT;
        $response = $this->insertRecord($data);

        if ($response) {
            $response = ['success', 'Client added successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

    public function editClients($id) {
        $data['select'] = ['*'];
        $data ['where'] = ['client_id' => $id];
        $data ['table'] = TABLE_CLIENT;
        $singleclientdata = $this->selectRecords($data);
        return $singleclientdata;
    }

    public function updateClients($postdata) {

        $dataclient = array(
            "client_uci" => $postdata['client_uci'],
            "consumer_name" => $postdata['consumer_name'],
            "address1" => $postdata['address1'],
            "address2" => $postdata['address2'],
            "client_dob" => date('Y-m-d', strtotime($postdata['client_dob'])),
            "emergency_contact" => $postdata['emergency_contact'],
            "disability" => $postdata['disability'],
            "manager_name" => $postdata['manager_name'],
            "manager_phone" => $postdata['manager_phone'],
            "allocated_hours" => $postdata['allocated_hours'],
            "remaining_hours" => $postdata['allocated_hours'],
            "dt_updated" => DATE_TIME,
        );

        $data['update'] = $dataclient;
        $data['table'] = TABLE_CLIENT;
        $data['where'] = ['client_id' => $postdata['client_id']];
        $response = $this->updateRecords($data);
        if ($response) {
            $response = ['success', 'Client edit successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }

}

?>
