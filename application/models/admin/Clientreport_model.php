<?php

class Clientreport_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function clientreportdata($postdata) {

        $data['select'] = ['*'];
        $data ['table'] = TABLE_CLIENT_REPORT . ' as cr';
        $data['join'] = [
            TABLE_CLIENT . ' as c' => [
                "c.client_id = cr.client_id"
            ],
            TABLE_USER . ' as u' => [
                "u.user_id = cr.employee_id"
            ],
        ];
        $data ['where'] = ['cr.report_id' => $postdata];
        $singleclientreport = $this->selectFromJoin($data);

        return $singleclientreport;
    }

    public function clientreportdatas($postData) {
        $idArray = array();
        for ($i = 0; $i < count(@$postData['checkedId']); $i++) {

            $ids = $this->utility->newdeCode($postData['checkedId'][$i]);
            if (!ctype_digit($ids)) {
                $this->utility->setFlashMessage('danger', DEFAULT_MESSAGE);
                redirect(admin_url() . 'clientreport');
            }
            $idArray[] = $ids;
        }

        $data['select'] = ['*'];
        $data['table'] = TABLE_CLIENT_REPORT . ' as cr';
        $data['join'] = [
            TABLE_CLIENT . ' as c' => [
                "c.client_id = cr.client_id",'LEFT'
            ],
            TABLE_USER . ' as u' => [
                "u.user_id = cr.employee_id",'LEFT'
            ],
        ];
        $data['where_in'] = ['cr.report_id' => $idArray];
        $clientreports = $this->selectFromJoin($data, TRUE);

        return @$clientreports;
    }

}

?>
