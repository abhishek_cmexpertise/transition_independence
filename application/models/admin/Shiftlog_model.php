<?php

class Shiftlog_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function deleteShiftlog($id) {
        
        $data ['where'] = ['shiftlog_id' => $id];
        $data ['table'] = TABLE_SHIFTLOG;
        $response = $this->deleteRecords($data);

        if ($response) {
            $response = ['success', 'Shiftlog delete successfully'];
        } else {
            $response = ['danger', DEFAULT_MESSAGE];
        }
        return $response;
    }
    
    public function getEmployeedata()
    {
        $data['select'] = ['user_id','first_name','middle_name','last_name'];
        $data['table'] = TABLE_USER;
        $data['order'] = 'first_name ASC';
       $data['where'] = ['user_type'=> 'Employee'];
        $result = $this->selectRecords($data);
        return $result; 
    }
    
    public function getclient()
    {
        $data['select'] = ['client_id','consumer_name'];
        $data['table'] = TABLE_CLIENT;
        $data['order'] = 'consumer_name ASC';
        $result = $this->selectRecords($data);
        return $result; 
    }

    public function shiftlogdatas()
    {
          $data['select']=['sv.start_time','sv.end_time','sv.total_time','sv.travel_time','sv.consumer_details','s.shiftlog_date','s.total_hours','s.total_travel_time','c.consumer_name','u.first_name','u.middle_name','u.last_name']; 
        $data ['table'] = TABLE_SHIFTLOG_VISIT.' as sv';
       $data['join'] = [
             TABLE_SHIFTLOG.' as s' => [
                "s.shiftlog_id = sv.shiftlog_id",'LEFT'
            ],

             TABLE_USER.' as u' => [
                "u.user_id = s.staff_id",'LEFT'
            ],
             TABLE_CLIENT.' as c' => [
                "c.client_id = s.client_id",'LEFT'
            ],
           ];
        $shiftlogdata = $this->selectFromJoin($data);
        return $shiftlogdata;
    }

    public function shiftlogview($postdata)
    {   
          $data['select']=['sv.start_time','sv.end_time','sv.total_time','sv.travel_time','sv.consumer_details','s.shiftlog_date','s.total_hours','s.total_travel_time','c.consumer_name','u.first_name','u.middle_name','u.last_name']; 
        $data ['table'] = TABLE_SHIFTLOG_VISIT.' as sv';
       $data['join'] = [
             TABLE_SHIFTLOG.' as s' => [
                "s.shiftlog_id = sv.shiftlog_id"
            ],

             TABLE_USER.' as u' => [
                "u.user_id = s.staff_id"
            ],
             TABLE_CLIENT.' as c' => [
                "c.client_id = s.client_id"
            ],
           ];
         $data ['where'] = ['sv.shiftlog_id' => $postdata];
        $singleshiftlogdata = $this->selectFromJoin($data);
        return $singleshiftlogdata;
    } 
 }
?>
