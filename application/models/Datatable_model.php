<?php

class Datatable_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    function getSupervisorsData() {
        $this->datatables->select('user_id,username,first_name,last_name,user_email,dob,social_security_no');
        $this->datatables->from(TABLE_USER);
        $this->datatables->where('user_type', 'Supervisor');
        $result = $this->datatables->generate();
        $records = (array) json_decode($result);

        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            $editUrl = admin_url() . 'supervisors/editSupervisor/' . $id;
            $deleteUrl = admin_url() . 'supervisors/delete/' . $id;
            $records["data"][$i][0] = $j;
            $records["data"][$i][5] = date('m/d/Y', strtotime($records["data"][$i][5]));
            $records["data"][$i][7] = '<a href="' . $editUrl . '" data-tooltip="Edit"><i class="fas fa-edit action_icons2 font_s icon-color" aria-hidden="true"></i></a>
                                       <a href="javascript:;" data-tooltip="Delete" class="deleteClient" data-href="' . $deleteUrl . '"><i class="fa fa-times action_icons font_s btn-light icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }

    function getEmployeesData() {
        $usertype = $this->session->userdata['valid_login']['Type'];

        if ($usertype == 'Super Admin') {
            $this->datatables->select('user_id,username,first_name,last_name,user_email,dob,social_security_no');
            $this->datatables->from(TABLE_USER);
            $this->datatables->where('user_type', 'Employee ');
            $result = $this->datatables->generate();
            $records = (array) json_decode($result);

            $j = intval($this->input->post("start"));
            for ($i = 0; $i < count($records['data']); $i++) {
                $j++;
                $id = $this->utility->newenCode($records["data"][$i][0]);
                $editUrl = admin_url() . 'employees/editEmployees/' . $id;
                $deleteUrl = admin_url() . 'employees/delete/' . $id;
                $records["data"][$i][0] = $j;
                $records["data"][$i][5] = date('m/d/Y', strtotime($records["data"][$i][5]));
                $records["data"][$i][7] = '<a href="' . $editUrl . '" data-tooltip="Edit"><i class="fas fa-edit action_icons2 font_s icon-color" aria-hidden="true"></i></a>
                                                     <a href="javascript:;" data-tooltip="Delete" class="deleteClient" data-href="' . $deleteUrl . '"><i class="fa fa-times action_icons font_s btn-light icon-color" aria-hidden="true"></i></a>';
            }
        } else if ($usertype == 'Supervisor') {
            $this->datatables->select('user_id,username,first_name,last_name,user_email,dob,md5(social_security_no)');
            $this->datatables->from(TABLE_USER);
            $this->datatables->where('user_type', 'Employee ');
            $result = $this->datatables->generate();
            $records = (array) json_decode($result);

            $j = intval($this->input->post("start"));
            for ($i = 0; $i < count($records['data']); $i++) {
                $j++;
                $id = $this->utility->newenCode($records["data"][$i][0]);
                $editUrl = supervisor_url() . 'employees/editEmployees/' . $id;
                $deleteUrl = supervisor_url() . 'employees/delete/' . $id;
                $records["data"][$i][0] = $j;
                   $records["data"][$i][5] = date('m/d/Y', strtotime($records["data"][$i][5]));
                $records["data"][$i][7] = '<a href="' . $editUrl . '" data-tooltip="Edit"><i class="fas fa-edit action_icons2 font_s icon-color" aria-hidden="true"></i></a>
                                                       <a href="javascript:;" data-tooltip="Delete" class="deleteClient" data-href="' . $deleteUrl . '"><i class="fa fa-times action_icons font_s btn-light icon-color" aria-hidden="true"></i></a>';
            }
        }
        return $records;
    }

    function getClientsData() {

        $this->datatables->select('client_id,client_uci,consumer_name,address1,client_dob,allocated_hours,remaining_hours,manager_name,manager_phone');
        $this->datatables->from(TABLE_CLIENT);
        $result = $this->datatables->generate();
        $records = (array) json_decode($result);

        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            $usertype = $this->session->userdata['valid_login']['Type'];
            if ($usertype == 'Super Admin') {
                $editUrl = admin_url() . 'clients/editClients/' . $id;
                $deleteUrl = admin_url() . 'clients/delete/' . $id;
            } else if ($usertype == 'Supervisor') {
                $editUrl = supervisor_url() . 'clients/editClients/' . $id;
                $deleteUrl = supervisor_url() . 'clients/delete/' . $id;
            }

            $records["data"][$i][0] = $j;
            $records["data"][$i][4] = date('m/d/Y', strtotime($records["data"][$i][4]));
            $records["data"][$i][9] = '<a href="' . $editUrl . '" data-tooltip="Edit"><i class="fas fa-edit action_icons2 font_s icon-color" aria-hidden="true"></i></a>
                                       <a href="javascript:;" data-tooltip="Delete" class="deleteClient" data-href="' . $deleteUrl . '"><i class="fa fa-times action_icons font_s btn-light icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }

    function getShiftlogData($employee, $client, $startDate, $endDate) {

        $this->datatables->select('shiftlog_id,s.shiftlog_date,c.consumer_name,u.first_name,s.total_hours,s.total_travel_time');
        $this->datatables->join(TABLE_CLIENT . ' c', 'c.client_id=s.client_id', 'LEFT');
        $this->datatables->join(TABLE_USER . ' u', 'u.user_id=s.staff_id', 'LEFT');
        $this->datatables->from(TABLE_SHIFTLOG . ' as s');
        if ($employee) {
            $this->datatables->where('s.staff_id', $employee);
        }
        if ($client) {
            $this->datatables->where('s.client_id', $client);
        }
        if ($startDate && $endDate) {
            $this->datatables->where('s.shiftlog_date >=', $startDate);
            $this->datatables->where('s.shiftlog_date <=', $endDate);
        }
        $result = $this->datatables->generate();

        $records = (array) json_decode($result);
//        print_r($records); exit();
        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            $records["data"][$i][0] = $j;

            $usertype = $this->session->userdata['valid_login']['Type'];

            if ($usertype == 'Super Admin') {
                $shiftLogView = admin_url() . 'shiftlog/shiftLogview/' . $id;
                  $shiftLogpdf = admin_url() . 'shiftlog/shiftlogpdfview/' . $id;
                $shiftLogexcel = admin_url() . 'shiftlog/shiftlogexcelview/' . $id;
              
                $deleteUrl = admin_url() . 'shiftlog/delete/' . $id;
                $records["data"][$i][1] = date('m/d/Y',strtotime($records["data"][$i][1]));
                $records["data"][$i][6] = '<a href="' . $shiftLogView . '" data-tooltip="Edit"><i class="fa fa-file-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>'
                        . ' <a href="' . $shiftLogexcel . '" data-tooltip="Edit"><i class="fa fa-file-excel-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>'
                        . ' <a href="' . $shiftLogpdf . '" data-tooltip="Edit"><i class="fa fa-file-pdf-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
                $records["data"][$i][7] = '<a href="javascript:;" data-tooltip="Delete" class="deleteClient" data-href="' . $deleteUrl . '"><i class="fa fa-times action_icons font_s btn-light icon-color" aria-hidden="true"></i></a>';
            } else if ($usertype == 'Supervisor') {
                $shiftLogView = supervisor_url() . 'shiftlog/shiftLogview/' . $id;
                 $shiftLogpdf = supervisor_url() . 'shiftlog/shiftlogpdfview/' . $id;
                $shiftLogexcel = supervisor_url() . 'shiftlog/shiftlogexcelview/' . $id;
                    $records["data"][$i][1] = date('m/d/Y',strtotime($records["data"][$i][1]));
                $records["data"][$i][6] = '<a href="' . $shiftLogView . '" data-tooltip="Edit"><i class="fa fa-file-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>'
                        . ' <a href="' . $shiftLogexcel . '" data-tooltip="Edit"><i class="fa fa-file-excel-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>'
                        . ' <a href="' . $shiftLogpdf . '" data-tooltip="Edit"><i class="fa fa-file-pdf-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
            }
        }
        return $records;
    }

    function getMiscellaneousfileData() {
        $this->datatables->select('FILE_ID,FILE_NAME,FILE_EXT');
        $this->datatables->from(TABLE_MISCELLANEOUS_FILE);
        $result = $this->datatables->generate();
        $records = (array) json_decode($result);

        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            $miscellaneousfiledownload = base_url() . 'public/assets/miscellaneousfile/' . $records["data"][$i][1] . '.' . $records["data"][$i][2];

            $usertype = $this->session->userdata['valid_login']['Type'];
            if ($usertype == 'Super Admin') {
                $deleteUrl = admin_url() . 'miscellaneousfile/delete/' . $id;
            } else if ($usertype == 'Supervisor') {
                $deleteUrl = supervisor_url() . 'miscellaneousfile/delete/' . $id;
            }
            $records["data"][$i][0] = $j;
            $records["data"][$i][2] = '<a href="' . $miscellaneousfiledownload . '" data-tooltip="Edit"><i class="fa fa-download action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
            $records["data"][$i][3] = '<a href="javascript:;" data-tooltip="Delete" class="deleteClient" data-href="' . $deleteUrl . '"><i class="fa fa-times action_icons font_s btn-light icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }

    function getClientreport() {
        $this->datatables->select('sr.report_id,c.consumer_name,c.client_uci,c.client_dob,sr.report_date,u.first_name');
        $this->datatables->join(TABLE_CLIENT . ' c', 'c.client_id=sr.client_id', 'left');
        $this->datatables->join(TABLE_USER . ' u', 'u.user_id=sr.employee_id', 'left');
        $this->datatables->from(TABLE_CLIENT_REPORT . ' as sr');
        $result = $this->datatables->generate();
        $records = (array) json_decode($result);
        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            $usertype = $this->session->userdata['valid_login']['Type'];

            if ($usertype == 'Super Admin') {
                $clientreportexcel = admin_url() . 'Clientreport/clientreportexcel/' . $id;
                $clientreportpdf = admin_url() . 'Clientreport/clientreportpdf/' . $id;
            } else if ($usertype == 'Supervisor') {
                $clientreportexcel = supervisor_url() . 'Clientreport/clientreportexcel/' . $id;
                $clientreportpdf = supervisor_url() . 'Clientreport/clientreportpdf/' . $id;
            }

            $records["data"][$i][0] = '<label class="chkspace">
                        <input type="checkbox" class="selectCheck" id="chk' . $j . '"  value="' . $id . '"><label for="chk' . $j . '"></label>
                     </label>';
            $records["data"][$i][3] = date('m/d/Y', strtotime($records["data"][$i][3]));
            $records["data"][$i][4] = date('m/d/Y', strtotime($records["data"][$i][4]));
            $records["data"][$i][6] = ' <a href="' . $clientreportexcel . '" data-tooltip="Edit" target="_blank"><i class="fa fa-file-excel-o action_icons2 font_s icon-color" aria-hidden="true "></i></a>'
                    . ' <a href="' . $clientreportpdf . '" data-tooltip="Edit"><i class="fa fa-file-pdf-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }

    function getEmployeetimesheet() {
        $this->datatables->select('er.report_id,er.submission_date,u.first_name,er.report_month,er.report_year,er.subtotal_hours,er.subtotal_travel_time_google');
        $this->datatables->from(TABLE_USER . ' as u');
        $this->datatables->join(TABLE_EMPLOYEE_REPORT . ' er', 'u.user_id=er.employee_id', 'RIGHT');

        $result = $this->datatables->generate();
        $records = (array) json_decode($result);
        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);

            $usertype = $this->session->userdata['valid_login']['Type'];

            if ($usertype == 'Super Admin') {
                $employeereportView = admin_url() . 'employeetimesheet/employeetimesheetview/' . $id;
                $employeereportexcel = admin_url() . 'employeetimesheet/employeereportexcel/' . $id;
                $employeereportpdf = admin_url() . 'employeetimesheet/employeereportpdf/' . $id;
            } else {
                $employeereportView = supervisor_url() . 'employeetimesheet/employeetimesheetview/' . $id;
                $employeereportexcel = supervisor_url() . 'employeetimesheet/employeereportexcel/' . $id;
                $employeereportpdf = supervisor_url() . 'employeetimesheet/employeereportpdf/' . $id;
            }
            $records["data"][$i][0] = $j;
            $records["data"][$i][1] = date('m/d/Y', strtotime($records["data"][$i][1]));
            $records["data"][$i][7] = '<a href="' . $employeereportView . '" data-tooltip="Edit"><i class="fa fa-file-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>'
                    . ' <a href="' . $employeereportexcel . '" data-tooltip="Edit"><i class="fa fa-file-excel-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>'
                    . ' <a href="' . $employeereportpdf . '" data-tooltip="Edit"><i class="fa fa-file-pdf-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }

    function getEmpClientsData() {

        $this->datatables->select('client_id,client_uci,consumer_name,address1,client_dob,remaining_hours,manager_name,manager_phone');
        $this->datatables->from(TABLE_CLIENT);
        $result = $this->datatables->generate();
        $records = (array) json_decode($result);

        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);

            $usertype = $this->session->userdata['valid_login']['Type'];

            $shiftLogView = employee_url() . 'clients/clientView/' . $id;
            $ILSshiftlog = employee_url() . 'clients/clientshiftlog/' . $id;

            $records["data"][$i][0] = $j;
            $records["data"][$i][4] = date('m/d/Y',strtotime($records["data"][$i][4]));
            $records["data"][$i][8] = '<a href="' . $shiftLogView . '" data-tooltip="Edit"><i class="fa fa-file-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
            $records["data"][$i][9] = '<a href="' . $ILSshiftlog . '"><i class="fa fa-clock-o"></i></a>';
        }
        return $records;
    }

    function getClientlog($postdata) {

        $staffid = $this->session->userdata['valid_login']['id'];
        $this->datatables->select('shiftlog_id,s.shiftlog_date,c.consumer_name,u.first_name,s.total_hours,s.total_travel_time');
        $this->datatables->join(TABLE_CLIENT . ' c', 'c.client_id=s.client_id');
        $this->datatables->join(TABLE_USER . ' u', 'u.user_id=s.staff_id');
        $this->datatables->from(TABLE_SHIFTLOG . ' as s');
        $this->datatables->where('c.client_id=' . $postdata);
        $this->datatables->where('s.staff_id=' . $staffid);
        $result = $this->datatables->generate();
        $records = (array) json_decode($result);
        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            $shiftLogView = employee_url() . 'clients/Showshiftlog/' . $id;
            $records["data"][$i][0] = $j;
            $records["data"][$i][1] = date('m/d/Y', strtotime($records["data"][$i][1]));
            ;
            $records["data"][$i][6] = '<a href="' . $shiftLogView . '" data-tooltip="Edit"><i class="fa fa-file-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }

    function EmpgetMiscellaneousfileData() {
        $this->datatables->select('FILE_ID,FILE_NAME,FILE_EXT');
        $this->datatables->from(TABLE_MISCELLANEOUS_FILE);
        $result = $this->datatables->generate();
        $records = (array) json_decode($result);

        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            $miscellaneousfiledownload = base_url() . 'public/assets/miscellaneousfile/' . $records["data"][$i][1] . '.' . $records["data"][$i][2];

            $records["data"][$i][0] = $j;
            $records["data"][$i][2] = '<a href="' . $miscellaneousfiledownload . '" data-tooltip="Edit"><i class="fa fa-download action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }

    function getLog($postdata) {

        $data['select'] = ['report_month', 'report_year'];
        $data['table'] = TABLE_EMPLOYEE_REPORT;
        $data['where'] = ['report_id' => $postdata];
        $result = $this->selectRecords($data);

        $month = $result[0]->report_month;
        $year = $result[0]->report_year;

        $timestamp = strtotime($month . " " . $year);
        $first_date = date('Y-m-01', $timestamp);
        $last_date = date('Y-m-t', $timestamp);
        $this->datatables->select('s.shiftlog_id,s.shiftlog_date,c.consumer_name,u.first_name,s.total_hours,total_travel_time');
        $this->datatables->join(TABLE_CLIENT . ' c', 'c.client_id = s.client_id', 'left');
        $this->datatables->join(TABLE_USER . ' u', 'u.user_id = s.staff_id', 'left');
        $this->datatables->from(TABLE_SHIFTLOG . ' as s');
        $this->datatables->where('s.shiftlog_date >= ', $first_date);
        $this->datatables->where('s.shiftlog_date <= ', $last_date);
        $results = $this->datatables->generate();
        $recordss = (array) json_decode($results);

        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($recordss['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($recordss["data"][$i][0]);
            $recordss["data"][$i][0] = $j;
            $recordss["data"][$i][1] = date('m/d/Y', strtotime($recordss["data"][$i][1]));
          }
        return $recordss;
    }

    function clientmonthreport() {
        $userid = $this->session->userdata['valid_login']['id'];
        $this->datatables->select('sr.report_id,c.consumer_name,c.client_uci,c.client_dob,sr.report_date,sr.client_id');
        $this->datatables->join(TABLE_CLIENT_REPORT . ' as sr', 'c.client_id=sr.client_id', 'left');
        $this->datatables->join(TABLE_USER . ' u', 'u.user_id=sr.employee_id', 'left');
        $this->datatables->from(TABLE_CLIENT . ' c');
        $this->datatables->where('u.user_id =', $userid);
        $this->datatables->where('c.client_id = sr.client_id');
        $this->datatables->where('sr.employee_id=', $userid);

        $result = $this->datatables->generate();
        $records = (array) json_decode($result);
        $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            $addclientreport = employee_url() . 'clientreport/addsingleclientreport/' . $id;
            $clientreportexcel = employee_url() . 'clientreport/clientreportexcel/' . $id;
            $clientreportpdf = employee_url() . 'clientreport/clientreportpdf/' . $id;
            $records["data"][$i][0] = '<label class="chkspace">
                        <input type="checkbox" class="selectCheck" id="chk' . $j . '"  value="' . $id . '"><label for="chk' . $j . '"></label>
                     </label>';
            $records["data"][$i][3] = date('m/d/Y', strtotime($records["data"][$i][3]));
            $records["data"][$i][4] = date('m/d/Y',strtotime($records["data"][$i][4]));
            $records["data"][$i][5] = '<a href="' . $addclientreport . '" data-tooltip="Edit"><i class="fa fa-plus" aria-hidden="true"></i>
' . ' <a href="' . $clientreportexcel . '" data-tooltip="Edit"><i class="fa fa-file-excel-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>'
                    . ' <a href="' . $clientreportpdf . '" data-tooltip="Edit"><i class="fa fa-file-pdf-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }

    function manageTimesheet($month, $search_duration, $year) {

        $timestamp = strtotime($month . " " . $year);
        $staffid = $this->session->userdata['valid_login']['id'];
        $this->datatables->select('er.report_id,er.submission_date,er.report_month,er.report_year,er.subtotal_hours,er.subtotal_travel_time_google,report_half');
        $this->datatables->from(TABLE_EMPLOYEE_REPORT . ' as er');
        $this->datatables->where('er.employee_id =', $staffid);
        $this->datatables->where('er.report_half =', $search_duration);
        $this->datatables->where('er.report_year =', $year);
        $result = $this->datatables->generate();
        $records = (array) json_decode($result);
    
         $j = intval($this->input->post("start"));
        for ($i = 0; $i < count($records['data']); $i++) {
            $j++;
            $id = $this->utility->newenCode($records["data"][$i][0]);
            
            $shiftLogexcel = employee_url() . 'employeetimesheet/employeereportexcel/' . $id;
            $shiftLogpdf = employee_url() . 'employeetimesheet/employeereportpdf/' . $id;
            
            if($records["data"][$i][6] == '1'){
                $var = $records["data"][$i][2].'(1st to 15th)';
            }else{
                $var = $records["data"][$i][2].'(16th to end of month)';
            }

            $records["data"][$i][0] = $j;
             $records["data"][$i][1] = date('m/d/Y', strtotime($records["data"][$i][1]));
            $records["data"][$i][2] = $var;
            $records["data"][$i][6] = ' <a href="' . $shiftLogexcel . '" data-tooltip="Edit"><i class="fa fa-file-excel-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>'
                    . ' <a href="' . $shiftLogpdf . '" data-tooltip="Edit"><i class="fa fa-file-pdf-o action_icons2 font_s icon-color" aria-hidden="true"></i></a>';
        }
        return $records;
    }
}
?>
