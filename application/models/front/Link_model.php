<?php

class Link_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function show_banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'Helpful Links', 'banner_id' => '6'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function link() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_LINK;
        $data['where'] = ['status' => 'Y'];
        $data ["order"] = 'helpful_link_id';
        $result = $this->selectRecords($data);
        return $result;
    }

}

?>
