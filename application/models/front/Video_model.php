<?php

class Video_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function show_banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'Videos', 'banner_id' => '11'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function video() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_VIDEO;
        $data['where'] = ['status' => 'Y'];
        $data ["order"] = 'video_id DESC';
        $result = $this->selectRecords($data);
        return $result;
    }

}

?>
