<?php

class Patient_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function show_banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'Patients', 'banner_id' => '10'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function patient() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_PATIENTS;
        $data['where'] = ['status' => 'Y'];
        $data ["order"] = 'patient_id';
        $result = $this->selectRecords($data);
        return $result;
    }

}

?>
