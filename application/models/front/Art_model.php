<?php
class Art_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function show_banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'Artists', 'banner_id' => '9'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function row_art($id) {
        $data['select'] = ['*'];
        $data['table'] = TABLE_ARTISTS;
        $data['where'] = ['artist_id' => $id];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function art_gallery($id) {
        $data['select'] = ['*'];
        $data['table'] = TABLE_ALL_PHOTOS;
        $data['where'] = ['status' => 'Y', 'image_type' => 'Artists', 'from_table_id' => $id];
        $data ["order"] = 'image_id';
        $data ['limit'] = '20';
        $result = $this->selectRecords($data);
        return $result;
    }

}

?>
