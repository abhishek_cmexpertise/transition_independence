<?php

class Contact_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function show_banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'Contact', 'banner_id' => '5'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function contacts() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CONTACTS;
        $data['where'] = ['status' => 'Y'];
        $data ["order"] = 'contact_id';
        $data ['limit'] = '3';
        $result = $this->selectRecords($data);
        return $result;
    }

}

?>
