<?php

class About_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function show_banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'About', 'banner_id' => '4'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_cms() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'About','page_id' => '4'];
        $result = $this->selectRecords($data);
        return $result;
    }
    
    public function about(){
        $data['select'] = ['*'];
        $data['table'] = TABLE_ABOUT_WRITERS;
        $data['where'] = ['status' => 'Y'];
        $data['order'] = 'writer_id';
        $result = $this->selectRecords($data);
        return $result;
    }

}

?>
