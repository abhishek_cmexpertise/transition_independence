<?php

class Testimonial_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function show_banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'Testimonials', 'banner_id' => '8'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function testimonials() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_TESTIMONIALS;
        $data['where'] = ['status' => 'Y'];
        $data ["order"] = 'testimonial_id DESC';
        $result = $this->selectRecords($data);
        return $result;
    }

}

?>
