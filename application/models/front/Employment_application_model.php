<?php

class Employment_application_model extends My_model {

    public function __construct() {
        parent::__construct();
    }

    public function show_banner() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $data['where'] = ['banner_type' => 'Employment Opportunities', 'banner_id' => '7'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_cms() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'Employment Opportunities', 'page_id' => '5'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_cms2() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'Employment Opportunities', 'page_id' => '6'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function show_cms3() {
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $data['where'] = ['page_type' => 'Employment Opportunities', 'page_id' => '7'];
        $result = $this->selectRecords($data);
        return $result;
    }

    public function JobDescriptionInstructorbanner() {
        $data['where'] = ['banner_type' => 'Employment Opportunities', 'banner_id' => '7'];
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $resultbanner = $this->selectRecords($data);


        return $resultbanner;
    }

    public function JobDescriptionInstructorcms() {

        $data['where'] = ['page_type' => 'Instructor Job Description', 'page_id' => '8'];
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $resultcms = $this->selectRecords($data);
        return $resultcms;
    }

    public function JobDescriptionsupervisorbanner() {
        $data['where'] = ['banner_type' => 'Employment Opportunities', 'banner_id' => '7'];
        $data['select'] = ['*'];
        $data['table'] = TABLE_BANNES;
        $resultbanner = $this->selectRecords($data);


        return $resultbanner;
    }

    public function JobDescriptionsupervisorcms() {

        $data['where'] = ['page_type' => 'Supervisor Job Description', 'page_id' => '9'];
        $data['select'] = ['*'];
        $data['table'] = TABLE_CMS;
        $resultcms = $this->selectRecords($data);

        return $resultcms;
    }

    public function Employeementapplication($postData) {



        $internet_site = $postData['internet_site'];
        $employee = $postData['employee'];
        $flyer = $postData['flyer'];
        $other = $postData['other'];

        $fname = $postData['f_name'];
        $lname = $postData['l_name'];
        $mid_name = $postData['mid_name'];
        $street_name = $postData['street_name'];
        $city_name = $postData['city_name'];
        $state_name = $postData['state_name'];
        $uemail = $postData['uemail'];
        $zip_name = $postData['zip_name'];
        $hphone = $postData['hphone'];
        $cphone = $postData['celltxt1'] . '-' . $postData['celltxt2'] . '-' . $postData['celltxt3'];
        $ophone = $postData['ophone'];
        $fax = $postData['fax'];

        $f_name_em = $postData['f_name_em'];
        $l_name_em = $postData['l_name_em'];
        $f_rel_em = $postData['f_rel_em'];
        $street_name_em = $postData['street_name_em'];
        $city_name_em = $postData['city_name_em'];
        $state_name_em = $postData['state_name_em'];
        $uemail_em = $postData['uemail_em'];
        $zip_name_em = $postData['zip_name_em'];
        $hphone_em = $postData['hphone_em'];
        $cphone_em = $postData['celltxt1_em'] . '-' . $postData['celltxt2_em'] . '-' . $postData['celltxt3_em'];
        $ophone_em = $postData['ophone_em'];
        $fax_em = $postData['fax_em'];

        $age = $postData['age'];
        $eligible = $postData['eligible'];
        $docu = $postData['docu'];
        $license = $postData['license'];
        $clean = $postData['clean'];
        $vehicle = $postData['vehicle'];

        $make = $postData['make'];
        $model = $postData['model'];
        $year = $postData['year'];

        $reg = $postData['reg'];
        $automobile = $postData['automobile'];
        $health = $postData['health'];
        $criminal = $postData['criminal'];
        $cpr = $postData['cpr'];
        $special_training = $postData['special_training'];
        $hs = $postData['hs'];
        $degree1 = $postData['degree_type'];
        $degree2 = $postData['degree_type2'];
        $degree3 = $postData['degree_type3'];
        $graduate = $postData['graduate'];
        $ts = $postData['ts'];
        $degree_type = $postData['degree_type'];
        $degree_type2 = $postData['degree_type2'];
        $degree_type3 = $postData['degree_type3'];

        $employer = $postData['employer'];
        $employment = $postData['employment'];
        $sadd = $postData['sadd'];
        $h_city = $postData['h_city'];
        $h_state = $postData['h_state'];
        $h_zip = $postData['h_zip'];
        $h_ptitle = $postData['h_ptitle'];
        $h_supervisor = $postData['h_supervisor'];
        $h_phone = $postData['h_phone'];
        $h_salary = $postData['h_salary'];
        $h_duties = $postData['h_duties'];
        $h_reason = $postData['h_reason'];

        $fullname = "<html><body>";
        $fullname .= "<table width='100%' cellpadding='5' cellspacing='2' border='1'>";
        $fullname .= "<thead><tr><th colspan='2' align='left'>Position Desired</th></tr></thead>";
        $fullname .= "<tbody>";
        $fullname .= "<tr><td colspan='2'><strong>How did you hear about the position opening?</strong></td></tr>";
        $fullname .= "<tr><td><label>Internet Site : </label>" . $internet_site . "</td><td><label>Employee Name :</label>" . $employee . "</td></tr>";
        $fullname .= "<tr><td><label>Flyer : </label>" . $flyer . "</td><td><label>Other : </label>" . $other . "</td></tr>";
        $fullname .= "</tbody>";

        $fullname .= "</table >";

        $fullname .= "<table width='100%' cellpadding='5' cellspacing='2' border='1'>";
        $fullname .= "<thead><tr><th colspan='2' align='left'>Personal Information</th></tr></thead>";
        $fullname .= "<tbody>";
        $fullname .= "<tr><td><label>Last Name : </label>" . $lname . "</td><td><label>First Name : </label>" . $fname . "</td></tr>";
        $fullname .= "<tr><td><label>Middle Initial : </label>" . $mid_name . "</td><td><label>Street Address : </label>" . $street_name . "</td></tr>";

        $fullname .= "<tr><td><label>City : </label>" . $city_name . "</td><td><label>State : </label>" . $state_name . "</td></tr>";

        $fullname .= "<tr><td><label>Email : </label>" . $uemail . "</td><td><label>Zip Code : </label>" . $zip_name . "</td></tr>";

        $fullname .= "<tr><td><label>Home Phone : </label>" . $hphone . "</td><td><label>Cell Phone : </label>" . $cphone . "</td></tr>";

        $fullname .= "<tr><td><label>Other Phone : </label>" . $ophone . "</td><td><label>Fax : </label>" . $fax . "</td></tr>";

        $fullname .= "</tbody>";

        $fullname .= "</table >";

        $fullname .= "<table width='100%' cellpadding='5' cellspacing='2' border='1'>";
        $fullname .= "<thead><tr><th colspan='2' align='left'>In Case of Emergency Please Notify</th></tr></thead>";
        $fullname .= "<tbody>";
        $fullname .= "<tr><td><label>Last Name : </label>" . $l_name_em . "</td><td><label>First Name : </label>" . $f_name_em . "</td></tr>";
        $fullname .= "<tr><td><label>Relationship : </label>" . $f_rel_em . "</td><td><label>Street Address : </label>" . $street_name_em . "</td></tr>";

        $fullname .= "<tr><td><label>City : </label>" . $city_name_em . "</td><td><label>State : </label>" . $state_name_em . "</td></tr>";

        $fullname .= "<tr><td><label>Email : </label>" . $uemail_em . "</td><td><label>Zip Code : </label>" . $zip_name_em . "</td></tr>";

        $fullname .= "<tr><td><label>Home Phone : </label>" . $hphone_em . "</td><td><label> Cell Phone : </label>" . $cphone_em . "</td></tr>";

        $fullname .= "<tr><td><label> Other Phone : </label>" . $ophone_em . "</td><td><label>Fax :</label>" . $fax_em . "</td></tr>";
        $fullname .= "</tbody>";
        $fullname .= "</table >";


        $fullname .= "<table width='100%' cellpadding='5' cellspacing='2' border='1'>";
        $fullname .= "<thead><tr><th colspan='2' align='left'>Work Eligibility</th></tr></thead>";
        $fullname .= "<tbody><tr><td colspan='2'><strong>The following minimum conditions must be met for initial and/or continued employment with Transition to Independence</strong></td></tr>";
        $fullname .= "<tr><td width='91%'><label>Are you at least 18 years of age or older?  </label></td><td width='9%'>" . $age . "</td></tr>";
        $fullname .= "<tr><td><label>Are you eligible to work in the United States?  </label></td><td>" . $eligible . "</td></tr>";
        $fullname .= "<tr><td><label>Can you provide at least two documents that prove your eligibility to work in the United States (e.g. Valid California Driver License, Social Security Card,Birth Certificate, Passport, etc.)?  </label></td><td>" . $docu . "</td></tr>";
        $fullname .= "<tr><td><label>Do you possess a Valid California Driver License?  </label></td><td>" . $license . "</td></tr>";
        $fullname .= "<tr><td><label>Can you provide a copy of Proof of a clean California DMV Driving Record? </label></td><td>" . $clean . "</td></tr>";
        $fullname .= "<tr><td><label>Do you own a vehicle?   </label></td><td>" . $vehicle . "</td></tr>";

        if ($vehicle == 'Yes') {
            $fullname .= "<tr><td><label> Make: " . $make . "</label>&nbsp;&nbsp;&nbsp;&nbsp;<label> Model: " . $model . " </label>&nbsp;&nbsp;&nbsp;&nbsp;<label> Year: " . $year . "</label></td></tr>";
        }


        $fullname .= "<tr><td><label>Can you provide proof of current California Automobile Registration for the vehicle that you own? : </label></td><td>" . $reg . "</td></tr>";
        $fullname .= "</tbody>";
        $fullname .= "</table >";




        $fullname .= "<table width='100%' cellpadding='5' cellspacing='2' border='1'>";
        $fullname .= "<thead><tr><th colspan='2' align='left'>Work Eligibility Continued</th></tr></thead>";
        $fullname .= "<tbody>";
        $fullname .= "<tr><td colspan='2'><strong>The following minimum conditions must be met for initial and/or continued employment with Transition to Independence</strong></td></tr>";
        $fullname .= "<tr><td width='91%'><label>Can you provide Proof of current valid automobile insurance for the vehicle that you own? : </label></td><td width='9%'>" . $automobile . "</td></tr>";
        $fullname .= "<tr><td><label>Can you provide evidence of good physical health including tuberculosis clearance? </label></td><td>" . $health . "</td></tr>";
        $fullname .= "<tr><td><label>Can you obtain a California Criminal Clearance?  </label></td><td>" . $criminal . "</td></tr>";
        $fullname .= "<tr><td><label>Do you hold a valid CPR and/or First Aid Certificate?  </label></td><td>" . $cpr . "</td></tr>";
        $fullname .= "</tbody>";
        $fullname .= "</table >";

        $fullname .= "<table width='100%' cellpadding='5' cellspacing='2' border='1'>";
        $fullname .= "<thead><tr><th colspan='3' align='left'>Education</th></tr></thead>";
        $fullname .= "<tbody>";
        $fullname .= "<tr><td colspan='3'><strong>Please provide the following information about your educational background</strong></td></tr>";

        $fullname .= "<tr>
      <td width='51%'><label>Name and Location of High School</label></td>
        <td width='26%'><label>Did you Graduate? </label>" . $hs . "</td>
        <td width='23%'><label>Type of Degree : </label>" . $degree_type . "</td>
    </tr>";

        $fullname .= "<tr>
      <td width='51%'><label>Name and Location of College/University</label></td>
        <td width='26%'><label>Did you Graduate? </label>" . $graduate . "</td>
        <td width='23%'><label>Type of Degree : </label>" . $degree_type2 . "</td>
    </tr>";

        $fullname .= "<tr>
      <td width='51%'><label>Name and Location of Business or Trade School</label></td>
        <td width='26%'><label>Did you Graduate? </label>" . $ts . "</td>
        <td width='23%'><label>Type of Degree : </label>" . $degree_type3 . "</td>
    </tr>";

        $fullname .= "<tr>
        <td colspan='3'><strong>Describe any special training or skills (additional spoken languages, computer knowledge,etc.): </strong><br><hr>" . $special_training . "</td></tr>";
        $fullname .= "</tbody>";

        $fullname .= "</table >";


        $fullname .= "<table width='100%' cellpadding='5' cellspacing='2' border='1'>";
        $fullname .= "<thead><tr><th colspan='2' align='left'>Work History</th></tr></thead>";
        $fullname .= "<tbody>";

        $fullname .= "<tr><td colspan='2' align='left'><strong>Provide your full-time employment history for the last 5 years starting with your present or most recent employer. You may attach a resume or additional pages in addition to completing this portion of the employment application if necessary.</strong></td></tr>";

        $fullname .= "<tr><td><label>Employer : </label>" . $employer . "</td><td><label>Dates of Employment : </label>" . $employment . "</td></tr>";

        $fullname .= "<tr><td><label>Street Address : </label>" . $sadd . "</td><td><label>City : </label>" . $h_city . "</td></tr>";

        $fullname .= "<tr><td><label>State : </label>" . $h_state . "</td><td><label>Zip Code : </label>" . $h_zip . "</td></tr>";

        $fullname .= "<tr><td><label>Position Title : </label>" . $h_ptitle . "</td><td><label>Supervisor Name : </label>" . $h_supervisor . "</td></tr>";

        $fullname .= "<tr><td><label> Phone : </label>" . $h_phone . "</td><td><label>Ending Salary : </label>" . $h_salary . "</td></tr>";
        $fullname .= "<tr><td><label> Duties and Responsibilities : </label>" . $h_duties . "</td><td><label>Reason for Leaving : </label>" . $h_reason . "</td></tr>";

        $fullname .= "</tbody>";

        $fullname .= "</table >";

        if ($_FILES) {

//            foreach ($_FILES as $name => $file) {
//                $file_name = $file["name"];
//                $file_temp = $file["tmp_name"];
//                foreach ($file_name as $key) {
//                    $path_parts = pathinfo($key);
//                    $extension = strtolower($path_parts["extension"]);
//                    $paths = " C:/xampp/htdocs/transition_independence/public/assets/front/test/";
//                    $server_file[] = $paths . $path_parts['basename'];
//                }
//                for ($i = 0; $i < count($file_temp); $i++) {
//                    move_uploaded_file($file_temp[$i], $server_file[$i]);
//                }
//            }
//            
//            
//             for ($i = 0; $i < count($server_file); $i++) {
//                $afile = fopen($server_file[$i], "rb");
//                $data = fread($afile, filesize($server_file[$i]));
//                fclose($afile);
//                $data = chunk_split(base64_encode($data));
//            }

            $data1['subject'] = 'Herboglobal';
            $data1['message'] = $fullname;
            $asd = sendMail($data1);
            if ($asd) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Your registration has been done successfully. We have sent you an email';
                $json_response['redirect'] = SITE_URL;
            } else {
                $json_response['status'] = 'warning';
                $json_response['message'] = 'mail could not be sent!';
            }
        }
        return $json_response;
    }

}

?>
